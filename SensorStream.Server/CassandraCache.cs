﻿using SensorStreamCore;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SensorStream
{
    public class CassandraCache
    {
        public ConcurrentDictionary<Guid, Dictionary<String, String>> complexColumns = new ConcurrentDictionary<Guid, Dictionary<String, String>>();
        public ConcurrentDictionary<Guid, Guid> StreamToDeviceID = new ConcurrentDictionary<Guid, Guid>();

        public ConcurrentDictionary<Guid, CassandraStream> Streams = new ConcurrentDictionary<Guid, CassandraStream>();
        //public ConcurrentDictionary<Guid, CassandraCStream> CStreams = new ConcurrentDictionary<Guid, CassandraCStream>();
        
        private static readonly Lazy<CassandraCache>_instance = new Lazy<CassandraCache>(()=> new CassandraCache());
        
        public static CassandraCache Instance
        {
            get
            {
                return _instance.Value;
            }
        }

        public CassandraCache()
        {
        }
    }
}