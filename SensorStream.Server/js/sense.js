var __nplots = 0;
var scatterPlot = function(selector, w, h, d, lbl)
{
    var uu = __nplots++;
    console.log(lbl);
    var yLabel = lbl;
    var margin = {
        top: 40,
        right: 25,
        bottom: 80,
        left: 90
    };

    var dFn = function(a)
    {
        return a.Time;
    };
    var rFn = function(a)
    {
        return a.Value;
    };



    var setData = function(da)
    {

        var data = [];

        var lim = da.length > 150 ? da.length - 150 : 0;
        for (var i = lim; i < da.length; i++)
        {
            data.push(
            {
                Time: moment(da[i].Time).toDate(),
                Value: parseFloat(da[i].Value)
            });
        };

        
        return data;
    }

    var data = setData(d);

    this.yAxisLabel = function(lbl)
    {
        yLabel = lbl;
        return this;
    }

    this.addData = function(obj)
    {
        obj.Time = moment(obj.Time).toDate();
        obj.Value = parseFloat(obj.Value);
        if (data.length > 150) data.shift();
        data.push(obj);
        return this;
    }

    this.drop = function(n)
    {
        for (var i = 0; i < n; i++)
        {
            data.shift();
        };
        return this;
    }

    var width = w - margin.left - margin.right;
    var height = h - margin.top - margin.bottom;

    var x = d3.time.scale()
        .range([0, width])
        .domain(d3.extent(data, dFn)).nice(10);

    var y = d3.scale.linear()
        .range([height, 0])
        .domain(d3.extent(data, rFn)).nice();;

    var xAxis = d3.svg.axis()
        .scale(x)
        .orient("bottom");

    var yAxis = d3.svg.axis()
        .scale(y)
        .orient("left");



    var chart = d3.select(selector)
        .append('svg:svg')
        .attr('width', width + margin.right + margin.left)
        .attr('height', height + margin.top + margin.bottom)
        .attr('class', 'chart');

    var main = chart.append('svg:g')
        .attr('transform', 'translate(' + margin.left + ',' + margin.top + ')')
        .attr('width', width)
        .attr('height', height)
        .attr('class', 'main');

    main.append('svg:g')
        .attr('transform', 'translate(0,' + height + ')')
        .attr('class', 'axis')
        .attr('id', 'xaxis' + uu)
        .call(xAxis);

    main.append('svg:g')
        .attr('transform', 'translate(0,0)')
        .attr('class', 'axis')
        .attr('id', 'yaxis' + uu)
        .call(yAxis)
        .append('text')
        .attr('transform', "rotate(-90)")
        .attr('y', -69)
        .attr('dy', '0.6em')
        .style('text-anchor', 'end')
        .text(yLabel);


    // draw the graph object
    var g = main.append("svg:g");

    g.selectAll("scatter-dots")
        .data(data, dFn)
        .enter()
        .append("svg:circle")
        .attr("cy", function(d)
        {
            return y(rFn(d));
        })
        .attr("cx", function(d)
        {
            return x(dFn(d));
        })
        .attr('class', 'dots')
        .attr("r", 3)
        .style("opacity", 0.6);


    this.redraw = function()
    {
        width = $(selector).width() - margin.left - margin.right;
        height = $(selector).height() - margin.top - margin.bottom;

        chart.attr('width', width + margin.right + margin.left)
            .attr('height', height + margin.top + margin.bottom);

        main.attr('width', width)
            .attr('height', height);

        x.range([0, width])
            .domain(d3.extent(data, dFn));

        y.range([height, 0])
            .domain(d3.extent(data, rFn));

        xAxis = d3.svg.axis()
            .scale(x)
            .orient("bottom");

        yAxis = d3.svg.axis()
            .scale(y)
            .orient("left");

        d3.select("#xaxis" + uu)
            .transition()
            .attr('transform', 'translate(0,' + height + ')')
            .call(xAxis);


        d3.select("#yaxis" + uu)
            .transition()
            .call(yAxis)

        var ps = g.selectAll(".dots").data(data, dFn);
        ps.transition()
            .attr("cx", function(d)
            {
                return x(dFn(d));
            })
            .attr("cy", function(d)
            {
                return y(rFn(d));
            })
        ps.enter().append('svg:circle')
            .attr('class', 'dots')
            .attr("r", 3)
            .style("opacity", 0.6)
            .transition()
            .attr("cx", function(d, i)
            {
                return x(dFn(d));
            })
            .attr("cy", function(d, i)
            {
                return y(rFn(d));
            });

        ps.exit()
            .transition()
            .remove();


    }

    return this;
}

    function createPJSCanvas(id, width, height)
    {
        var canvas = $('<canvas id="' + id + '" width="' + width + '" height="' + height + '"></canvas>');
        return canvas;
    }
