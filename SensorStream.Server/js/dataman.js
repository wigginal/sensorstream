$.dm = new(function()
{
    var baseURL = "localhost";
    var deviceAPI = "/device.ashx?";
    var streamAPI = "/stream.ashx?";
    var dataAPI = "/data.ashx?";
    this.streamIdMap = [];
    var hub = null;
    var pref = this;
    // test device guid: 5b4ac80f-41e3-412d-b893-61ddf5357a46

    // Private functions

    // Converts an integer (unicode value) to a char

    function itoa(i)
    {
        return String.fromCharCode(i);
    }


    // Converts a char into to an integer (unicode value)

    function atoi(a)
    {
        return a.charCodeAt();
    }

    // Singalr Bits
    // Only avalable if signalr is included in the client page.
    if ($.connection)
    {

        // Initialize a connection with the server.
        // This must be called before using subscribe() or unsubscribe()
        this.connect = function()//g)
        {
            $.connection.hub.url = baseURL + "/signalr";
            $.connection.hub.start().done(function(r)
            {
                console.log(r);
                console.log("Connected to " + baseURL);
                //g();
            });
            hub = $.connection.dataHub;
            hub.client.newData = function(name, message)
            {
                message = JSON.parse(message);
                if (pref.streamIdMap[message.streamid])
                {
                    var f = pref.streamIdMap[message.streamid];
                    message.time = new Date(message.time);
                    f(name, message);
                }
                // In theory you shouldn't get a stream you haven't defined a handler for.
                else
                {
                    console.error("Trouble. This is a stream you don't have a handler for.");
                }
            }
            $.connection.hub.start();
            return pref = this;
        }

        // Add a stream specific handler.
        this.subscribe = function(streamID, newDataCallback)
        {
            if (!hub) return this;
            pref.streamIdMap[streamID] = newDataCallback;
            hub.server.subscribe(streamID);
            return pref = this;
        }

        // Unsubscribe a stream specific handler. 
        this.unsubscribe = function(streamID)
        {
            if (!hub) return this;
            hub.server.unsubscribe(streamID);
            pref.streamIdMap[streamID] = undefined;
            return pref = this;
        }
    }

    // All of these functions support the jQuery style .fail(), .done(), and .always()
    this.createDevice = function(deviceInfo)
    {
        return $.ajax(
        {
            url: baseURL + deviceAPI + 'create=',
            contentType: "application/json; charset=utf-8;",
            type: "POST",
            data: JSON.stringify(deviceInfo),
            xhrFields:
            {
                withCredentials: false
            }
        });
    }

    this.createStream = function(deviceID, streamInfo)
    {
        var d = JSON.stringify(streamInfo);
        console.log(d);
        return $.ajax(
        {
            url: baseURL + streamAPI + 'create=',
            headers:
            {
                key: deviceID
            },
            contentType: "application/json; charset=utf-8;",
            type: "POST",
            data: d,
            xhrFields:
            {
                withCredentials: false
            }
        });
    }

    this.deleteStream = function(streamID)
    {
        return $.ajax(
        {
            url: baseURL + streamAPI + 'create=',
            headers:
            {
                key: did
            },
            contentType: "application/json; charset=utf-8;",
            type: "POST",
            data:
            {
                'delete': streamID
            },
            xhrFields:
            {
                withCredentials: false
            }
        });
    }

    this.deleteDevice = function(deviceID)
    {
        return $.ajax(
        {
            url: baseURL + deviceAPI + 'delete=device',
            headers:
            {
                key: deviceID
            },
            contentType: "application/json; charset=utf-8;",
            type: "POST",
            xhrFields:
            {
                withCredentials: false
            }
        });
    }

    this.deleteDeviceStreams = function(deviceID)
    {
        return $.ajax(
        {
            url: baseURL + deviceAPI + 'delete=streams',
            headers:
            {
                key: deviceID
            },
            contentType: "application/json; charset=utf-8;",
            type: "POST",
            xhrFields:
            {
                withCredentials: false
            }
        });
    }

    this.getDevices = function()
    {

        return $.getJSON(baseURL + deviceAPI + "getdevices=");
    }

    // {
    //      getstreams: DeviceName,
    //      user: UserName
    //  }
    this.getStreams = function(deviceInfo)
    {
        return $.getJSON(baseURL + streamAPI, deviceInfo)
    }


    this.getData = function(streamID, startDate, nPoints)
    {
        if (startDate && nPoints)
        {
            var timestring = startDate.toISOString();
            return $.getJSON(baseURL + dataAPI,
            {
                getdata: streamID,
                count: nPoints,
                start: timestring
            });
        }
        return $.getJSON(baseURL + dataAPI,
        {
            getdata: streamID
        });
    }

    this.sendData = function(deviceID, data)
    {
        return $.ajax(
        {
            url: baseURL + dataAPI + 'create=',
            headers:
            {
                key: deviceID,
            },
            contentType: "application/json; charset=utf-8;",
            type: "POST",
            data: JSON.stringify(data),
            xhrFields:
            {
                withCredentials: false
            }
        });
    }
})();
