// Check for the various File API support.
if ( window.File && window.FileReader && window.FileList && window.Blob )
{
    // Great success! All the File APIs are supported.
}
else
{
    alert( 'The File APIs are not fully supported in this browser.' );
}

function traverseFiles( files )
{
    if ( typeof files !== "undefined" )
    {
        for ( var i = 0, l = files.length; i < l; i++ )
        {
            uploadFile( files[ i ] );
        }
    }
    else
    {
        fileList.innerHTML = "No support for the File API in this web browser";
    }
}

function outputTable(id)
{
	var t = document.createElement('table');
	t.id = id;
	return t;
}

function handleFileSelect( evt )
{
    var files = evt.target.files; // FileList object
    // files is a FileList of File objects. List some properties.
    var output = [ ];
    for ( var i = 0, f; f = files[ i ]; i++ )
    {
    	console.log("dataman for "+f.name);
    	var dm = new dataman();
        output.push( '<li><strong>', escape( f.name ), '</strong> (', f.type || 'n/a', ') - ',
            f.size, ' bytes, last modified: ',
            f.lastModifiedDate ? f.lastModifiedDate.toLocaleDateString( ) : 'n/a',
            '</li>' );
        console.log("adding event");
        var t = outputTable(f.name);
        document.getElementById('contents').appendChild(t);
        dm.addOnDataReady("appendTable",function(f,d){document.getElementById(f.name).innerHTML = dm.makeTable(d);});
        dm.dget(f);
    }
    document.getElementById( 'list' ).innerHTML = '<ul>' + output.join( '' ) + '</ul>';
}

function addFileHandler( id, handler )
{
    document.getElementById( id ).addEventListener( 'change', handler, false );
}
