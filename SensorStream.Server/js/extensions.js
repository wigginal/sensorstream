$.extend(
{
    distinct: function(anArray)
    {
        var result = [];
        $.each(anArray, function(i, v)
        {
            if ($.inArray(v, result) == -1) result.push(v);
        });
        return result;
    }
});

$.transpose = function(a2dArray)
{
    return Object.keys(a2dArray[0]).map(
        function(c)
        {
            return a2dArray.map(function(r)
            {
                return r[c];
            });
        }
    );
}

$.anyEq = function(a, b)
{
    return $.map(a, function(n)
    {
        return $.inArray(n, b) > -1;
    }).reduce(function(c, d)
    {
        return c || d;
    });
}

$.subset = function(a, b)
{
    return $.map(a, function(n)
    {
        return $.inArray(n, b) > -1;
    }).reduce(function(c, d)
    {
        return c && d;
    });
}

function nan2null(a)
{
    if (isNaN(a)) return "null";
    else return a;
}

function same(a, b)
{
    if (!a.length || !b.length || a.length != b.length)
        return false;
    return $.map(a, function(c, i)
    {
        return c === b[i]
    }).reduce(function(a, b)
    {
        return (a && b)
    });
}

function type(x)
{
    return Object.prototype.toString.call(x);
}

$.avg = function(a)
{
    var ca = a.map(function(a)
    {
        if (!isNaN(a)) return a
    });
    return ca.reduce(function(b, c)
    {
        return b + c;
    }, 0.0) / ca.length;
}

$.sd = function(a)
{
    var ave = $.avg(a);
    var ca = a.map(function(a)
    {
        if (!isNaN(a)) return a
    });
    return Math.sqrt(ca.map(function(e)
    {
        return (e - ave) * (e - ave);
    }).reduce(function(b, c)
    {
        return b + c;
    }, 0.0) / ca.length);
}

$.esc = function(str)
{
    // http://stackoverflow.com/questions/3446170/escape-string-for-use-in-javascript-regex#6969486
    // Referring to the table here:
    // https://developer.mozilla.org/en/JavaScript/Reference/Global_Objects/regexp
    // these characters should be escaped
    // \ ^ $ * + ? . ( ) | { } [ ]
    // These characters only have special meaning inside of brackets
    // they do not need to be escaped, but they MAY be escaped
    // without any adverse effects (to the best of my knowledge and casual testing)
    // : ! , = 
    // my test "~!@#$%^&*(){}[]`/=?+\|-_;:'\",<.>".match(/[\#]/g)

    var specials = [
        // order matters for these
        "-",
        "[",
        "]",
        // order doesn't matter for any of these,
        "/",
        "{",
        "}",
        "(",
        ")",
        "*",
        "+",
        "?",
        ".",
        "\\",
        "^",
        "$",
        "|",
        // Extras for selector statements.
        "#",
        "%"
    ];

    // I choose to escape every character with '\'
    // even though only some strictly require it when inside of []
    var regex = RegExp('[' + specials.join('\\') + ']', 'g');


    return str.replace(regex, "\\$&").replace(new RegExp(" ", "g"), "_");
    // test escapeRegExp("/path/to/res?search=this.that")
}

$.desc = function(str)
{
    // http://stackoverflow.com/questions/3446170/escape-string-for-use-in-javascript-regex#6969486
    // Referring to the table here:
    // https://developer.mozilla.org/en/JavaScript/Reference/Global_Objects/regexp
    // these characters should be escaped
    // \ ^ $ * + ? . ( ) | { } [ ]
    // These characters only have special meaning inside of brackets
    // they do not need to be escaped, but they MAY be escaped
    // without any adverse effects (to the best of my knowledge and casual testing)
    // : ! , = 
    // my test "~!@#$%^&*(){}[]`/=?+\|-_;:'\",<.>".match(/[\#]/g)

    var specials = [
        // order matters for these
        "-",
        "[",
        "]",
        // order doesn't matter for any of these,
        "/",
        "{",
        "}",
        "(",
        ")",
        "*",
        "+",
        "?",
        ".",
        "\\",
        "^",
        "$",
        "|",
        // Extras for selector statements.
        "#",
        "%",
        " "
    ];

    // I choose to escape every character with '\'
    // even though only some strictly require it when inside of []
    var regex = RegExp('[' + specials.join('\\') + ']', 'g');


    return str.replace(regex, "");
    // test escapeRegExp("/path/to/res?search=this.that")
}

clamp = function(a,lo,hi)
{
    if(a > hi) return hi;
    if(a < lo) return lo;
    return a;
}

/*
 * object.watch polyfill
 *
 * 2012-04-03
 *
 * By Eli Grey, http://eligrey.com
 * Public Domain.
 * NO WARRANTY EXPRESSED OR IMPLIED. USE AT YOUR OWN RISK.
 */

// object.watch
if (!Object.prototype.watch)
{
    Object.defineProperty(Object.prototype, "watch",
    {
        enumerable: false,
        configurable: true,
        writable: false,
        value: function(prop, handler)
        {
            var
            oldval = this[prop],
                newval = oldval,
                getter = function()
                {
                    return newval;
                }, setter = function(val)
                {
                    oldval = newval;
                    return newval = handler.call(this, prop, oldval, val);
                };

            if (delete this[prop])
            { // can't watch constants
                Object.defineProperty(this, prop,
                {
                    get: getter,
                    set: setter,
                    enumerable: true,
                    configurable: true
                });
            }
        }
    });
}

// object.unwatch
if (!Object.prototype.unwatch)
{
    Object.defineProperty(Object.prototype, "unwatch",
    {
        enumerable: false,
        configurable: true,
        writable: false,
        value: function(prop)
        {
            var val = this[prop];
            delete this[prop]; // remove accessors
            this[prop] = val;
        }
    });
}

/*
 * End object.watch
 */


/*
 * JavaScript Pretty Date
 * Copyright (c) 2011 John Resig (ejohn.org)
 * Licensed under the MIT and GPL licenses.
 */

// Takes an ISO time and returns a string representing how
// long ago the date represents.
function prettyDate(time){
    var date = new Date((time || "").replace(/-/g,"/").replace(/[TZ]/g," ")),
        diff = (((new Date()).getTime() - date.getTime()) / 1000),
        day_diff = Math.floor(diff / 86400);
            
    if ( isNaN(day_diff) || day_diff < 0 || day_diff >= 31 )
        return;
            
    return day_diff == 0 && (
            diff < 60 && "just now" ||
            diff < 120 && "1 minute ago" ||
            diff < 3600 && Math.floor( diff / 60 ) + " minutes ago" ||
            diff < 7200 && "1 hour ago" ||
            diff < 86400 && Math.floor( diff / 3600 ) + " hours ago") ||
        day_diff == 1 && "Yesterday" ||
        day_diff < 7 && day_diff + " days ago" ||
        day_diff < 31 && Math.ceil( day_diff / 7 ) + " weeks ago";
}

// If jQuery is included in the page, adds a jQuery plugin to handle it as well
if ( typeof jQuery != "undefined" )
    jQuery.fn.prettyDate = function(){
        return this.each(function(){
            var date = prettyDate(this.title);
            if ( date )
                jQuery(this).text( date );
        });
    };

/*
 * End Pretty Date 
 */