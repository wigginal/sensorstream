function dataman( )
{
    this.data   = null;
    this.header = null;
    this.csv    = null;
    this.ondata = [ ];

    this.dget = function( file )
    {
        var reader = new FileReader( );
        reader.readAsText( file );
        var ref = this;
        reader.onload = function( event )
        {
            ref.csv = event.target.result;
            ref.data = $.csv.toArrays( ref.csv );
            ref.header = ref.data[ 0 ].map( function( a )
            {
                return a.trim( );
            } );
            for ( var e in ref.ondata )
            {
                if ( e ) ref.ondata[ e ]( file, ref.data, ref.header );
            }

        };
        reader.onerror = function( )
        {
            alert( 'Unable to read ' + file.fileName );
        };
        return this;
    }

    this.makeTable = function( )
    {
        var html = '';
        for ( var row in data )
        {
            html += '<tr>\r\n';
            for ( var item in data[ row ] )
            {
                html += '<td>' + data[ row ][ item ] + '</td>\r\n';
            }
            html += '</tr>\r\n';
        }
        return html;
        return this;
    }

    this.addOnDataReady = function( name, eve )
    {
        this.ondata[ name ] = eve;
        
        return this;
    }

    this.Header = function( )
    {
        return this.header;
    }

    this.Data = function( field )
    {
        var idx = this.header.indexOf( field );
        return this.data.slice( 1 ).map( function( a, b )
        {
            return a[ idx ];
        } );
    }

    return this;
}
