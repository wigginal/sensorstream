﻿using Cassandra;
using Cassandra.Data.Linq;
using Microsoft.ApplicationServer.Caching;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SensorStreamCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace SensorStream
{
    /// <summary>
    /// The stream handler is in charge of creating new streams for each device, deleteing streams 
    /// and allowing users to query which streams are available
    /// </summary>
    public class stream : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            Utilities.SendHeaders(context);
            if (context.Request.HttpMethod.Equals("OPTIONS", StringComparison.InvariantCultureIgnoreCase))
            {
                return;
            }
            //get the ip of the request
            String ip = Utilities.GetRequestIP(context);

            //check for the create flag for creating  a new string
            if (context.Request.QueryString["create"] != null)
            {
                CreateStream(context, ip);
            }
            else if (!String.IsNullOrEmpty(context.Request.QueryString["getstreams"]) && !String.IsNullOrEmpty(context.Request.QueryString["user"]))
            {
                GetStreams(context, ip);
            }
            else if (!String.IsNullOrEmpty(context.Request.QueryString["devid"]))
            {
                GetStreamsByDevice(context, ip);
            }
            else if (!String.IsNullOrEmpty(context.Request.QueryString["streamid"]))
            {
                GetStream(context, ip);
            }
            else if (context.Request.QueryString["delete"] != null)
            {
                DeleteStream(context, ip);
            }
            else
            {
                //Incorrect Query String parameters
                Utilities.LogAndWriteErrorResponse(context, ip + " : " + "Invalid Query string parameters", "Invalid Query string parameters");
            }
        }

        private void GetStreamsByDevice(HttpContext context, string ip)
        {
            //get the device name and user name for the two devices we are looking for
            Guid deviceID = Guid.Parse(context.Request.QueryString["devid"]);

            //log the request to for later debugging
            Logger.Instance.LogData(ip + ": Requested streams for deviceID: " + deviceID.ToString());

            //get the Cassandra session and the device contexts
            ISession session = Cluster.Instance.Session;

            //get the table we need to search through for the device and then the streams
            Table<CassandraStream> streamTable = SessionExtensions.GetTable<CassandraStream>(session);
            Table<CassandraStreamByDevice> streambydev = SessionExtensions.GetTable<CassandraStreamByDevice>(session);
            Table<CassandraSensorDevice> sensordeviceTable = SessionExtensions.GetTable<CassandraSensorDevice>(session);

            try
            {
                //get the sensor device
                CassandraSensorDevice csd = sensordeviceTable.First(dev => dev.guid == deviceID).Execute();

                //create the Device to serialize and return later
                SensorDevice js = new SensorDevice { devicename = csd.DeviceName, description = csd.Description, username = csd.UserName };

                //get all of the streams for this device, enumerate through them adding them to the response list
                foreach (CassandraStreamByDevice streams in streambydev.Where(st => st.deviceID == csd.guid).Execute())
                {
                    //call to the stream table to get the information on these streams
                    foreach (CassandraStream strm in streamTable.Where(st => st.StreamID == streams.StreamID).Execute())
                    {
                        //create the new JSON object for the stream to add to the list
                        DeviceStream jstrm = new DeviceStream { name = strm.Name, units = strm.Units, streams = new List<StreamInfo>(), description = strm.Description, streamid = strm.StreamID.ToString(), statistics = Statistics.Instance.GetStatistic(strm.StreamID) };
                        foreach (string stream in strm.Streams)
                        {
                            StreamInfo st = JsonConvert.DeserializeObject<StreamInfo>(stream);
                            jstrm.streams.Add(st);
                        }
                        js.streams.Add(jstrm);
                    }
                }
                //serialize the streams and write them back
                context.Response.ContentType = "application/json; charset=utf-8";
                context.Response.Write(JsonConvert.SerializeObject(js));
            }
            catch (Exception ex)
            {
                Utilities.LogAndWriteErrorResponse(context, ip + " : exception in stream.ashx :: " + ex.Message, "Invalid streamID from request, no stream found");
            }
        }

        private void GetStream(HttpContext context, string ip)
        {
            Guid streamID = Guid.Parse(context.Request.QueryString["streamid"]);

            ISession session = Cluster.Instance.Session;
            //get the table we need to search through for the device and then the streams
            Table<CassandraStream> streamTable = SessionExtensions.GetTable<CassandraStream>(session);
            Table<CassandraStreamByDevice> streambydev = SessionExtensions.GetTable<CassandraStreamByDevice>(session);
            Table<CassandraSensorDevice> sensordeviceTable = SessionExtensions.GetTable<CassandraSensorDevice>(session);

            try
            {
                //call to the stream table to get the information on these streams
                CassandraStream strm = streamTable.First(st => st.StreamID == streamID).Execute();

                //get the sensor device
                CassandraSensorDevice csd = sensordeviceTable.First(dev => dev.UserName == strm.User && dev.DeviceName == strm.devName).Execute();


                SensorDevice js = new SensorDevice { devicename = csd.DeviceName, description = csd.Description, username = csd.UserName };
                //create the new JSON object for the stream to add to the list
                DeviceStream jstrm = new DeviceStream { name = strm.Name, streams = new List<StreamInfo>(), description = strm.Description, streamid = strm.StreamID.ToString(), statistics = Statistics.Instance.GetStatistic(strm.StreamID) };
                
                foreach (string stream in strm.Streams)
                {
                    StreamInfo st = JsonConvert.DeserializeObject<StreamInfo>(stream);
                    jstrm.streams.Add(st);
                }
                js.streams.Add(jstrm);

                //serialize the streams and write them back
                context.Response.ContentType = "application/json; charset=utf-8";
                context.Response.Write(JsonConvert.SerializeObject(js));
            }
            catch (Exception ex)
            {
                Utilities.LogAndWriteErrorResponse(context, ip + " : exception in stream.ashx :: " + ex.Message, "Unknown Error getting the stream, If this problem persists please let us know what is failing");
            }

        }

        private static void DeleteStream(HttpContext context, String ip)
        {
            //get the device ID from the header to match with a device
            Guid devID = Guid.Parse(context.Request.Headers["key"]);
            Guid streamID = Guid.Parse(context.Request.QueryString["delete"]);

            try
            {
                Utilities.DeleteStream(context, ip, devID, streamID);

                //if there is a no reply query string close the connection
                if (context.Request.QueryString["noreply"] != null)
                {
                    context.Response.End();
                    return;
                }
            }
            catch (Exception ex)
            {
                Logger.Instance.LogData(ip + ": Requested delete of stream " + streamID.ToString() + " for device: " + devID + Environment.NewLine + ex.ToString());
            }

            //write out the serialized json object
            context.Response.ContentType = "application/json; charset=utf-8";
            context.Response.Write("{ \"status\" : \"succeess\" }");
        }

        private static void GetStreams(HttpContext context, String ip)
        {
            //get the device name and user name for the two devices we are looking for
            string deviceName = context.Request.QueryString["getstreams"];
            string user = context.Request.QueryString["user"];

            //log the request to for later debugging
            Logger.Instance.LogData(ip + ": Requested streams for device: " + deviceName + "   user: " + user);

            //get the Cassandra session and the device contexts
            ISession session = Cluster.Instance.Session;

            //get the table we need to search through for the device and then the streams
            Table<CassandraStream> streamTable = SessionExtensions.GetTable<CassandraStream>(session);
            Table<CassandraStreamByDevice> streambydev = SessionExtensions.GetTable<CassandraStreamByDevice>(session);
            Table<CassandraSensorDevice> sensordeviceTable = SessionExtensions.GetTable<CassandraSensorDevice>(session);

            try
            {
                //get the sensor device
                CassandraSensorDevice csd = sensordeviceTable.First(dev => dev.UserName == user && dev.DeviceName == deviceName).Execute();

                //create the JSON stream with device object to serialize and return later
                SensorDevice js = new SensorDevice { devicename = csd.DeviceName, description = csd.Description, username = csd.UserName };

                //get all of the streams for this device, enumerate through them adding them to the response list
                //call to get all the streams matching this device
                //****** IF WE WANT MORE THAN 1000 RESULTS WE DO THAT HERE *******************************
                foreach (CassandraStreamByDevice streams in streambydev.Where(st => st.deviceID == csd.guid).Execute())
                {
                    //call to the stream table to get the information on these streams
                    foreach (CassandraStream strm in streamTable.Where(st => st.StreamID == streams.StreamID).Execute())
                    {
                        //create the new JSON object for the stream to add to the list
                        DeviceStream jstrm = new DeviceStream { name = strm.Name, units = strm.Units, streams = new List<StreamInfo>(), description = strm.Description, streamid = strm.StreamID.ToString(), statistics = Statistics.Instance.GetStatistic(strm.StreamID) };
                        foreach (string stream in strm.Streams)
                        {
                            StreamInfo st = JsonConvert.DeserializeObject<StreamInfo>(stream);
                            jstrm.streams.Add(st);
                        }
                        js.streams.Add(jstrm);
                    }
                }

                //serialize the streams and write them back
                context.Response.ContentType = "application/json; charset=utf-8";
                context.Response.Write(JsonConvert.SerializeObject(js));
            }
            catch (Exception ex)
            {
                Utilities.LogAndWriteErrorResponse(context, ip + " : exception in stream.ashx :: " + ex.Message, "Unknown Error getting the stream, If this problem persists please let us know what is failing ");
            }
        }

        private static void CreateStream(HttpContext context, String ip)
        {
            //get the POST data from the input stream
            string jsonInput = Utilities.GetPOSTString(context);

            //if the string is not empty, log it for later testing if we run into an error
            if (!String.IsNullOrEmpty(jsonInput))
            {
                Logger.Instance.LogData(ip + " : " + jsonInput);
            }
            else
            {
                Utilities.LogAndWriteErrorResponse(context, ip + " : No Input String Found", "No Input String Found");
                return;
            }

            try
            {
                //try and deserialize the JSON string into a stream
                dynamic dobject = JsonConvert.DeserializeObject(jsonInput);
                Type t = dobject.GetType();
                if (t == typeof(JArray))
                {
                    DeviceStream[] json = JsonConvert.DeserializeObject<DeviceStream[]>(jsonInput);
                    CreateStream(context, ip, json[0]);
                }
                else
                {
                    DeviceStream json = JsonConvert.DeserializeObject<DeviceStream>(jsonInput);
                    CreateStream(context, ip, json);
                }
            }
            catch (Exception ex)
            {
                Utilities.LogAndWriteErrorResponse(context, ip + " : " + ex.ToString(), "Unknown Error Creating Stream, If this problem persists please let us know what is failing");
                return;
            }
            return;
        }

        private static void CreateStream(HttpContext context, String ip, DeviceStream json)
        {
            try
            {
                //VALIDATE THE JSON OBJECT
                if (json.streams.Count == 0)
                {
                    Utilities.LogAndWriteErrorResponse(context, ip + " : The Stream you are trying to create must contain at least one data field", "The Stream you are trying to create must contain at least one data field");
                    return;
                }

                //check the stream has a name
                if (String.IsNullOrEmpty(json.name))
                {
                    Utilities.LogAndWriteErrorResponse(context, ip + " : No name provided with the stream creation", "No name provided with the stream creation");
                    return;
                }

                foreach (StreamInfo data in json.streams)
                {
                    //check it has a valid type
                    data.type = data.type.ToLower();
                    if (!Utilities.IsValidType(data.type))
                    {
                        Utilities.LogAndWriteErrorResponse(context, ip + " : " + string.Format("The type {0} is an invalid data type. Please check the documentation for valid types", data.type), string.Format("The type {0} is an invalid data type. Please check the documentation for valid types", data.type));
                        return;
                    }

                    //check the stream has a name
                    if (String.IsNullOrEmpty(data.name))
                    {
                        Utilities.LogAndWriteErrorResponse(context, ip + " : No name provided with the stream creation", "No name provided with the stream creation");
                        return;
                    }
                }

                //get the device ID from the header to match with a device
                Guid devID = Guid.Parse(HttpContext.Current.Request.Headers["key"]);

                //get a Cassandra session and contexts for devices and streams
                ISession session = Cluster.Instance.Session;
               
                //get the tables needed to verify the device ID and create a new streams
                Table<CassandraSensorDevice> sensetable = SessionExtensions.GetTable<CassandraSensorDevice>(session);
                Table<CassandraStream> streamTable = SessionExtensions.GetTable<CassandraStream>(session);
                Table<CassandraStreamByDevice> streambydev = SessionExtensions.GetTable<CassandraStreamByDevice>(session);

                //get the matching device from the guid provided
                CassandraSensorDevice scd = sensetable.FirstOrDefault(dev => dev.guid == devID).Execute();

                //if this is null, the deviceID was invalid return the error
                if (scd == null)
                {
                    Utilities.LogAndWriteErrorResponse(context, ip + " : Invalid \"key\" in the header", "Invalid \"key\" in the header");
                    return;
                }

                //create the new stream with the collected data
                CassandraStream cs = new CassandraStream
                {
                    IPCreated = ip,
                    Created = DateTimeOffset.Now,
                    Name = json.name,
                    StreamID = Guid.NewGuid(),
                    Description = json.description,
                    Streams = new List<string>(),
                    devName = scd.DeviceName,
                    User = scd.UserName
                };

                foreach (StreamInfo data in json.streams)
                {
                    cs.Streams.Add(JsonConvert.SerializeObject(data));
                }

                //create the stream for the device table
                CassandraStreamByDevice streamDev = new CassandraStreamByDevice
                {
                    deviceID = devID,
                    StreamID = cs.StreamID,
                    Name = json.name
                };

                var insertBatch = new BatchStatement()
                    .Add(streamTable.Insert(cs))
                    .Add(streambydev.Insert(streamDev));

                //commit the changes to Cassandra
                session.Execute(insertBatch);

                Caching.DeleteKey("getstreams" + scd.DeviceName + scd.UserName);

                //create the new data table to store all the stream data
                string execute = "CREATE TABLE data" + cs.StreamID.ToString().Replace("-", "") + " ( Date text, Tuid timeuuid";
                foreach (StreamInfo data in json.streams)
                {
                    execute += ", " + data.name.Replace(' ', '_') + " " + data.type;
                }
                execute += ", PRIMARY KEY (Date, Tuid) );";

                Logger.Instance.LogData("String to create a new table : " + execute);
                session.Execute( new SimpleStatement(execute));
                
                //if there is a no reply query string close the connection
                if (context.Request.QueryString["noreply"] != null)
                {
                    context.Response.End();
                    return;
                }

                DeviceStream ds = new DeviceStream { description = cs.Description, name = cs.Name, streamid = cs.StreamID.ToString(), units = cs.Units, statistics = null, streams = new List<StreamInfo>() };
                foreach (string data in cs.Streams)
                {
                    ds.streams.Add(JsonConvert.DeserializeObject<StreamInfo>(data));
                }

                //they did want a reply, lets write out the object
                context.Response.ContentType = "application/json; charset=utf-8";
                context.Response.Write(JsonConvert.SerializeObject(ds));
            }
            catch (Exception ex)
            {
                Utilities.LogAndWriteErrorResponse(context, ip + " : " + ex.ToString(), "Unknown Error Creating Stream, If this problem persists please let us know what is failing");
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}