﻿﻿using System;
using System.Web;
using System.Collections.Generic;
using System.Threading;
using System.Globalization;
using System.Linq;
using System.Diagnostics;
using System.IO;
using System.Configuration;
using SensorStreamCore;
using Cassandra;
using Cassandra.Data.Linq;

namespace SensorStream
{
    public sealed class Cluster
    {
        private static readonly Lazy<CassandraCluster> _instance = new Lazy<CassandraCluster>(() => new CassandraCluster(ConfigurationManager.AppSettings["CassandraNode"], ConfigurationManager.AppSettings["CassandraKeySpace"]));

        public static CassandraCluster Instance
        {
            get
            {
                return _instance.Value;
            }
        }
    }
}