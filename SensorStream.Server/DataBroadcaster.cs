﻿using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SensorStream
{
    public class DataBroadcaster
    {
        private readonly static Lazy<DataBroadcaster> _instance = new Lazy<DataBroadcaster>(() => new DataBroadcaster(GlobalHost.ConnectionManager.GetHubContext<DataHub>()));

        private IHubContext _context;
        public static DataBroadcaster Instance
        {
            get
            {
                return _instance.Value;
            }
        }

        public DataBroadcaster(IHubContext context)
        {
            _context = context;
        }

        public void BroadcastData(string stream, string jsonObject, Guid streamID)
        {
            if (DataHub.SubscriptonsByGuid.ContainsKey(streamID))
                _context.Clients.Clients(DataHub.SubscriptonsByGuid[streamID]).newData(stream, jsonObject);

            if (DataHubSockets.clients.ContainsKey(streamID.ToString()))
                DataHubSockets.clients[streamID.ToString()].Broadcast(jsonObject);

        }

    }
}