﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using Enyim.Caching.Configuration;
using System.Net;
using Enyim.Caching.Memcached;
using Enyim.Caching;
using System.IO;
using System.Threading;
namespace SensorStream
{
    public class MemcachedConnection
    {
        private Socket socket;
        TcpClient tc;
        private string host;
        private int port;
        public MemcachedConnection(string nhost, int nport)
        {
            tc = new TcpClient();
            tc.Connect(nhost, nport);
            this.host = nhost;
            port = nport;
            tc.ReceiveTimeout = 5;
            tc.ReceiveBufferSize = 10000;
            tc.NoDelay = true;
            IPHostEntry host = Dns.GetHostEntry(nhost);
            IPAddress ip = host.AddressList[0];
            IPEndPoint ipe = new IPEndPoint(ip, nport);
            socket = new Socket(ipe.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
            socket.NoDelay = true;
            socket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.KeepAlive, true);
            socket.Connect(ipe);
        }

        public void CloseConnection()
        {
            tc.Close();
        }

        private void CheckAndReconnect()
        {
            if (tc.Connected == false)
            {
                tc = new TcpClient(host, port);
                tc.ReceiveBufferSize = 100000;
                tc.ReceiveTimeout = 10;
                tc.NoDelay = true;
            }
        }


        /// <summary>
        /// set the key
        /// </summary>
        /// <param name="key">key to set</param>
        /// <param name="flags">flags for the set</param>
        /// <param name="exptime">time before expiration</param>
        /// <param name="value">value to set for the key</param>
        public void SetKeyValueNoReply(string key, int flags, int exptime, string value)
        {
            key = key.Replace(" ", "-space-");
            try
            {
                string s = "set " + key + " " + flags + " " + exptime + " " + value.Length + " noreply" + " \r\n" + value + "\r\n";
                tc.Client.Send(System.Text.Encoding.ASCII.GetBytes(s));
            }
            catch (Exception) { }
            CheckAndReconnect();
        }


        /// <summary>
        /// set the key
        /// </summary>
        /// <param name="key">key to set</param>
        /// <param name="flags">flags for the set</param>
        /// <param name="exptime">time before expiration</param>
        /// <param name="value">value to set for the key</param>
        /// <returns>response from the server:
        /// "STORED\r\n" or "NOT_STORED\r\n"</returns>
        public string SetKeyValue(string key, int flags, int exptime, string value)
        {
            key = key.Replace(" ", "-space-");
            string reply = "";
            try
            {
                string s = "set " + key + " " + flags + " " + exptime + " " + value.Length + "" + " \r\n" + value + "\r\n";
                tc.Client.Send(System.Text.Encoding.ASCII.GetBytes(s));
                byte[] buffer = new byte[500];
                int count;
                count = tc.Client.Receive(buffer);
                reply = System.Text.Encoding.ASCII.GetString(buffer, 0, count);
            }
            catch (Exception) { }
            CheckAndReconnect();
            return reply;
        }


        /// <summary>
        /// returns the value of the specified key if it existed
        /// </summary>
        /// <param name="key">the key to look for</param>
        /// <returns>
        /// VALUE <key> <flags> <bytes> [<cas unique>]\r\n
        /// <data block>\r\n     
        /// END\r\n
        /// </returns>
        public string GetValue(string key)
        {
            key = key.Replace(" ", "-space-");
            string reply = "";
            try
            {
                string s = "get " + key + "\r\n";
                socket.Send(Encoding.ASCII.GetBytes(s));
                //tc.Client.Send(System.Text.Encoding.ASCII.GetBytes(s));
                byte[] buffer = new byte[tc.Client.ReceiveBufferSize];
                //int count = tc.Client.Receive(buffer);
                int count = socket.Receive(buffer);
                reply = System.Text.Encoding.ASCII.GetString(buffer, 0, count);
                Thread.Sleep(5);
                while (socket.Available > 0)
                {
                    count = socket.Receive(buffer);
                    reply += System.Text.Encoding.ASCII.GetString(buffer, 0, count);
                    Thread.Sleep(5);
                }
                //tc.ReceiveTimeout = 5;
                //while (count > 0)
                //{
                  //  socket.ReceiveTimeout = 1;
                   // count = socket.Receive(buffer);
                   // reply += System.Text.Encoding.ASCII.GetString(buffer, 0, count);
                //}
            }
            catch (Exception) { }
            //CheckAndReconnect();
            //tc.ReceiveTimeout = 5;
            return reply;
        }

        /// <summary>
        /// delete <key> [noreply]\r\n
        /// </summary>
        /// <param name="key">key to delete</param>
        /// <returns>response from the server:
        /// "DELETED\r\n" or "NOT_FOUND\r\n"</returns>
        public string DeleteKeyWithReply(string key)
        {
            key = key.Replace(" ", "-space-");
            string reply = "";
            try
            {
                string s = "delete " + key + "\r\n";
                tc.Client.Send(System.Text.Encoding.ASCII.GetBytes(s));
                byte[] buffer = new byte[500];
                int count = tc.Client.Receive(buffer);
                reply = System.Text.Encoding.ASCII.GetString(buffer, 0, count);
            }
            catch (Exception) { }
            CheckAndReconnect();
            return reply;
        }

        /// <summary>
        /// delete <key> [noreply]\r\n
        /// </summary>
        /// <param name="key">key to delete</param>
        public void DeleteKey(string key)
        {
            key = key.Replace(" ", "-space-");
            try
            {
                string s = "delete " + key + " noreply\r\n";
                tc.Client.Send(System.Text.Encoding.ASCII.GetBytes(s));
            }
            catch (Exception) { }
            CheckAndReconnect();
        }

    }
}
