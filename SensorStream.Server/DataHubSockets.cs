﻿using Microsoft.Web.WebSockets;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SensorStream
{
    public class DataHubSockets : WebSocketHandler
    {
        private string ID;
        private List<string> Subscriptions = new List<string>();

        public static Dictionary<string, WebSocketCollection> clients = new Dictionary<string, WebSocketCollection>();


        public DataHubSockets(string userID)
        {
            ID = userID;
        }

        public override void OnOpen()
        {
        }

        public override void OnClose()
        {
            foreach (string connection in Subscriptions)
            {
                Unsubscribe(connection);
            }
            base.OnClose();
        }

        public override void OnMessage(string message)
        {
            Message m = JsonConvert.DeserializeObject<Message>(message);
            switch (m.type)
            {
                //Subscribe!
                case 0:
                    Subscribe(m.streamID);
                    return;
                //Unsubscribe
                case 1:
                    Unsubscribe(m.streamID);
                    return;

                default:
                    return;
            }
        }

        private void Unsubscribe(string StreamID)
        {
            //parse the given string to a StreamID
            Guid id = Guid.Parse(StreamID);
            if (!Subscriptions.Contains(id.ToString()))
            {
                Subscriptions.Remove(id.ToString());
                //make sure we were still subscribed
                if (clients.ContainsKey(id.ToString()))
                {
                    //remove this socket from the connections
                    clients[id.ToString()].Remove(this);

                    //remove the collection if it is empty
                    if (clients[id.ToString()].Count == 0)
                        clients.Remove(id.ToString());
                }
            }
        }
        
        public void Subscribe(string StreamID)
        {
            //parse the given string to a StreamID
            Guid id = Guid.Parse(StreamID);
            if (!Subscriptions.Contains(id.ToString()))
            {
                Subscriptions.Add(id.ToString());
                //check to see if this connection has subscriptions already
                if (clients.ContainsKey(id.ToString()))
                {
                    //the list does exist, add this new Guid to the list
                    clients[id.ToString()].Add(this);
                }
                else
                {
                    //the list doesnt exist already, create it and add it to the clients list
                    WebSocketCollection newCollection = new WebSocketCollection();
                    newCollection.Add(this);
                    clients.Add(id.ToString(), newCollection);
                }
            }
        }
    }

    public class Message
    {
        public int type;
        public string streamID;
    }
}