﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Cassandra.Data.Linq;
using Cassandra;
using Newtonsoft.Json;

namespace SensorStream
{
    public class User
    {
        [PartitionKey]
        public string liveid;

        [ClusteringKey(0)]
        public string sensorstreamid;

        public string accesstoken;

        public int access;
    }
}