﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Configuration;
using System.Threading;
using System.Threading.Tasks;

namespace SensorStream
{
    public sealed class MemcachedConnectionPool
    {
        private readonly static Lazy<MemcachedConnectionPool> _instance = new Lazy<MemcachedConnectionPool>(() => new MemcachedConnectionPool(int.Parse(ConfigurationManager.AppSettings["MemcachedConnections"]), ConfigurationManager.AppSettings["MemcachedServer"], int.Parse(ConfigurationManager.AppSettings["MemcachedPort"])));

        public static MemcachedConnectionPool Instance
        {
            get 
            {
                return _instance.Value;
            }
        }

        ConcurrentBag<MemcachedConnection> connections = new ConcurrentBag<MemcachedConnection>();

        public MemcachedConnectionPool(int numThreads, string host, int port)
        {
            Parallel.For(0, numThreads, (i) =>
            {
                connections.Add(new MemcachedConnection(host, port));
            });
        }

        public MemcachedConnection getConnection()
        {
            MemcachedConnection mc = null;
            while (true)
            {
                if (connections.Count > 0)
                {

                    if (connections.TryTake(out mc))
                        return mc;
                }
            }
        }

        public void ReturnConnection(MemcachedConnection connection)
        {
            connections.Add(connection);
        }
    }
}
