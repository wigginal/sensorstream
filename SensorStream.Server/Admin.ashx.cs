﻿using Cassandra;
using Cassandra.Data.Linq;
using SensorStreamCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace SensorStream
{
    /// <summary>
    /// Summary description for Admin
    /// </summary>
    public class Admin : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            if (context.Request.QueryString["updateStatistics"] != null)
            {
                //get the Cassandra session and the device contexts
                ISession session = Cluster.Instance.Session;
                
                //get the table we need to search through for the device and then the streams
                Table<CassandraStream> streamTable = SessionExtensions.GetTable<CassandraStream>(session);
                int count = 0;
                
                foreach (CassandraStream strm in streamTable.Execute())
                {
                    try
                    {
                        Logger.Instance.LogData("Rebuilding Stream " + strm.Name + "  -  " + strm.StreamID);
                        Statistics.Instance.RebuildStatistics(strm);
                        count++;
                    }
                    catch (Exception ex)
                    {
                        Logger.Instance.LogData(ex.ToString());
                    }
                }

                context.Response.ContentType = "text/plain";
                context.Response.Write("Updated " + count + " Statistics");
            } 
            if (context.Request.QueryString["flushdevicecache"] != null)
            {
                Caching.DeleteKey("getdevices");
                context.Response.ContentType = "text/plain";
                context.Response.Write("cleared cache");
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}