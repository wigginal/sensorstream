﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SensorStream
{
    /// <summary>
    /// Summary description for Stats
    /// </summary>
    public class Stats : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            if (context.Request.QueryString["streamID"] != null)
            {
                string streamID = context.Request.QueryString["streamID"];

                context.Response.ContentType = "application/json";
                context.Response.Write(JsonConvert.SerializeObject(Statistics.Instance.GetStatistic(Guid.Parse(streamID))));
                
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}