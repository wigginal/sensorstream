﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;

namespace SensorStream
{
    /// <summary>
    /// Summary description for notifier
    /// </summary>
    public class notifier : IHttpHandler
    {
        public class MessageInfo
        {
            public List<String> To;
            public string Subject;
            public string Body;
        }


        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            string message = Utilities.GetPOSTString(context);
            Logger.Instance.LogData(message);
            try
            {
                MessageInfo send = JsonConvert.DeserializeObject<MessageInfo>(message);
                MailMessage messageToSend = new MailMessage();
                messageToSend.From = new MailAddress("email@localhost");

                foreach (string s in send.To)
                    messageToSend.To.Add(new MailAddress(s));

                messageToSend.Subject = send.Subject;
                messageToSend.Body = send.Body;
                SmtpClient client = new SmtpClient();
                client.Send(messageToSend);
            }
            catch (Exception ex)
            {
                Logger.Instance.LogData(ex.ToString());
            }
            context.Response.StatusCode = 200;
            context.Response.Write("Success!");
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}