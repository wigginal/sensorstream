﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Web.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace SensorStream
{
    /// <summary>
    /// Summary description for DataSocket
    /// </summary>
    public class DataSocket : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            if (context.IsWebSocketRequest)
                context.AcceptWebSocketRequest(new DataHubSockets(Guid.NewGuid().ToString()));
            else
                context.Response.StatusCode = 400;
        }
        
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}