﻿using Cassandra;
using Cassandra.Data.Linq;
using Microsoft.ApplicationServer.Caching;
using Newtonsoft.Json;
using SensorStreamCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace SensorStream
{
    public class Utilities
    {
        public static bool IsIntegerType(string units)
        {
            switch (units)
            {
                case "int":
                case "bigint":
                case "counter":
                case "decimal":
                case "float":
                case "double":
                    return true;
                default:
                    return false;
            }
        }

        public static void LogAndWriteErrorResponse(HttpContext context, string logMessage, string errorMessage)
        {
            Logger.Instance.LogData(logMessage);
            context.Response.TrySkipIisCustomErrors = true;
            context.Response.StatusCode = 400;
            context.Response.ContentType = "application/json; charset=utf-8";
            context.Response.Write(Utilities.CreateJSONErrorString(errorMessage));
            context.ApplicationInstance.CompleteRequest();
        }

        public static string CreateJSONErrorString(string p)
        {
            return "{ \"error\" : \"" + p + "\" }";
        }

        public static void SendHeaders(HttpContext context)
        {
            context.Response.Headers.Add("Access-Control-Allow-Origin", "*");
        }

        public static bool IsValidType(string datatype)
        {
            switch (datatype)
            {
                case "int":
                case "ascii":
                case "bigint":
                case "blob":
                case "boolean":
                case "counter":
                case "decimal":
                case "double":
                case "float":
                case "text":
                case "timestamp":
                case "uuid":
                case "timeuuid":
                case "varchar":
                case "varint":
                    break;
                default:
                    return false;
            }
            return true;
        }



        public static object ParseValue(string type, string value)
        {
            object j;

            switch (type)
            {

                case "int":
                    j = int.Parse(value);
                    break;

                case "bigint":
                case "counter":
                    j = long.Parse(value);
                    break;

                case "blob":
                    j = Convert.FromBase64String(value);
                    break;

                case "decimal":
                case "float":
                    j = float.Parse(value);
                    break;

                case "double":
                    j = double.Parse(value);
                    break;

                case "timestamp":
                    j = DateTimeOffset.Parse(value);
                    break;

                case "boolean":
                    j = bool.Parse(value);
                    break;

                case "varchar":
                case "ascii":
                case "text":
                    j = value;
                    break;

                case "uuid":
                case "timeuuid":
                    j = Guid.Parse(value);
                    break;
                default:
                    return null;
            }
            return j;
        }

        public static string GetRequestIP(HttpContext context)
        {
            String ip = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            if (string.IsNullOrEmpty(ip))
            {
                ip = context.Request.ServerVariables["REMOTE_ADDR"];
            }
            return ip;
        }

        public static double GetEpochTime()
        {
            DateTime dtEpochStartTime = new DateTime(1970, 1, 1, 0, 0, 0, 0);
            DateTime dtCurTime = DateTime.Now;
            TimeSpan ts = dtCurTime.ToLocalTime().Subtract(dtEpochStartTime);
            double epochtime;
            epochtime = ts.Ticks / 10000;
            return epochtime;
        }


        public static string GetPOSTString(HttpContext context)
        {
            string jsonInput;
            using (var reader = new StreamReader(context.Request.InputStream))
            {
                jsonInput = reader.ReadToEnd();
            }
            return jsonInput;
        }

        public static bool DeleteStream(HttpContext context, String ip, Guid devID, Guid streamID)
        {
            //get a cassandra session and contexts for devices and streams
            ISession session = Cluster.Instance.Session;

            //get the tables needed to verify the device ID and create a new streams
            Table<CassandraStream> streamTable = SessionExtensions.GetTable<CassandraStream>(session);
            Table<CassandraStreamByDevice> streambydev = SessionExtensions.GetTable<CassandraStreamByDevice>(session);

            //get the stream by the ids we were given and make sure we get a match with the deviceID and the streamID
            CassandraStream cs = null;
            CassandraStreamByDevice sbd = streambydev.FirstOrDefault(dev => dev.deviceID == devID && dev.StreamID == streamID).Execute();

            if (sbd == null)
            {
                Logger.Instance.LogData(ip + " : Simple Stream : Invalid streamID/devID Pair ----- ");
                return false;
            }


            cs = streamTable.FirstOrDefault(dev => dev.StreamID == streamID).Execute();
            CqlDelete statement = streambydev.Where(dev => dev.deviceID == devID && dev.StreamID == streamID).Delete();
            Logger.Instance.LogData(statement.ToString());
            session.Execute(statement);
            if (cs != null){
                statement = streamTable.Where(dev => dev.StreamID == streamID).Delete();
                Logger.Instance.LogData(statement.ToString());
                session.Execute(statement);
            }
            //Drop the data table! Robert'); DROP TABLE Students;--
            string execute = "DROP TABLE data" + streamID.ToString().Replace("-", "") + ";";
            try
            {
                session.Execute(execute);
                Logger.Instance.LogData("dropped the table...");
            }
            catch (Exception) {
                Logger.Instance.LogData("tried dropping: " + execute.ToString());
            }

            Caching.DeleteKey("getstreams" + cs.devName + cs.User);
            Statistics.Instance.DeleteStream(streamID);

            return true;
        }

        internal static bool StreamDeviceMatch(Guid devID, Guid StreamID)
        {
            if (CassandraCache.Instance.StreamToDeviceID.ContainsKey(StreamID))
            {
                Guid matchedVal;
                if (CassandraCache.Instance.StreamToDeviceID.TryGetValue(StreamID, out matchedVal))
                {
                    if (matchedVal == devID)
                        return true;
                    else
                        return false;
                }
            }
            ISession session = Cluster.Instance.Session;
            Table<CassandraStreamByDevice> streambydev = SessionExtensions.GetTable<CassandraStreamByDevice>(session);
            CassandraStreamByDevice sbd = streambydev.FirstOrDefault(dev => dev.deviceID == devID && dev.StreamID == StreamID).Execute();
            if (sbd != null)
            {
                CassandraCache.Instance.StreamToDeviceID.TryAdd(StreamID, devID);
                return true;
            }
            return false;

        }

        public static Dictionary<string, string> GetStreamColumns(Guid StreamID)
        {
            Dictionary<string, string> columns;
            if (CassandraCache.Instance.complexColumns.ContainsKey(StreamID))
            {
                if (CassandraCache.Instance.complexColumns.TryGetValue(StreamID, out columns))
                {
                    return columns;
                }
            }

            ISession session = Cluster.Instance.Session;

            Table<CassandraStream> streams = SessionExtensions.GetTable<CassandraStream>(session);
            CassandraStream stream = streams.First(strm => strm.StreamID == StreamID).Execute();

            //parse strings from sbd.streams into objects to compare with
            columns = new Dictionary<string, string>();
            foreach (string streamNames in stream.Streams)
            {
                StreamInfo sci = JsonConvert.DeserializeObject<StreamInfo>(streamNames);
                columns.Add(sci.name, sci.type);
            }
            CassandraCache.Instance.complexColumns.TryAdd(StreamID, columns);
            return columns;
        }

        internal static CassandraStream GetStream(Guid StreamID)
        {
            CassandraStream cs = null;
            if (CassandraCache.Instance.Streams.ContainsKey(StreamID))
            {
                if (CassandraCache.Instance.Streams.TryGetValue(StreamID, out cs))
                {
                    return cs;
                }
            }
            ISession session = Cluster.Instance.Session;
            Table<CassandraStream> streams = SessionExtensions.GetTable<CassandraStream>(session);

            cs = streams.FirstOrDefault(strm => strm.StreamID == StreamID).Execute();
            if (cs != null)
            {
                CassandraCache.Instance.Streams.TryAdd(StreamID, cs);
            }
            return cs;
        }

        internal static string ParseValueGetString(string type, string value)
        {
            string j;

            switch (type)
            {

                case "int":
                    j = int.Parse(value).ToString();
                    break;

                case "bigint":
                case "counter":
                    j = long.Parse(value).ToString();
                    break;

                case "blob":
                    j = "textAsBlob('" + value.ToString() + "')";
                    break;

                case "decimal":
                case "float":
                    j = float.Parse(value).ToString();
                    break;

                case "double":
                    j = double.Parse(value).ToString();
                    break;

                case "timestamp":
                    j = DateTimeOffset.Parse(value).ToString();
                    break;

                case "boolean":
                    j = "'" + bool.Parse(value).ToString() + "'";
                    break;

                case "varchar":
                case "ascii":
                case "text":
                    j = "'" + value + "'";
                    break;

                case "uuid":
                case "timeuuid":
                    j = Guid.Parse(value).ToString();
                    break;
                default:
                    return null;
            }
            return j;
        }
    }
}