﻿using Cassandra;
using Cassandra.Data.Linq;
using Newtonsoft.Json;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SensorStreamCore;

namespace SensorStream
{
    public class Statistics
    {
        public Dictionary<string, OverallStreamStatistics> fullstats = new Dictionary<string, OverallStreamStatistics>();
        public Dictionary<string, Dictionary<string, HourlyFullStreamStatistics>> hourlystats = new Dictionary<string, Dictionary<string, HourlyFullStreamStatistics>>();
        public ConcurrentQueue<Tuple<CassandraStream, OverallStreamStatistics>> queueForStats = new ConcurrentQueue<Tuple<CassandraStream, OverallStreamStatistics>>();
        static object hourlyupdatelock = new object();
        static object overallupdatelock = new object();

        private static readonly Lazy<Statistics> _instance = new Lazy<Statistics>(() => new Statistics());

        public static Statistics Instance
        {
            get
            {
                return _instance.Value;
            }
        }

        public Statistics()
        {
        }

        public void UpdateStatistics(CassandraStream stream, Data data)
        {
            //create the statistics for this update
            OverallStreamStatistics oss = CreateNewOverallStatistics(stream, data);

            try
            {
                //update the overall stats
                UpdateOverallStatistics(stream, oss);
            }
            catch (Exception ex)
            {
                Logger.Instance.LogData("error in overall stats, " + ex.ToString());
            }

            try
            {
                UpdateHourlyStatistics(stream, oss);
            }
            catch (Exception ex)
            {
                Logger.Instance.LogData("error in hourly stats, " + ex.ToString());
            }
        }

        private void UpdateHourlyStatistics(CassandraStream stream, OverallStreamStatistics oss)
        {
            //get the hour for the current data point
            HourlyFullStreamStatistics hfss = new HourlyFullStreamStatistics { streamid = oss.streamid, count = oss.count, streamstats = new Dictionary<string, IndividualStreamStatistics>(), timeindex = oss.firstentry.ToString("yyyyMMddHH") };

            foreach (var streamstat in oss.streamstats)
            {
                hfss.streamstats.Add(streamstat.Key, new IndividualStreamStatistics { count = streamstat.Value.count, id = streamstat.Value.id, m2 = streamstat.Value.m2, max = streamstat.Value.max, mean = streamstat.Value.mean, min = streamstat.Value.min, stddev = streamstat.Value.stddev, variance = streamstat.Value.variance });
            }

            lock (hourlyupdatelock)
            {
                //get the hour into the hourly stats cache
                CheckOrGetHourlyData(stream, hfss.timeindex);

                //Add this data in
                MergeHourlyStatisticsWithCurrent(stream, hfss);

                //write back the data to Cassandra
                WriteBackHourlyData(stream.StreamID, hfss.timeindex);
            }
        }

        internal void UpdateStatistics(Guid StreamID, Data data)
        {
            //get the stream, and then call the update statistics
            ISession session = Cluster.Instance.Session;
            Table<CassandraStream> stream = SessionExtensions.GetTable<CassandraStream>(session);
            CassandraStream cs = stream.FirstOrDefault(st => st.StreamID == StreamID).Execute();

            //if there was no stream, then something is very screwed up because this shouldn't have been inserted
            if (cs == null)
                return;

            //update stats
            UpdateStatistics(cs, data);
        }

        private void UpdateOverallStatistics(CassandraStream stream, OverallStreamStatistics oss)
        {
            lock (overallupdatelock)
            {
                //check to see if the stats exist in memory
                if (!fullstats.ContainsKey(stream.StreamID.ToString()))
                {

                    fullstats.Add(stream.StreamID.ToString(), CreateCopyOverallStatistics(oss));

                    //collect stats from Cassandra
                    OverallStreamStatistics cassStats = GetOverallStatsFromCassandra(stream.StreamID.ToString());
                    //merge them into our current values
                    if (cassStats != null)
                    {
                        MergeOverallStatistics(stream, cassStats);
                    }
                }
                else
                {
                    //update the overalls with this data point
                    MergeOverallStatistics(stream, oss);
                }
            }
        }

        private void CheckOrGetHourlyData(CassandraStream stream, string timeid)
        {
            //if the key exists already return
            if (hourlystats.ContainsKey(stream.StreamID.ToString()))
            {
                if (hourlystats[stream.StreamID.ToString()].ContainsKey(timeid))
                {
                    return;
                }
            }
            else
            {
                //we need to add this into the cache so we can add in the hourly stats
                hourlystats.Add(stream.StreamID.ToString(), new Dictionary<string, HourlyFullStreamStatistics>());
            }

            //go to Cassandra and get it
            ISession session = Cluster.Instance.Session;
            Table<CassandraHourlyFullStreamStatistics> hstatsTable = SessionExtensions.GetTable<CassandraHourlyFullStreamStatistics>(session);
            CassandraHourlyFullStreamStatistics chfss = hstatsTable.FirstOrDefault(st => st.streamid == stream.StreamID.ToString() && st.timeindex == timeid).Execute();

            if (chfss != null)
            {
                HourlyFullStreamStatistics hfss = new HourlyFullStreamStatistics { count = chfss.count, streamid = chfss.streamid, timeindex = chfss.timeindex, streamstats = new Dictionary<string, IndividualStreamStatistics>() };
                hfss.streamstats = JsonConvert.DeserializeObject<Dictionary<string, IndividualStreamStatistics>>(chfss.streamstats);
                hourlystats[stream.StreamID.ToString()].Add(timeid, hfss);
            }
            else
            {
                //there were no stats to speak of, insert a blank one...
                hourlystats[stream.StreamID.ToString()].Add(timeid, CreateNewEmptyHourlyStatistics(stream, timeid));
            }
        }

        private void WriteBackHourlyData(Guid streamid, string timeid)
        {
            ISession session = Cluster.Instance.Session;
            Table<CassandraHourlyFullStreamStatistics> hstatsTable = SessionExtensions.GetTable<CassandraHourlyFullStreamStatistics>(session);
            HourlyFullStreamStatistics hfss;

            //lock the hourly stuff and write it back
            hfss = hourlystats[streamid.ToString()][timeid];
            if (hfss != null)
            {
                CassandraHourlyFullStreamStatistics chfss = new CassandraHourlyFullStreamStatistics { count = hfss.count, streamid = hfss.streamid, timeindex = hfss.timeindex, streamstats = JsonConvert.SerializeObject(hfss.streamstats) };
                hstatsTable.Insert(chfss).Execute();
            }
        }

        private void MergeHourlyStatisticsWithCurrent(CassandraStream stream, HourlyFullStreamStatistics hss)
        {
            HourlyFullStreamStatistics currentVal = hourlystats[stream.StreamID.ToString()][hss.timeindex];
            hourlystats[stream.StreamID.ToString()][hss.timeindex] = MergeHourlyStatistics(currentVal, hss);
        }

        private void MergeOverallStatistics(CassandraStream stream, OverallStreamStatistics oss)
        {
            //get the value
            OverallStreamStatistics currentVal = fullstats[stream.StreamID.ToString()];
            currentVal.count += oss.count;

            //set the new time
            if (currentVal.lastentry < oss.lastentry)
            {
                currentVal.lastentry = new DateTimeOffset(oss.lastentry.Ticks, oss.lastentry.Offset);
            }
            if (currentVal.firstentry > oss.firstentry)
            {
                currentVal.firstentry = new DateTimeOffset(oss.firstentry.Ticks, oss.firstentry.Offset);
            }

            //update each of the stream stats
            foreach (var istream in oss.streamstats)
            {
                IndividualStreamStatistics isss = currentVal.streamstats[istream.Key];
                currentVal.streamstats[istream.Key] = MergeIndividualStreamStats(isss, istream.Value);
            }

            //add it back in
            fullstats[stream.StreamID.ToString()] = currentVal;
        }

        private IndividualStreamStatistics MergeIndividualStreamStats(IndividualStreamStatistics stats1, IndividualStreamStatistics stats2)
        {
            IndividualStreamStatistics retstats = new IndividualStreamStatistics()
            {
                count = 0,
                max = 0,
                min = 0,
                id = stats1.id,
                m2 = 0,
                mean = 0,
                stddev = 0,
                variance = 0
            };
            if (stats1.count == 0 && stats2.count > 0)
            {
                retstats.count = stats2.count;
                retstats.m2 = stats2.m2;
                retstats.max = stats2.max;
                retstats.mean = stats2.mean;
                retstats.min = stats2.min;
                retstats.stddev = stats2.stddev;
                retstats.variance = stats2.variance;
            }
            else if (stats1.count > 0 && stats2.count == 0)
            {
                retstats.count = stats1.count;
                retstats.m2 = stats1.m2;
                retstats.max = stats1.max;
                retstats.mean = stats1.mean;
                retstats.min = stats1.min;
                retstats.stddev = stats1.stddev;
                retstats.variance = stats1.variance;
            }
            else if (stats2.count > 0 && stats1.count > 0)
            {
                double delta = stats2.mean - stats1.mean;
                retstats.mean = (double)(stats1.count * stats1.mean + stats2.count * stats2.mean) / (double)(stats2.count + stats1.count);
                retstats.m2 = stats1.m2 + stats2.m2 + (delta * delta) * ((double)(stats2.count * stats1.count) / (double)(stats1.count + stats2.count));
                retstats.count = stats1.count + stats2.count;
                retstats.variance = retstats.m2 / (double)(retstats.count);
                retstats.stddev = Math.Sqrt(retstats.variance);

                //check min/max
                retstats.max = stats1.max;
                if (stats2.max > stats1.max)
                {
                    retstats.max = stats2.max;
                }

                retstats.min = stats1.min;
                if (stats2.min < stats1.min)
                {
                    retstats.min = stats2.min;
                }
            }
            return retstats;
        }

        private HourlyFullStreamStatistics CreateNewEmptyHourlyStatistics(CassandraStream stream, string timeid)
        {
            HourlyFullStreamStatistics statsadd = new HourlyFullStreamStatistics() { count = 0, streamid = stream.StreamID.ToString(), streamstats = new Dictionary<string, IndividualStreamStatistics>(), timeindex = timeid };
            foreach (string sistring in stream.Streams)
            {
                StreamInfo si = JsonConvert.DeserializeObject<StreamInfo>(sistring);
                statsadd.streamstats.Add(si.name, new IndividualStreamStatistics() { count = 0, id = si.name, max = 0, min = 0, m2 = 0, mean = 0, stddev = 0, variance = 0 });

            }
            return statsadd;
        }

        private HourlyFullStreamStatistics MergeHourlyStatistics(HourlyFullStreamStatistics current, HourlyFullStreamStatistics newest)
        {
            HourlyFullStreamStatistics hfss = new HourlyFullStreamStatistics()
            {
                count = current.count + newest.count,
                streamid = current.streamid,
                streamstats = new Dictionary<string, IndividualStreamStatistics>(),
                timeindex = current.timeindex
            };

            //update each of the stream stats
            foreach (var istream in current.streamstats)
            {
                hfss.streamstats.Add(istream.Key, MergeIndividualStreamStats(current.streamstats[istream.Key], newest.streamstats[istream.Key]));
            }
            return hfss;
        }

        private OverallStreamStatistics CreateNewOverallStatistics(CassandraStream stream, Data data)
        {
            OverallStreamStatistics statsadd = new OverallStreamStatistics()
            {
                count = 1,
                firstentry = (DateTimeOffset)data.parsedtime,
                lastentry = (DateTimeOffset)data.parsedtime,
                streamid = stream.StreamID.ToString(),
                streamstats = new Dictionary<string, IndividualStreamStatistics>()
            };

            //create the 
            foreach (string sistring in stream.Streams)
            {
                StreamInfo si = JsonConvert.DeserializeObject<StreamInfo>(sistring);
                if (data.values.ContainsKey(si.name))
                {
                    if (Utilities.IsIntegerType(si.type))
                    {
                        double value = double.Parse(data.values[si.name]);

                        statsadd.streamstats.Add(si.name, new IndividualStreamStatistics() { count = 1, id = si.name, max = value, min = value, m2 = 0, mean = value, stddev = 0, variance = 0 });
                    }
                    else
                    {
                        statsadd.streamstats.Add(si.name, new IndividualStreamStatistics() { count = 1, id = si.name, max = 0, min = 0, m2 = 0, mean = 0, stddev = 0, variance = 0 });
                    }
                }
                else
                {
                    statsadd.streamstats.Add(si.name, new IndividualStreamStatistics() { count = 0, id = si.name, max = 0, min = 0, m2 = 0, mean = 0, stddev = 0, variance = 0 });
                }
            }
            return statsadd;
        }


        private OverallStreamStatistics CreateCopyOverallStatistics(OverallStreamStatistics oss)
        {
            OverallStreamStatistics statsadd = new OverallStreamStatistics()
            {
                count = oss.count,
                firstentry = new DateTime(oss.firstentry.Ticks),
                lastentry = new DateTime(oss.lastentry.Ticks),
                streamid = oss.streamid,
                streamstats = new Dictionary<string, IndividualStreamStatistics>()
            };

            //create the 
            foreach (var stats in oss.streamstats)
            {
                statsadd.streamstats.Add(stats.Key, new IndividualStreamStatistics { count = stats.Value.count, id = stats.Value.id, m2 = stats.Value.m2, max = stats.Value.max, mean = stats.Value.mean, min = stats.Value.min, stddev = stats.Value.stddev, variance = stats.Value.variance });
            }
            return statsadd;
        }


        private OverallStreamStatistics GetOverallStatsFromCassandra(string streamid)
        {
            //get the device and stream context so we can retrieve the data and information on the device
            ISession session = Cluster.Instance.Session;
            Table<CassandraHourlyFullStreamStatistics> hstatsTable = SessionExtensions.GetTable<CassandraHourlyFullStreamStatistics>(session);

            long min = 0, max = 0;
            HourlyFullStreamStatistics completeStats = null;
            HourlyFullStreamStatistics hfss = null;
            foreach (var chfss in hstatsTable.Where(st => st.streamid == streamid).Execute())
            {
                hfss = new HourlyFullStreamStatistics { count = chfss.count, streamid = chfss.streamid, timeindex = chfss.timeindex, streamstats = new Dictionary<string, IndividualStreamStatistics>() };
                hfss.streamstats = JsonConvert.DeserializeObject<Dictionary<string, IndividualStreamStatistics>>(chfss.streamstats);
                if (min == 0)
                {
                    min = long.Parse(chfss.timeindex);
                    max = min;
                    completeStats = hfss;
                    continue;
                }

                long test = long.Parse(chfss.timeindex);
                //check if there is a newer or older hour
                if (test < min)
                    min = test;
                if (test > max)
                    max = test;

                completeStats = MergeHourlyStatistics(completeStats, hfss);
                //combine all these stats into one large thing

            }
            if (completeStats == null)
                return null;

            return new OverallStreamStatistics()
            {
                count = (long)completeStats.count,
                firstentry = new DateTimeOffset(int.Parse(min.ToString().Substring(0, 4)), int.Parse(min.ToString().Substring(4, 2)), int.Parse(min.ToString().Substring(6, 2)), int.Parse(min.ToString().Substring(8, 2)), 0, 0, new TimeSpan()),
                lastentry = new DateTimeOffset(int.Parse(max.ToString().Substring(0, 4)), int.Parse(max.ToString().Substring(4, 2)), int.Parse(max.ToString().Substring(6, 2)), int.Parse(max.ToString().Substring(8, 2)), 0, 0, new TimeSpan()),
                streamid = streamid,
                streamstats = completeStats.streamstats
            };
        }

        public OverallStreamStatistics GetStatistic(Guid streamID)
        {
            if (fullstats.ContainsKey(streamID.ToString()))
            {
                return fullstats[streamID.ToString()];
            }
            else
            {

                //collect stats from Cassandra
                OverallStreamStatistics cassStats = GetOverallStatsFromCassandra(streamID.ToString());
                //merge them into our current values
                if (cassStats != null)
                {
                    lock (overallupdatelock)
                    {
                        if (!fullstats.ContainsKey(streamID.ToString()))
                            fullstats.Add(streamID.ToString(), cassStats);
                    }
                    return cassStats;
                }

            }
            return new OverallStreamStatistics();
        }

        internal void RebuildStatistics(CassandraStream strm)
        {
            //create a new overall stats
            OverallStreamStatistics oss = new OverallStreamStatistics { count = 0, streamid = strm.StreamID.ToString(), streamstats = new Dictionary<string, IndividualStreamStatistics>() };
            Dictionary<string, StreamInfo> streamList = new Dictionary<string, StreamInfo>();

            //start getting all the data in chunks of 10K
            int iterationSize = 10000;
            foreach (string sistring in strm.Streams)
            {
                StreamInfo si = JsonConvert.DeserializeObject<StreamInfo>(sistring);
                streamList.Add(si.name, si);
                oss.streamstats.Add(si.name, new IndividualStreamStatistics() { count = 0, id = si.name, max = 0, min = 0, m2 = 0, mean = 0, stddev = 0, variance = 0 });
                if (si.type.ToLower() == "blob")
                {
                    iterationSize = 400;
                }
            }

            ISession session = Cluster.Instance.Session;
            Table<CassandraHourlyFullStreamStatistics> hfsstable = SessionExtensions.GetTable<CassandraHourlyFullStreamStatistics>(session);
            Batch batchInserts = session.CreateBatch();
            hfsstable.Where(st => st.streamid == strm.StreamID.ToString()).Delete().Execute();

            string exec = "SELECT * FROM data" + strm.StreamID.ToString().Replace("-", "") + " LIMIT " + iterationSize + ";";


            Statement ps = new SimpleStatement(exec);

            Logger.Instance.LogData("Executing " + exec);
            //execute the statement
            RowSet rs = session.Execute(ps);
            bool first = true;
            bool done = false;
            bool additionalRowQuery = false;
            Guid lastTuid = new Guid();
            HourlyFullStreamStatistics hfss = null;
            CassandraHourlyFullStreamStatistics chfss = null;
            int saves = 0;
            while (!done)
            {
                string lastDate = "";
                int count = 0;

                foreach (Row row in rs.GetRows())
                {
                    if (hfss != null)
                    {
                        //if we are now working on a different one, save the last and create a new one
                        if (hfss.timeindex != row["date"].ToString())
                        {
                            chfss = new CassandraHourlyFullStreamStatistics
                            {
                                count = hfss.count,
                                streamid = hfss.streamid,
                                timeindex = hfss.timeindex,
                                streamstats = JsonConvert.SerializeObject(hfss.streamstats)
                            };
                            batchInserts.Append(hfsstable.Insert(chfss));
                            saves++;
                            if (saves >= 10)
                            {
                                batchInserts.Execute();
                                saves = 0;
                                batchInserts = session.CreateBatch();
                            }
                            hfss = new HourlyFullStreamStatistics
                            {
                                count = 0,
                                streamid = strm.StreamID.ToString(),
                                streamstats = new Dictionary<string, IndividualStreamStatistics>(),
                                timeindex = row["date"].ToString()
                            };
                            foreach (var streamstat in oss.streamstats)
                            {
                                hfss.streamstats.Add(streamstat.Key, new IndividualStreamStatistics { count = 0, id = streamstat.Value.id, m2 = 0, max = 0, mean = 0, min = 0, stddev = 0, variance = 0 });
                            }
                        }
                    }

                    count++;
                    oss.count++;
                    lastDate = row["date"].ToString();
                    lastTuid = Guid.Parse(row["tuid"].ToString());
                    if (first)
                    {
                        hfss = new HourlyFullStreamStatistics
                        {
                            count = 1,
                            streamid = strm.StreamID.ToString(),
                            streamstats = new Dictionary<string, IndividualStreamStatistics>(),
                            timeindex = row["date"].ToString()
                        };
                        foreach (var streamstat in oss.streamstats)
                        {
                            hfss.streamstats.Add(streamstat.Key, new IndividualStreamStatistics { count = 0, id = streamstat.Value.id, m2 = 0, max = 0, mean = 0, min = 0, stddev = 0, variance = 0 });
                        }

                        oss.firstentry = GuidGenerator.GetDateTimeOffset(lastTuid);
                        oss.lastentry = GuidGenerator.GetDateTimeOffset(lastTuid);
                        foreach (var istream in hfss.streamstats)
                        {
                            if (!String.IsNullOrEmpty(row[istream.Key.Replace(" ", "_").ToLower()].ToString()))
                            {
                                if (Utilities.IsIntegerType(streamList[istream.Key].type))
                                {
                                    Double value = Double.Parse(row[istream.Key.Replace(" ", "_").ToLower()].ToString());
                                    if (istream.Value.count == 0)
                                    {
                                        istream.Value.count++;
                                        istream.Value.min = value;
                                        istream.Value.max = value;
                                        istream.Value.mean = value;
                                    }
                                    else
                                    {
                                        double delta = value - istream.Value.mean;
                                        istream.Value.mean = istream.Value.mean + delta / istream.Value.count;
                                        istream.Value.m2 = istream.Value.m2 + delta * (value - istream.Value.mean);
                                        istream.Value.variance = istream.Value.m2 / (istream.Value.count - 1);

                                        if (istream.Value.min > value)
                                            istream.Value.min = value;
                                        if (istream.Value.max < value)
                                            istream.Value.max = value;
                                    }
                                }
                                else
                                {
                                    istream.Value.count++;
                                }
                            }
                        }
                        first = false;
                    }
                    else
                    {
                        //update the overall stats
                        DateTimeOffset dt = GuidGenerator.GetDateTimeOffset(lastTuid);
                        if (oss.lastentry < dt)
                        {
                            oss.lastentry = dt;
                        }

                        //is it early
                        if (oss.firstentry > dt)
                        {
                            oss.firstentry = dt;
                        }

                        hfss.count++;
                        foreach (var istream in hfss.streamstats)
                        {
                            if (!String.IsNullOrEmpty(row[istream.Key.Replace(" ", "_").ToLower()].ToString()))
                            {
                                if (Utilities.IsIntegerType(streamList[istream.Key].type))
                                {
                                    Double value = Double.Parse(row[istream.Key.Replace(" ", "_").ToLower()].ToString());
                                    if (istream.Value.count == 0)
                                    {
                                        istream.Value.count++;
                                        istream.Value.min = value;
                                        istream.Value.max = value;
                                        istream.Value.mean = value;
                                    }
                                    else
                                    {
                                        double delta = value - istream.Value.mean;
                                        istream.Value.mean = istream.Value.mean + delta / istream.Value.count;
                                        istream.Value.m2 = istream.Value.m2 + delta * (value - istream.Value.mean);
                                        istream.Value.variance = istream.Value.m2 / (istream.Value.count - 1);

                                        if (istream.Value.min > value)
                                            istream.Value.min = value;
                                        if (istream.Value.max < value)
                                            istream.Value.max = value;
                                    }
                                }
                                else
                                {
                                    istream.Value.count++;
                                }
                            }
                        }
                    }
                }

                Logger.Instance.LogData("Retrieved " + count + " Columns");
                if (!additionalRowQuery && count < iterationSize)
                {
                    done = true;
                }
                else if (count == iterationSize)
                {
                    exec = "SELECT * FROM data" + strm.StreamID.ToString().Replace("-", "") + " where date = '" + lastDate + "' and tuid > " + lastTuid.ToString() + " LIMIT " + iterationSize + ";";
                    ps = new SimpleStatement(exec);
                    rs = session.Execute(ps);
                    additionalRowQuery = true;
                }
                else if (additionalRowQuery && count < iterationSize)
                {
                    //go to the next row and higher!
                    exec = "SELECT * FROM data" + strm.StreamID.ToString().Replace("-", "") + " where token(date) > token('" + lastDate + "') LIMIT " + iterationSize + ";";
                    ps = new SimpleStatement(exec);
                    rs = session.Execute(ps);
                    additionalRowQuery = false;
                }
                else
                {
                    Logger.Instance.LogData("Iterator Problems");
                    throw new Exception("iterator problem");
                }


            }
            //we didnt retrieve any data 
            if (hfss == null)
                return;

            chfss = new CassandraHourlyFullStreamStatistics { count = hfss.count, streamid = hfss.streamid, timeindex = hfss.timeindex, streamstats = JsonConvert.SerializeObject(hfss.streamstats) };
            batchInserts.Append(hfsstable.Insert(chfss));
            //write them back
            batchInserts.Execute();
        }

        internal void DeleteStream(Guid streamID)
        {
            ISession session = Cluster.Instance.Session;
            Table<CassandraHourlyFullStreamStatistics> hfss = SessionExtensions.GetTable<CassandraHourlyFullStreamStatistics>(session);
            try
            {
                hfss.Where(st => st.streamid == streamID.ToString()).Delete().Execute();
                
            }
            catch { }
            lock (hourlyupdatelock)
            {
                if (hourlystats.ContainsKey(streamID.ToString()))
                    hourlystats.Remove(streamID.ToString());
            }

            lock (overallupdatelock)
            {
                if (fullstats.ContainsKey(streamID.ToString()))
                    fullstats.Remove(streamID.ToString());
            }
        }

        internal string GetInKeysStart(Guid streamID, int count, DateTime dt)
        {
            ISession session = Cluster.Instance.Session;

            long total = 0;
            long need = count;
            string hours = "";
            bool first = true;
            string selectString = String.Format("SELECT * FROM \"CassandraHourlyFullStreamStatistics\" where streamid = '{0}' and timeindex >= '{1}' ORDER BY timeindex ASC;", streamID.ToString(), dt.ToString("yyyyMMddHH"));

            //create the prepared statment for the select
            Statement ps = new SimpleStatement(selectString);

            //execute the statement
            RowSet rs = session.Execute(ps);

            foreach (Row row in rs.GetRows())
            {
                //during the first loop we add in that many extra point in case all these ones are after the given time
                if (first)
                {
                    first = false;
                    total += long.Parse(row["count"].ToString());
                    hours += "'" + row["timeindex"] + "'";

                    if (row["timeindex"].ToString() == dt.ToString("yyyyMMddHH"))
                        need += total;

                }
                else
                {
                    hours += ", '" + row["timeindex"] + "'";
                    total += long.Parse(row["count"].ToString());
                    if (total >= need)
                        break;
                }
            }
            return hours;
        }

        internal string GetInKeysEnd(Guid streamID, int count, DateTime dt)
        {
            ISession session = Cluster.Instance.Session;

            long total = 0;
            long need = count;
            string hours = "";
            bool first = true;
            string selectString = String.Format("SELECT * FROM \"CassandraHourlyFullStreamStatistics\" where streamid = '{0}' and timeindex <= '{1}' ORDER BY timeindex DESC;", streamID.ToString(), dt.ToString("yyyyMMddHH"));

            //create the prepared statment for the select
            Statement ps = new SimpleStatement(selectString);

            //execute the statement
            RowSet rs = session.Execute(ps);

            foreach (Row row in rs.GetRows())
            {
                //during the first loop we add in that many extra point in case all these ones are after the given time
                if (first)
                {
                    first = false;
                    hours += "'" + row["timeindex"] + "'";
                    total += long.Parse(row["count"].ToString());
                    if (row["timeindex"].ToString() == dt.ToString("yyyyMMddHH"))
                        need += total;
                }
                else
                {
                    hours += ", '" + row["timeindex"] + "'";
                    total += long.Parse(row["count"].ToString());
                    if (total >= need)
                        break;
                }
            }
            return hours;
        }

        internal string GetInKeys(Guid streamID, int count)
        {
            ISession session = Cluster.Instance.Session;

            long total = 0;
            string hours = "";
            bool first = true;
            string selectString = String.Format("SELECT * FROM \"CassandraHourlyFullStreamStatistics\" where streamid = '{0}' ORDER BY timeindex DESC;", streamID.ToString());

            //create the prepared statment for the select
            Statement ps = new SimpleStatement(selectString);

            //execute the statement
            RowSet rs = session.Execute(ps);

            foreach (Row row in rs.GetRows())
            {
                //during the first loop we add in that many extra point in case all these ones are after the given time
                if (first)
                {
                    first = false;
                    hours += "'" + row["timeindex"] + "'";
                }
                else
                {
                    hours += ", '" + row["timeindex"] + "'";
                }
                total += long.Parse(row["count"].ToString());
                if (total >= count)
                    break;
            }
            return hours;
        }
    }
}