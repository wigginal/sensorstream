﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json;
using System.IO;
using Cassandra;
using Cassandra.Data.Linq;
using Microsoft.ApplicationServer.Caching;
using SensorStreamCore;

namespace SensorStream
{
    /// <summary>
    /// Creates a device or returns a set of devices
    /// </summary>
    public class device : IHttpHandler
    {
        public void ProcessRequest(HttpContext context)
        {
            Utilities.SendHeaders(context);
            if (context.Request.HttpMethod.Equals("OPTIONS", StringComparison.InvariantCultureIgnoreCase))
            {
                return;
            }

            //get the request IP and log the IP
            String ip = Utilities.GetRequestIP(context);

            try
            {
                if (context.Request.QueryString["create"] != null)
                {
                    CreateDevice(context, ip);
                }
                else if (context.Request.QueryString["user"] != null)
                {
                    GetDevicesByUser(context, ip);
                }
                else if (context.Request.QueryString["getdevices"] != null)
                {
                    GetDevices(context, ip);
                }
                else if (context.Request.QueryString["delete"] != null && (context.Request.QueryString["delete"] == "streams" || context.Request.QueryString["delete"] == "device"))
                {
                    DeleteDevice(context, ip);
                }
                else
                {
                    //Incorrect Query String parameters
                    Utilities.LogAndWriteErrorResponse(context, ip + " : " + "Invalid Query string parameters", "Invalid Query string parameters");
                }
            }
            catch (Exception ex)
            {
                Logger.Instance.LogData(ip + " : errored out of device.ashx | " + ex.ToString());
            }
        }

        private void GetDevicesByUser(HttpContext context, string ip)
        {
            //log the request to for later debugging
            Logger.Instance.LogData(ip + ": Requested devices");
            /* ADD THIS BACK IN WITH USER NAME 
            object val = Caching.GetValue("getdevices");

            if (val != null)
            {

                //Logger.Instance.LogData("Received Key: " + (string)val);
                //return the data from the memcached query
                context.Response.ContentType = "application/json; charset=utf-8";
                context.Response.Write((string)val);
                return;
            }
            */
            //get the Cassandra cluster and devices
            ISession session = Cluster.Instance.Session;

            //get the tables containing the devices
            Table<CassandraSensorDevice> sensordeviceTable = SessionExtensions.GetTable<CassandraSensorDevice>(session);
            Table<CassandraDevicesByGUID> sensordeviceGUIDTable = SessionExtensions.GetTable<CassandraDevicesByGUID>(session);
            Table<CassandraDeviceByUsername> sensordeviceUserTable = SessionExtensions.GetTable<CassandraDeviceByUsername>(session);

            //create a list of devices to be returned
            List<SensorDevice> devices = new List<SensorDevice>();

            //***** WE SHOULD GET THIS FROM THE MEMCACHED NODE AND UPDATE THE NODE ON CREATION ********
            //go through all of the devices in the Cassandra node and add them to the list of devices
            //****** IF WE WANT MORE THAN 1000 RESULTS WE DO THAT HERE *******************************
            foreach (CassandraSensorDevice csd in sensordeviceTable.Where(st => st.UserName == context.Request.QueryString["user"].ToString()).Execute())
            {
                devices.Add(new SensorDevice { description = csd.Description, devicename = csd.DeviceName, username = csd.UserName });
            }

            //serialize the list of devices
            string response = JsonConvert.SerializeObject(devices);
            context.Response.ContentType = "application/json; charset=utf-8";
            context.Response.Write(response);

            /* ADD THIS BACK IN WITH USER NAME 
            //save the value to the memcached instance
            Caching.AddKey("getdevices", response);
             */
        }

        private static void CreateDevice(HttpContext context, String ip)
        {
            //get the POST data from the input stream
            string jsonInput = Utilities.GetPOSTString(context);

            //if the string is not empty, log it for later testing if we run into an error
            if (!String.IsNullOrEmpty(jsonInput))
            {
                Logger.Instance.LogData(ip + " : " + jsonInput);
            }
            else
            {
                Utilities.LogAndWriteErrorResponse(context, ip + " : No Input String Found", "No Input String Found");
                return;
            }


            try
            {
                //get the session and context for the Cassandra cluster and the device table
                ISession session = Cluster.Instance.Session;

                //get the tables for the device we are going to be adding
                Table<CassandraSensorDevice> sensordeviceTable = SessionExtensions.GetTable<CassandraSensorDevice>(session);
                Table<CassandraDevicesByGUID> sensordeviceGUIDTable = SessionExtensions.GetTable<CassandraDevicesByGUID>(session);
                Table<CassandraDeviceByUsername> sensordeviceUserTable = SessionExtensions.GetTable<CassandraDeviceByUsername>(session);

                //parse the json object into the sensor device
                SensorDevice json;
                try
                {
                    //try and deserialize the entire string hoping it is an array
                    json = JsonConvert.DeserializeObject<SensorDevice>(jsonInput);
                }
                catch (Exception ex)
                {
                    //we received an error parsing the data, likely caused by it not being an array
                    //log the error and try to create a single object
                    Utilities.LogAndWriteErrorResponse(context, "unable to deserialize device" + Environment.NewLine + " | error = " + ex.Message, "unable to deserialize device");
                    return;
                }

                //Check if the Sensor Device has all the correct parameters
                if (String.IsNullOrEmpty(json.devicename) || String.IsNullOrEmpty(json.username))
                {
                    Utilities.LogAndWriteErrorResponse(context, "device is missing user name or device name", "device is missing user name or device name");
                    return;
                }

                //***********Check the username and user key are valid right here****** (If we add this)*********

                //ensure the user doesn't have a device with this name already
                if (sensordeviceTable.Where(dev => dev.UserName == json.username && dev.DeviceName == json.devicename).Count().Execute() > 0)
                {
                    Utilities.LogAndWriteErrorResponse(context, "device for this user with this user name already exists", "device for this user with this user name already exists");
                    return;
                }

                //create a Cassandra sensor device to be stored from the current json object
                CassandraSensorDevice csd = new CassandraSensorDevice
                {
                    Created = DateTimeOffset.Now,
                    DeviceName = json.devicename,
                    Description = json.description,
                    LatestIP = ip,
                    UserName = json.username,
                    guid = Guid.NewGuid()
                };

                //store the device by GUID as well so we can easily look it up
                CassandraDevicesByGUID devguid = new CassandraDevicesByGUID
                {
                    guid = csd.guid,
                    DeviceName = csd.DeviceName,
                    UserName = csd.UserName
                };

                //and again store it by user name so we can look up all devices from a given user
                CassandraDeviceByUsername userDev = new CassandraDeviceByUsername
                {
                    UserName = csd.UserName,
                    DeviceName = csd.DeviceName
                };

                //Add the device to the tables
                sensordeviceTable.Insert(csd).Execute();
                sensordeviceGUIDTable.Insert(devguid).Execute();
                sensordeviceUserTable.Insert(userDev).Execute();

                Caching.DeleteKey("getdevices");
                //if there is a no reply query string close the connection
                if (context.Request.QueryString["noreply"] != null)
                {
                    context.ApplicationInstance.CompleteRequest();
                    return;
                }

                SensorDevice sd = new SensorDevice { created = csd.Created, description = csd.Description, devicename = csd.DeviceName, guid = csd.guid, latestip = csd.LatestIP, streams = null, username = csd.UserName };

                //write out the serialized json object
                context.Response.ContentType = "application/json; charset=utf-8";
                context.Response.Write(JsonConvert.SerializeObject(sd));
                context.ApplicationInstance.CompleteRequest();
            }
            catch (Exception ex)
            {
                //log the exception and return the exception
                Utilities.LogAndWriteErrorResponse(context, ip + " : " + ex.Message, "Unknown Error Creating Device, If this problem persists please let us know what is failing ");
            }
        }

        private static void DeleteDevice(HttpContext context, String ip)
        {
            //get the device ID from the header to match with a device
            Guid devID = Guid.Parse(HttpContext.Current.Request.Headers["key"]);

            if (context.Request.QueryString["delete"] == "device")
            {
                //log the request to for later debugging
                Logger.Instance.LogData(ip + ": Requested delete of device " + devID.ToString());
            }
            else if (context.Request.QueryString["delete"] == "streams")
            {
                Logger.Instance.LogData(ip + ": Requested delete of all the streams for device " + devID.ToString());
            }

            //get the Cassandra cluster and devices
            ISession session = Cluster.Instance.Session;
            
            //get the tables containing the devices
            Table<CassandraSensorDevice> sensordeviceTable = SessionExtensions.GetTable<CassandraSensorDevice>(session);
            Table<CassandraDevicesByGUID> sensordeviceGUIDTable = SessionExtensions.GetTable<CassandraDevicesByGUID>(session);
            Table<CassandraDeviceByUsername> sensordeviceUserTable = SessionExtensions.GetTable<CassandraDeviceByUsername>(session);

            if (context.Request.QueryString["delete"] == "device")
            {
                try
                {
                    Caching.DeleteKey("getdevices");
                    
                    //check this device to ensure it exists
                    CassandraDevicesByGUID cdbg = sensordeviceGUIDTable.First(dev => dev.guid == devID).Execute();
                    CassandraSensorDevice csd = sensordeviceTable.First(dev => dev.UserName == cdbg.UserName && dev.DeviceName == cdbg.DeviceName).Execute();
                    CassandraDeviceByUsername cdbu = sensordeviceUserTable.First(dev => dev.UserName == csd.UserName && dev.DeviceName == csd.DeviceName).Execute();

                    //remove from the database
                    Batch b = session.CreateBatch();
                    b.Append(sensordeviceGUIDTable.Where(dev => dev.guid == devID).Delete());
                    b.Append(sensordeviceTable.Where(dev => dev.UserName == cdbg.UserName && dev.DeviceName == cdbg.DeviceName).Delete());
                    b.Append(sensordeviceUserTable.Where(dev => dev.UserName == csd.UserName && dev.DeviceName == csd.DeviceName).Delete());
                    Logger.Instance.LogData(b.ToString());
                    b.Execute();
                }

                catch (Exception ex)
                {
                    Logger.Instance.LogData(ip + " : " + ex.ToString());
                }
            }
            //get the context for the streams
            Table<CassandraStreamByDevice> streambydev = SessionExtensions.GetTable<CassandraStreamByDevice>(session);

            try
            {
                //get all the streams matching this GUID
                foreach (var stream in streambydev.Where(dev => dev.deviceID == devID).Execute())
                {
                    Utilities.DeleteStream(context, ip, devID, stream.StreamID);
                }
            }
            catch (Exception ex)
            {
                Logger.Instance.LogData("Error deleting streams in device.ashx(maybe there weren't any): " + ex.ToString());
            }

#if debug
            Logger.Instance.LogData(ip + " : finished deleting streams");
#endif

            //if there is a no reply query string close the connection
            if (context.Request.QueryString["noreply"] != null)
            {
                context.ApplicationInstance.CompleteRequest();
                return;
            }

#if debug
            Logger.Instance.LogData(ip + " : finished up everything!");
#endif
            //write out the serialized json object
            context.Response.StatusCode = 200;
            context.Response.ContentType = "application/json; charset=utf-8";
            context.Response.Write("{ \"status\" : \"success\" }");
            return;
        }

        private static void GetDevices(HttpContext context, String ip)
        {
            //log the request to for later debugging
            Logger.Instance.LogData(ip + ": Requested devices");

            object val = Caching.GetValue("getdevices");

            if (val != null)
            {
                //Logger.Instance.LogData("Received Key: " + (string)val);
                //return the data from the memcached query
                context.Response.ContentType = "application/json; charset=utf-8";
                context.Response.Write((string)val);
                return;
            }

            //get the Cassandra cluster and devices
            ISession session = Cluster.Instance.Session;

            //get the tables containing the devices
            Table<CassandraSensorDevice> sensordeviceTable = SessionExtensions.GetTable<CassandraSensorDevice>(session);
            Table<CassandraDevicesByGUID> sensordeviceGUIDTable = SessionExtensions.GetTable<CassandraDevicesByGUID>(session);
            Table<CassandraDeviceByUsername> sensordeviceUserTable = SessionExtensions.GetTable<CassandraDeviceByUsername>(session);

            //create a list of devices to be returned
            List<SensorDevice> devices = new List<SensorDevice>();

            //***** WE SHOULD GET THIS FROM THE MEMCACHED NODE AND UPDATE THE NODE ON CREATION ********
            //go through all of the devices in the Cassandra node and add them to the list of devices
            //****** IF WE WANT MORE THAN 1000 RESULTS WE DO THAT HERE *******************************
            foreach (CassandraSensorDevice csd in sensordeviceTable.Execute())
            {
                devices.Add(new SensorDevice { description = csd.Description, devicename = csd.DeviceName, username = csd.UserName });
            }

            //serialize the list of devices
            string response = JsonConvert.SerializeObject(devices);
            context.Response.ContentType = "application/json; charset=utf-8";
            context.Response.Write(response);

            //save the value to the memcached instance
            Caching.AddKey("getdevices", response);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}