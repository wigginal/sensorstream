﻿using Cassandra;
using Cassandra.Data.Linq;
using SensorStreamCore;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace SensorStream
{
    /// <summary>
    /// Summary description for data
    /// </summary>
    public class data : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            Utilities.SendHeaders(context);
            if (context.Request.HttpMethod.Equals("OPTIONS", StringComparison.InvariantCultureIgnoreCase))
            {
                return;
            }

            //get the ip of the request
            String ip = Utilities.GetRequestIP(context);

            //check for the create flag to see if they are inserting data
            if (context.Request.QueryString["create"] != null)
            {
                AddData(context, ip);
            }
            else if (context.Request.QueryString["getdata"] != null)
            {
                //retrieving data
                GetData(context, ip);
            }
            else if (context.Request.QueryString["audio"] != null)
            {
                //this is a request to search through the audio transcription
                SearchAudio(context);
            }
            else
            {
                Utilities.LogAndWriteErrorResponse(context, ip + " : " + "Invalid Query string parameters", "Invalid Query string parameters");
            }
        }

        private void GetData(HttpContext context, string ip)
        {
            //collect the streamID from the query parameters
            Guid streamID;
            if (Guid.TryParse(context.Request.QueryString["getdata"], out streamID))
            {
                GetData(context, ip, streamID);
            }
            else
            {
                Utilities.LogAndWriteErrorResponse(context, "Not a valid layout of a streamID", "Not a valid layout of a streamID");
            }
        }

        private static void GetData(HttpContext context, string ip, Guid streamID)
        {
            //DateTime start = DateTime.Now;
            //StringBuilder timinglog = new StringBuilder(1000);
            //get the device and stream context so we can retrieve the data and information on the device
            ISession session = Cluster.Instance.Session;

            //get the tables we need to retrieve informations
            Table<CassandraStream> streamTable = SessionExtensions.GetTable<CassandraStream>(session);
            Table<CassandraStreamByDevice> streambydev = SessionExtensions.GetTable<CassandraStreamByDevice>(session);
            Table<CassandraSensorDevice> sensordeviceTable = SessionExtensions.GetTable<CassandraSensorDevice>(session);

            //try and get the stream
            CassandraStream strm = streamTable.First(st => st.StreamID == streamID).Execute();

            if (strm == null)
            {
                Utilities.LogAndWriteErrorResponse(context, ip + " : Invalid streamID from request, no stream found", "Invalid streamID from request, no stream found, 1st attempt");
                return;
            }

            try
            {
                //get the device information
                CassandraSensorDevice sensor = sensordeviceTable.First(dev => dev.UserName == strm.User && dev.DeviceName == strm.devName).Execute();

                context.Response.ContentType = "application/json; charset=utf-8";

                //we have the overall object, now retrieve the data
                string exec = "";
                if (context.Request.QueryString["timeonly"] != null)
                {
                    exec = "SELECT tuid FROM data" + streamID.ToString().Replace("-", "");
                }
                else
                {
                    exec = "SELECT * FROM data" + streamID.ToString().Replace("-", "");
                }

                int count = 10000;
                if (context.Request.QueryString["count"] != null)
                {
                    int test;
                    if (int.TryParse(context.Request.QueryString["count"], out test))
                    {
                        if (test < 50000)
                        {
                            count = test;
                        }
                    }
                }

                BuildDataQueryString(context, streamID, count, ref exec);


                //timinglog.AppendLine("Created query string: " + (DateTime.Now - start).TotalMilliseconds);

                Logger.Instance.LogData("query sting is - " + exec);
                //create the prepared statment for the select
                SimpleStatement ps = new SimpleStatement(exec);
                ps.SetPageSize(int.MaxValue);
                //execute the statement
                RowSet rs = session.Execute(ps);


                //timinglog.AppendLine("Retrieved data: " + (DateTime.Now - start).TotalMilliseconds);

                //iterate through the rows, adding the data to the stream
                //create a return object
                SensorDevice jdr = new SensorDevice()
                {
                    username = sensor.UserName,
                    devicename = sensor.DeviceName,
                    description = sensor.Description,
                    streams = new List<DeviceStream>() { new DeviceStream() { streamid = streamID.ToString(), description = strm.Description, name = strm.Name, streams = new List<StreamInfo>(), data = new List<Data>(), statistics = Statistics.Instance.GetStatistic(streamID), units = strm.Units } }
                };

                foreach (string fields in strm.Streams)
                {
                    jdr.streams[0].streams.Add(JsonConvert.DeserializeObject<StreamInfo>(fields));
                }

                //parallel for?
                if (context.Request.QueryString["timeonly"] != null)
                {
                    //iterate through the rows, adding the data to the stream
                    foreach (Row row in rs.GetRows())
                    {
                        //if the data is a blob we need to re encode it before transmitting it
                        jdr.streams[0].data.Add(new Data { time = GuidGenerator.GetDateTimeOffset(Guid.Parse(row["tuid"].ToString())).ToString("yyyy-MM-ddTHH:mm:ss.FFF%K") });
                    } //serialize the data and write it out
                    context.Response.Write(JsonConvert.SerializeObject(jdr, Formatting.None, new IsoDateTimeConverter() { DateTimeFormat = "yyyy-MM-ddTHH:mm:ss.FFF%K" }));
                }
                else
                {
                    //create a return object
                    foreach (Row row in rs.GetRows())
                    {
                        Data data = new Data { time = GuidGenerator.GetDateTimeOffset(Guid.Parse(row["tuid"].ToString())).ToString("yyyy-MM-ddTHH:mm:ss.FFF%K"), values = new Dictionary<string, string>() };
                        //if the data is a blob we need to re encode it before transmitting it
                        foreach (StreamInfo key in jdr.streams[0].streams)
                        {
                            string value = String.Empty;
                            string keyName = key.name.Replace(' ', '_').ToLower();
                            //does the key exist in the row
                            try
                            {
                                if (row[keyName] == null)
                                {
                                    data.values.Add(key.name, "");
                                    continue;
                                }

                                //if it is a blob type convert it and store it to a string
                                if (key.type == "blob")
                                    value = Encoding.ASCII.GetString((byte[])row[keyName]);
                                else
                                    value = row[keyName].ToString();
                                //add it to the dictionary
                                data.values.Add(key.name, value);
                            }
                            catch (Exception)
                            {
                                data.values.Add(key.name, "");
                            }
                        }

                        //add it to the output
                        jdr.streams[0].data.Add(data);
                    }
                    //serialize the data and write it out
                    context.Response.Write(JsonConvert.SerializeObject(jdr, Formatting.None, new IsoDateTimeConverter() { DateTimeFormat = "yyyy-MM-ddTHH:mm:ss.FFF%K" }));
                }

                //timinglog.AppendLine("Done writing out: " + (DateTime.Now - start).TotalMilliseconds);
                //Logger.Instance.LogTime(timinglog.ToString());
            }
            catch (Exception ex)
            {
                Utilities.LogAndWriteErrorResponse(context, ip + " : exception in data.ashx :: " + ex.Message, "Invalid streamID from request, no stream found, second attempt");
                return;
            }
        }

        private static void SearchAudio(HttpContext context)
        {
            //get the cassandra cluster and the context and stream information we will need
            ISession session = Cluster.Instance.Session;
            Table<CassandraStream> streams = SessionExtensions.GetTable<CassandraStream>(session);
            Table<CassandraStreamByDevice> streambydev = SessionExtensions.GetTable<CassandraStreamByDevice>(session);
            Table<TranscriptionLink> linkedstream = SessionExtensions.GetTable<TranscriptionLink>(session);
            Table<AudioAnnotation> anotationstore = SessionExtensions.GetTable<AudioAnnotation>(session);

            List<AudioData> returnAudio = new List<AudioData>();
            //return the list of results for this set of objects
            int count = 0;
            foreach (AudioAnnotation aa in anotationstore.Where(val => val.word == context.Request.QueryString["audio"]).Execute())
            {
                string datestring = GuidGenerator.GetDateTime(aa.streamindex).Subtract(new TimeSpan(8, 0, 0)).ToString("yyyyMMddHH");

                var audioStatement = session.Prepare("SELECT value FROM data" + aa.stream.ToString().Replace("-", "") + " where Date = ? and tuid = ?");
                //gp through each results and get the audio
                foreach (Row audioRow in session.Execute(audioStatement.Bind(datestring, aa.streamindex.ToString())).GetRows())
                {
                    //get the linked table that has the transcription
                    TranscriptionLink tl = linkedstream.Where(s => s.stream1 == aa.stream).Execute().First();
                    //retieve the transcription!
                    var statement = session.Prepare("SELECT value FROM data" + tl.stream2.ToString().Replace("-", "") + " where Date = ? and tuid = ?");
                    foreach (Row textRow in session.Execute(statement.Bind(datestring, aa.streamindex.ToString())).GetRows())
                    {
                        //if the data is a blob we need to re encode it before transmitting it
                        returnAudio.Add(new AudioData { time = GuidGenerator.GetDateTimeOffset(Guid.Parse(aa.streamindex.ToString())).ToString("yyyy-MM-ddTHH:mm:ss.FFF%K"), value = Convert.ToBase64String((byte[])audioRow["value"]), speechtotext = (string)textRow["value"], streamid = aa.stream.ToString() });
                        count++;
                    }
                }
                if (count > 10)
                    break;
            }

            context.Response.ContentType = "application/json; charset=utf-8";
            context.Response.Write(JsonConvert.SerializeObject(returnAudio));
        }

        private static void BuildDataQueryString(HttpContext context, Guid streamID, int count, ref string exec)
        {
            if (context.Request.QueryString["start"] != null && context.Request.QueryString["end"] != null)
            {
                //create the list of hour string possible, then retrieve by tuid desc
                DateTime begin = DateTime.Parse(context.Request.QueryString["start"]);
                Guid begintuid = GuidGenerator.GenerateTimeBasedGuid(begin);
                DateTime end = DateTime.Parse(context.Request.QueryString["end"]);
                Guid endtuid = GuidGenerator.GenerateTimeBasedGuid(end);

                if (begin > end)
                {
                    DateTime holder = end;
                    end = begin;
                    begin = holder;
                }

                exec += " where date IN (";
                exec += " '" + begin.ToString("yyyyMMddHH") + "' ";
                begin = begin.AddHours(1);

                while (begin < end)
                {
                    exec += ", '" + begin.ToString("yyyyMMddHH") + "' ";
                    begin = begin.AddHours(1);
                }
                exec += " ) and tuid >= " + begintuid + " and tuid <= " + endtuid + " ORDER BY tuid DESC";
            }
            else
            {
                //We need stats for all these to determine how many 
                if (context.Request.QueryString["start"] != null)
                {
                    DateTime dt = DateTime.Parse(context.Request.QueryString["start"]);
                    Guid test = GuidGenerator.GenerateTimeBasedGuid(dt);

                    exec += " where date IN (";
                    exec += Statistics.Instance.GetInKeysStart(streamID, count, dt);
                    exec += ") and tuid >= " + test + " ORDER BY tuid DESC";
                }
                else if (context.Request.QueryString["end"] != null)
                {
                    DateTime dt = DateTime.Parse(context.Request.QueryString["end"]);
                    Guid test = GuidGenerator.GenerateTimeBasedGuid(dt);

                    exec += " where date IN (";
                    exec += Statistics.Instance.GetInKeysEnd(streamID, count, dt);
                    exec += ") and tuid <= " + test + " ORDER BY tuid DESC";
                }
                else
                {
                    exec += " where date IN (";
                    exec += Statistics.Instance.GetInKeys(streamID, count);
                    exec += ") ORDER BY tuid DESC";
                }
            }
            exec += " LIMIT " + count.ToString() + ";";
        }

        private static void AddData(HttpContext context, String ip)
        {
            string jsonInput;

            try
            {
                //get the POST data from the input stream
                jsonInput = Utilities.GetPOSTString(context);
            }
            catch (Exception ex)
            {
                Utilities.LogAndWriteErrorResponse(context, ip + " : Error Getting Post String : " + ex.Message, "Error retrieving uploaded data");
                return;
            }

            //if the string is not empty, log it for later testing if we run into an error
            if (String.IsNullOrEmpty(jsonInput))
            {
                Utilities.LogAndWriteErrorResponse(context, ip + "No Input Data Found", "No Input Data Found");
                return;
            }

            try
            {
                //we will be creating an array of object to be inserting even if we only recieve one
                dynamic jsonA = null;
                try
                {
                    //try and deserialize the entire string hoping it is an array
                    jsonA = JsonConvert.DeserializeObject(jsonInput);
                }
                catch (Exception ex)
                {
                    Utilities.LogAndWriteErrorResponse(context, ip + " : unable to deserialize array: " + ex.Message, "unable to deserialize data");
                    return;
                }

                Type t = jsonA.GetType();

                if (t == typeof(JArray))
                {
                    Data[] data = JsonConvert.DeserializeObject<Data[]>(jsonInput);
                    AddData(context, data, ip);
                }
                else
                {
                    Data[] data = new Data[] { JsonConvert.DeserializeObject<Data>(jsonInput) };
                    AddData(context, data, ip);
                }
            }
            catch (Exception ex)
            {
                Utilities.LogAndWriteErrorResponse(context, ip + " : data.ashx : " + ex.ToString(), "Unknown Error adding data, If this problem persists please let us know what is failing ");
            }
            return;
        }

        private static void AddDataTimed(HttpContext context, Data[] jsonA, string ip, StringBuilder timinglog, DateTime start)
        {
            int failed = 0;
            //validate the JSON objects
            Parallel.ForEach(jsonA, jd =>
            {
                if (jd.streamid == null)
                {
                    Interlocked.Increment(ref failed);
                    return;
                }

                //try and parse the datetime string
                try
                {
                    jd.parsedtime = new DateTimeOffset(DateTime.Parse(jd.time));
                }
                catch (Exception)
                {
                    jd.parsedtime = null;
                }
            });

            //if one or more of the above items are missing a stream just fail out
            if (failed > 0)
            {
                Utilities.LogAndWriteErrorResponse(context, ip + " : data.ashx : streamID missing in one or more data objects", "streamID missing in one or more data objects");
                return;
            }

            //get the device ID so we can verify this is a valid data push
            Guid devID = Guid.Parse(HttpContext.Current.Request.Headers["key"]);
            ConcurrentDictionary<string, Queue<object[]>> PreparedStatements = new ConcurrentDictionary<string, Queue<object[]>>();
            ParallelOptions po = new ParallelOptions();
            po.MaxDegreeOfParallelism = 48;
            failed = 0;
            object lockObject = new object();
            StringBuilder batchExec = new StringBuilder(jsonA.Length * 250);

            batchExec.Append("BEGIN BATCH");
            batchExec.Append(Environment.NewLine);

            Parallel.ForEach(jsonA, po, () => new StringBuilder(50000),

                (json, loopState, partialResults) =>
                {
                    string insert = CreateInsertString(ip, ref failed, devID, json);
                    return partialResults.Append(insert);
                },

                (localPartialSum) =>
                {
                    // Enforce serial access to single, shared result
                    lock (lockObject)
                    {
                        batchExec.Append(localPartialSum);
                    }

                }
            );

            batchExec.Append(Environment.NewLine);
            batchExec.Append("APPLY BATCH;");

            if (failed > 0)
            {
                Utilities.LogAndWriteErrorResponse(context, ip + " : data.ashx : Failed parsing the values to insert", "Failed parsing the values to insert");
                return;
            }

            timinglog.AppendLine("Created Insert: " + (DateTime.Now - start).TotalMilliseconds);

            try
            {
                //insert the object. When the new version of the api arrives we will turn this into a batch statement
                ISession session = Cluster.Instance.Session;
                session.Execute(batchExec.ToString());
            }
            catch (Exception ex)
            {
                Utilities.LogAndWriteErrorResponse(context, ip + " : Error executing insert: " + ex.ToString() + Environment.NewLine + "Batch string was: " + batchExec, "Failed to insert data");
                return;
            }
            SendSuccessResponse(context);
        }

        private static void AddData(HttpContext context, Data[] jsonA, string ip)
        {
            int failed = 0;
            //validate the JSON objects
            Parallel.ForEach(jsonA, jd =>
            {
                if (jd.streamid == null)
                {
                    Interlocked.Increment(ref failed);
                    return;
                }

                //try and parse the datetime string
                try
                {
                    jd.parsedtime = new DateTimeOffset(DateTime.Parse(jd.time));
                }
                catch (Exception)
                {
                    jd.parsedtime = null;
                }
            });

            //if one or more of the above items are missing a stream just fail out
            if (failed > 0)
            {
                Utilities.LogAndWriteErrorResponse(context, ip + " : data.ashx : streamID missing in one or more data objects", "streamID missing in one or more data objects");
                return;
            }

            //get the device ID so we can verify this is a valid data push
            Guid devID = Guid.Parse(HttpContext.Current.Request.Headers["key"]);

            ConcurrentDictionary<string, Queue<object[]>> PreparedStatements = new ConcurrentDictionary<string, Queue<object[]>>();

            ParallelOptions po = new ParallelOptions();
            po.MaxDegreeOfParallelism = 48;
            failed = 0;
            object lockObject = new object();

            StringBuilder batchExec = new StringBuilder(jsonA.Length * 250);

            batchExec.Append("BEGIN BATCH");
            batchExec.Append(Environment.NewLine);

            Parallel.ForEach(jsonA, po, () => new StringBuilder(50000),

                (json, loopState, partialResults) =>
                {
                    string insert = CreateInsertString(ip, ref failed, devID, json);
                    return partialResults.Append(insert);
                },

                (localPartialSum) =>
                {
                    // Enforce serial access to single, shared result
                    lock (lockObject)
                    {
                        batchExec.Append(localPartialSum);
                    }

                }
            );

            batchExec.Append(Environment.NewLine);
            batchExec.Append("APPLY BATCH;");

            if (failed > 0)
            {
                Utilities.LogAndWriteErrorResponse(context, ip + " : data.ashx : Failed parsing the values to insert", "Failed parsing the values to insert");
                return;
            }

            try
            {
                //insert the object. When the new version of the api arrives we will turn this into a batch statement
                ISession session = Cluster.Instance.Session;
                session.Execute(batchExec.ToString());
            }
            catch (Exception ex)
            {
                Utilities.LogAndWriteErrorResponse(context, ip + " : Error executing insert: " + ex.ToString() + Environment.NewLine + "Batch string was: " + batchExec, "Failed to insert data");
                return;
            }
            SendSuccessResponse(context);
        }

        private static string CreateInsertString(string ip, ref int failed, Guid devID, Data json)
        {
            //parse the StreamID so we can insert this data into the correct stream
            Guid StreamID = Guid.Parse(json.streamid);

            //make sure the streamID matches the deviceID to ensure security
            if (!Utilities.StreamDeviceMatch(devID, StreamID))
            {
                Logger.Instance.LogData(ip + " : Invalid \"key\" in the header, stream and device dont match - devid: " + devID.ToString() + "  streamid: " + StreamID.ToString());
                Interlocked.Increment(ref failed);
                return "";
            }

            //check the units so we can parse the value
            Dictionary<string, string> streamValueNameToType = Utilities.GetStreamColumns(StreamID);
            StringBuilder insertString = new StringBuilder(400);

            insertString.Append("INSERT INTO data");
            insertString.Append(StreamID.ToString().Replace("-", ""));
            insertString.Append(" (Date, Tuid");

            Queue<string> valuesToInsert = new Queue<string>();
            List<string> keysToRemove = new List<string>();

            if (json.parsedtime == null)
                json.parsedtime = DateTimeOffset.Now;

            string datestring = json.parsedtime.Value.ToString("yyyyMMddHH");
            Guid itemID = GuidGenerator.GenerateTimeBasedGuid(json.parsedtime.Value);
            valuesToInsert.Enqueue("'" + datestring + "'");
            valuesToInsert.Enqueue(itemID.ToString());

            foreach (var item in json.values.OrderBy(x => x.Key))
            {
                //check if the key exists in the stream
                if (!streamValueNameToType.ContainsKey(item.Key))
                {
                    //shouldnt be here remove it so we can brodcast and store correctly
                    keysToRemove.Add(item.Key);
                    continue;
                }

                if (String.IsNullOrWhiteSpace(item.Value) || String.IsNullOrEmpty(item.Value))
                {
                    keysToRemove.Add(item.Key);
                    continue;
                }
                //parse the value
                string j = null;
                try
                {
                    j = Utilities.ParseValueGetString(streamValueNameToType[item.Key], item.Value.Trim());
                }
                catch (Exception ex)
                {
                    Logger.Instance.LogData(ip + " : Unable to parse value to the correct object type: key - " + item.Key + " Val- " + item.Value.ToString() + " key type - " + streamValueNameToType[item.Key] + " Err: " + ex.Message);
                    Interlocked.Increment(ref failed);
                    return "";
                }

                //add it to the object queue
                valuesToInsert.Enqueue(j);
                //add its spot in the insertString
                insertString.Append(", ");
                if (item.Key.Contains(' '))
                    insertString.Append(item.Key.Replace(' ', '_'));
                else
                    insertString.Append(item.Key);

            }

            foreach (var item in streamValueNameToType)
            {
                if (streamValueNameToType[item.Key] == "blob" && json.values.ContainsKey(item.Key))
                {
                    byte[] j = (byte[])Utilities.ParseValue(streamValueNameToType[item.Key], json.values[item.Key]);
                    json.values[item.Key] = Convert.ToBase64String(j);
                }
            }

            //remove the extra values we dont need
            foreach (string key in keysToRemove)
            {
                json.values.Remove(key);
            }

            //finish the excess things we dont need
            insertString.Append(") VALUES ( ");
            insertString.Append(valuesToInsert.Dequeue());
            while (valuesToInsert.Count != 0)
            {
                insertString.Append(", ");
                insertString.Append(valuesToInsert.Dequeue());
            }
            insertString.Append(");");

            //broadcast the insert
            json.time = json.parsedtime.Value.ToString("yyyy-MM-ddTHH:mm:ss.FFF%K");

            DataBroadcaster.Instance.BroadcastData("Complex", JsonConvert.SerializeObject(json), StreamID);
            Statistics.Instance.UpdateStatistics(StreamID, json);
            return insertString.ToString();
        }

        private static void SendSuccessResponse(HttpContext context)
        {
            //if there is a no reply query string close the connection
            if (context.Request.QueryString["noreply"] != null)
            {
                context.Response.End();
                return;
            }

            //they did want a reply, lets write out the object
            context.Response.ContentType = "application/json; charset=utf-8";

            //write out the serialized json object
            context.Response.Write(JsonConvert.SerializeObject(new DataAddResponse { status = "Success", streamid = "", time = DateTimeOffset.Now }));
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}