﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;
using System.Collections.Concurrent;
using System.Net.Sockets;
using System.Threading;
using Microsoft.Web.WebSockets;
using System.Threading.Tasks;

namespace SensorStreamManage
{
    public class DataHub : Hub
    {
        public static Dictionary<Guid, List<string>> SubscriptonsByGuid = new Dictionary<Guid, List<string>>();
        public static Dictionary<string, List<Guid>> SubscriptionsByClient = new Dictionary<string, List<Guid>>();
        public static Dictionary<string, List<Guid>> DisconnectedClients = new Dictionary<string, List<Guid>>();
        public object subByGuidlock = new object();


        public override Task OnDisconnected(bool stopCalled)
        {
            //check the disconnected clients and add them back in
            if (!SubscriptionsByClient.ContainsKey(Context.ConnectionId))
            {
                //it didnt contain that key so we are done
                return base.OnReconnected();
            }

            //first remove the device from the dictionary of clients and retrieve the list
            foreach (Guid subscription in SubscriptionsByClient[Context.ConnectionId])
            {
                //remove all of its subscriptions
                lock (subByGuidlock)
                {
                    //remove the connection from the subscription list so it wont recieve updates
                    SubscriptonsByGuid[subscription].Remove(Context.ConnectionId);

                    //if that was the last one, then remove it from the dictionary
                    if (SubscriptonsByGuid[subscription].Count == 0)
                        SubscriptonsByGuid.Remove(subscription);
                }
            }
            DisconnectedClients.Add(Context.ConnectionId, SubscriptionsByClient[Context.ConnectionId]);
            SubscriptionsByClient.Remove(Context.ConnectionId);
            return base.OnDisconnected(stopCalled);
        }

        public override Task OnReconnected()
        {
            //check the disconnected clients and add them back in
            if (!DisconnectedClients.ContainsKey(Context.ConnectionId))
            {
                //it didnt contain that key so we are done
                return base.OnReconnected();
            }

            //first remove the device from the dictionary of clients and retrieve the list
            foreach (Guid subscription in DisconnectedClients[Context.ConnectionId])
            {
                //remove all of its subscriptions
                lock (subByGuidlock)
                {
                    if (SubscriptonsByGuid.ContainsKey(subscription))
                    {
                        //this ID existed already, add to the list of clients
                        SubscriptonsByGuid[subscription].Add(Context.ConnectionId);
                    }
                    else
                    {
                        //the list doesnt exist already, create a new one and add it
                        List<string> newList = new List<string>();
                        newList.Add(Context.ConnectionId);
                        SubscriptonsByGuid.Add(subscription, newList);
                    }
                }
            }
            SubscriptionsByClient.Add(Context.ConnectionId, DisconnectedClients[Context.ConnectionId]);
            DisconnectedClients.Remove(Context.ConnectionId);
            return base.OnReconnected();
        }

        public void Subscribe(string StreamID)
        {
            //parse the given string to a StreamID
            Guid id = Guid.Parse(StreamID);

            Logger.Instance.LogData("Connection from: " + id.ToString());
            //check to see if this connection has subscriptions already
            if (SubscriptionsByClient.ContainsKey(Context.ConnectionId))
            {
                //the list does exist, add this new Guid to the list
                SubscriptionsByClient[Context.ConnectionId].Add(id);
            }
            else
            {
                //the list doesnt exist already, create it and add it to the clients list
                List<Guid> newList = new List<Guid>();
                newList.Add(id);
                SubscriptionsByClient.Add(Context.ConnectionId, newList);
            }

            //now add the list to the subscriptions by guid table
            lock (subByGuidlock)
            {
                if (SubscriptonsByGuid.ContainsKey(id))
                {
                    //this ID existed already, add to the list of clients
                    SubscriptonsByGuid[id].Add(Context.ConnectionId);
                }
                else
                {
                    //the list doesnt exist already, create a new one and add it
                    List<string> newList = new List<string>();
                    newList.Add(Context.ConnectionId);
                    SubscriptonsByGuid.Add(id, newList);
                }
            }
        }

        public void Unsubscribe(string StreamID)
        {
            //parse the given string to a StreamID
            Guid id = Guid.Parse(StreamID);

            //check to see if this connection has subscriptions already
            if (SubscriptionsByClient.ContainsKey(Context.ConnectionId))
            {
                //the list does exist, check if it has this GUID
                if (SubscriptionsByClient[Context.ConnectionId].Contains(id))
                {
                    //remove it
                    SubscriptionsByClient[Context.ConnectionId].Remove(id);

                    //now add the list to the subscriptions by guid table
                    lock (subByGuidlock)
                    {
                        //remove the connection from the subscription list so it wont recieve updates
                        SubscriptonsByGuid[id].Remove(Context.ConnectionId);

                        //if that was the last one, then remove it from the dictionary
                        if (SubscriptonsByGuid[id].Count == 0)
                            SubscriptonsByGuid.Remove(id);
                    }
                }

            }
        }

        public void Send(string name, string message, Guid streamID)
        {
            //All these calls are done in DataBroadcaster.cs
        }
    }
}