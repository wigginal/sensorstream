﻿using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SensorStreamManage
{
    public class DataBroadcaster
    {
        private readonly static Lazy<DataBroadcaster> _instance = new Lazy<DataBroadcaster>(() => new DataBroadcaster(GlobalHost.ConnectionManager.GetHubContext<DataHub>()));

        private IHubContext _context;
        public static DataBroadcaster Instance
        {
            get
            {
                return _instance.Value;
            }
        }

        public DataBroadcaster(IHubContext context)
        {
            _context = context;
        }

        public void BroadcastData(Guid streamID, string jsonObject)
        {
            Logger.Instance.LogData("Broadcasting to: " + streamID.ToString() + Environment.NewLine + jsonObject);
            if (DataHub.SubscriptonsByGuid.ContainsKey(streamID))
            {
                _context.Clients.Clients(DataHub.SubscriptonsByGuid[streamID]).newData(streamID.ToString(), jsonObject);

            }
            if (DataHubSockets.clients.ContainsKey(streamID.ToString()))
                DataHubSockets.clients[streamID.ToString()].Broadcast(jsonObject);

        }

    }
}