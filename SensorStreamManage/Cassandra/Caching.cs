﻿#define NONE

using Microsoft.ApplicationServer.Caching;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SensorStreamManage
{
    public class Caching
    {
        internal static void DeleteKey(string keytodelete)
        {
#if AZURECACHE
            DataCache cache = new DataCache("default");
            cache.Remove(keytodelete);
#elif MEMCACHED
             //remove the cached value from memcached
             MemcachedConnection mc = MemcachedConnectionPool.Instance.getConnection();
             mc.DeleteKey(keytodelete);
             MemcachedConnectionPool.Instance.ReturnConnection(mc);
#endif
        }

        internal static object GetValue(string p)
        {
#if AZURECACHE
            //remove the cached devices from memcached, repopulate next request
            DataCache cache = new DataCache("default");
            return cache.Get("getdevices");
#elif MEMCACHED
            //MemcachedConnection mc = MemcachedConnectionPool.Instance.getConnection();
            object val = mc.GetValue("getdevices");
            MemcachedConnectionPool.Instance.ReturnConnection(mc);
#else
            return null;
#endif
        }

        internal static void AddKey(string key, string value)
        {
#if AZURECACHE
            //remove the cached devices from memcached, repopulate next request
            DataCache cache = new DataCache("default");
            cache.Add(key, value);
#elif MEMCACHED
            //save the value to the memcached instance
            mc = MemcachedConnectionPool.Instance.getConnection();
            mc.SetKeyValueNoReply(key, 0, 86400, value);
            MemcachedConnectionPool.Instance.ReturnConnection(mc);
#endif
        }
    }
}