﻿﻿using System;
using System.Configuration;
using SensorStreamCore;
using Cassandra.Data.Linq;

namespace SensorStreamManage
{
    public sealed class CassandraInstance
    {
        private static readonly Lazy<CassandraCluster> _instance = new Lazy<CassandraCluster>(() => new CassandraCluster(ConfigurationManager.AppSettings["CassandraNode"], ConfigurationManager.AppSettings["CassandraKeySpace"]));

        public static CassandraCluster Instance
        {
            get
            {
                return _instance.Value;
            }
        }
    }
}