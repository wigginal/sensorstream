﻿$.dm = new (function () {
    var baseURL = "https://locahost";
    var apiRoot = "/api";
    this.streamIdMap = [];
    var hub = null;
    var pref = this;
    // test device guid: 5b4ac80f-41e3-412d-b893-61ddf5357a46

    // Private functions
    // Converts an integer (unicode value) to a char

    function itoa(i) {
        return String.fromCharCode(i);
    }


    // Converts a char into to an integer (unicode value)

    function atoi(a) {
        return a.charCodeAt();
    }

    // Signalr Bits
    // Only avalable if signalr is included in the client page.
    if ($.connection) {

        // Initialize a connection with the server.
        // This must be called before using subscribe() or unsubscribe()
        this.connect = function ()
        {
            $.connection.hub.logging = true;
            $.connection.hub.url = baseURL + "/signalr";
            hub = $.connection.dataHub;
            hub.client.NewData = function (name, message) {
                message = JSON.parse(message);
                if (pref.streamIdMap[message[0].streamid]) {
                    var f = pref.streamIdMap[message[0].streamid];
                    for (var i in message) {
                        message[i].time = moment(message[i].time);
                    }
                    f(name, message);
                }
                    // In theory you shouldn't get a stream you haven't defined a handler for.
                else {
                    console.error("Trouble. This is a stream you don't have a handler for.");
                }
            }
            return $.connection.hub.start()
                .done(function (r) {
                    console.log(r);
                    console.log("Connected to " + baseURL);
                })
                .fail(function (r) {
                    console.log(r);
                    console.log("Error connecting to " + baseURL);
                });
        }

        // Add a stream specific handler.
        this.subscribe = function (streamID, newDataCallback) {
            if (!hub) return this;
            pref.streamIdMap[streamID] = newDataCallback;
            hub.server.subscribe(streamID);
            console.log(hub);
            return pref = this;
        }

        // Unsubscribe a stream specific handler. 
        this.unsubscribe = function (streamID) {
            if (!hub) return this;
            hub.server.unsubscribe(streamID);
            pref.streamIdMap[streamID] = undefined;
            return pref = this;
        }
    }

    // All of these functions support the jQuery style .fail(), .done(), and .always()
    this.getDevices = function () {
        return $.getJSON(baseURL + apiRoot + "/GetDevices");
    }

    // {
    //      getstreams: DeviceName,
    //      user: UserName
    //  }
    this.getStreams = function (deviceId) {
        return $.getJSON(baseURL + apiRoot + "/GetStreams/"+ deviceId)
    }

    this.getStream = function (streamid) {
        return $.getJSON(baseURL + apiRoot + "/GetStream/"+ streamid)
    }

    this.getData = function (streamID, endDate, nPoints) {
        if (endDate && nPoints) {
            var timestring = endDate.toISOString();
            return $.getJSON(baseURL + apiRoot+"/GetData/"+streamID,
            {
                count: nPoints,
                end: timestring
            });
        }
        return $.getJSON(baseURL + apiRoot+"/GetData/" +streamID);
    }
})();


streamMonitor = function (streamID, retain) {
    return new (function () {

        var self = this;

        if (typeof (retain) == 'undefined')
            retain = 100;

        var _data = null;
        var _cbs = [];
        self.streamID = streamID;
        self.ready = false;

        self.data = function () {
            return _data;
        };
        self.retains = function (v) {
            if(typeof(v) == 'undefined')
                retains = v;
            return retains;
        };
        self.subscribe = function (cb) {
            _cbs.push(cb);
            return self;
        };
        self.unsubscribe = function (cb) {
            var i = _cbs.indexOf(cb);
            if (i != -1) {
                _cbs.splice(i, 1);
            }
            return self;
        };
        var donefuncs = [function (d) { }];
        self.done = function (f) {
            donefuncs.push(f);
            return self;
        };

        self.destroy = function () {
            $.dm.unsubscribe(streamID);
            _data = [];
            _cbs = [];
        };

        // Maybes
        //this.streamcontext();
        //this.fields();

        $.dm.getData(streamID, moment(), retain)
            .done(function (d) {
                _data = d;
                for (var i in _data.Streams[0].Data) {
                    _data.Streams[0].Data[i].time = moment(_data.Streams[0].Data[i].time);
                }
                for (var i in donefuncs) {
                    var f = donefuncs[i];
                    f(d);
                }
                self.ready = true;
            })
            .fail(function (e) {
                console.error(e.status + ": Failed to retrieve data for stream " + streamID + ". " + e.responseText);
            });// Need a fail case.

        $.dm.subscribe(streamID, function(n, msg) {
            // Update the data store
            _data.Streams[0].Data.concat(msg);
            _data.Streams[0].Data.sort(function (a, b) { return a.time < b.time ? -1 : a.time > b.time ? 1 : 0; });
            _data.Streams[0].Data.shift();
            // Call all registered callbacks.
            for (var i in _cbs) {
                var cb = _cbs[i];
                cb(n, msg);
            }
        });
        return self;
    })();
};