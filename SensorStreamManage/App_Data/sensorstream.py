#!/usr/bin/python
import httplib, urllib, json, datetime

baseURL = "localhost"

DEBUG_LEVEL = 2;

def _get_conn():
    conn = httplib.HTTPSConnection(baseURL, port=443)
    conn.set_debuglevel(DEBUG_LEVEL)
    return conn

#returns all public devices
def getDevices():
    conn = _get_conn()
    conn.request("GET", url="/api/GetDevices")
    return conn.getresponse()
	
#username  - this searches for the exact name
#devicename - uses the LIKE operator (contains)
#description - uses the LIKE operator (contains)	
def getDevicesFiltered(username=None, devicename=None, description=None):
    conn = _get_conn()
    filters = ""

    if username:
        filters = filters + "User=" + username + "&"

    if description:
 	filters = filters + "Desc=" + description + "&"

    if devicename:
	filters = filters + "Dev=" + devicename+ "&"

    conn.request("GET", url="/api/GetDevices?" + filters)
    return conn.getresponse()

#deviceid - the id of the device to retrieve
def getDevice(deviceid):
    conn = _get_conn()
    conn.request("GET", url="/api/GetDevice/" + deviceid )
    return conn.getresponse()

#deviceid - the id of the device to retrieve
#key - the key for the private device
def getPrivateDevice(deviceid,key):
    conn = _get_conn()
    conn.request("GET", url="/api/GetDevice/" + deviceid + "?Key=" + key )
    return conn.getresponse()

#returns all the streamd of the given device id
def getStreams(deviceid):
    conn = _get_conn()
    conn.request("GET", url="/api/getStreams/" + deviceid )
    return conn.getresponse()
	
#returns all the streams of a the private device
def getPrivateStreams(deviceid, key):
    conn = _get_conn()
    conn.request("GET", url="/api/getStreams/" + deviceid + "?Key=" + key )
    return conn.getresponse()

#returns the single stream and encapsulating device
def getStream(streamid):
    conn = _get_conn()
    conn.request("GET", url="/api/getStream/" + streamid)
    return conn.getresponse()    
	
#returns the single private stream and encapsulating device
def getPrivateStream(streamid, key):
    conn = _get_conn()
    conn.request("GET", url="/api/getStream/" + streamid + "?Key=" + key )
    return conn.getresponse()    

#returns data from a stream
def getData(streamID, startDate=None, endDate=None, nPoints=None, timeonly=None, key=None):
    conn = _get_conn()
    urlparams = ""
    if startDate:
	urlparams = urlparams + "start=" + startDate.toISOString() + "&"
    if endDate:
	urlparams = urlparams + "end=" + endDate.toISOString() + "&"
    if nPoints:
	urlparams = urlparams + "count=" + nPoints + "&"
    if timeonly:
	urlparams = urlparams + "timeonly=y&"
    if timeonly:
	urlparams = urlparams + "Key=" + key + "&"
    
    conn.request("GET","/api/GetData/" + streamID+ "?" + urlparams)
    return conn.getresponse()

#send data to the given stream	
def sendData(key, data):
    conn = _get_conn()
    conn.request("POST","/api/AddData",body=json.dumps(data),headers={'key':key})
    return conn.getresponse()
	
