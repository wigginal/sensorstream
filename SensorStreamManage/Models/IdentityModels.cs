﻿using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Collections.Generic;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System;
using Newtonsoft.Json;
using System.Runtime.Serialization;
using SensorStreamCore;

namespace SensorStreamManage.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {

        public virtual ICollection<Device> MyDevices { get; set; }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }

    [DataContract]
    [Table("Device")]
    public class Device
    {
        [DataMember]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Key]
        [Display(Name = "Device ID")]
        public string DeviceID { get; set; }

        [DataMember]
        [Display(Name = "User Name")]
        public string UserName { get; set; }

        [DataMember]
        [Display(Name = "Device Name")]
        public string DeviceName { get; set; }

        [DataMember]
        [Display(Name = "Date Created")]
        public DateTimeOffset DateCreated { get; set; }

        [IgnoreDataMember]
        [Display(Name = "IPCreated")]
        public string IPCreated { get; set; }

        [IgnoreDataMember]
        [Display(Name = "Private Key")]
        public string DeviceKey { get; set; }

        [DataMember]
        [Display(Name = "Private?")]
        public bool IsPrivate { get; set; }

        [DataMember]
        [Display(Name = "Description")]
        public string Description { get; set; }

        [IgnoreDataMember]
        public virtual ApplicationUser User { get; set; }
    }

    [DataContract]
    [Table("Stream")]
    public class Stream
    {
        [DataMember]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Key]
        public string StreamID { get; set; }
        [Required]

        [DataMember]
        [Display(Name = "Stream Name")]
        public string StreamName { get; set; }

        [DataMember]
        [Required]
        public string Description { get; set; }

        [DataMember]
        public DateTimeOffset DateCreated { get; set; }

        [IgnoreDataMember]
        public string IPCreated { get; set; }

        [DataMember]
        public string Units { get; set; }

        [DataMember]
        [Display(Name = "Private?")]
        public bool isPrivate { get; set; }

        [IgnoreDataMember]
        public virtual Device device { get; set; }
    }

    [DataContract]
    [Table("DataStream")]
    public class DataStream
    {
        [IgnoreDataMember]
        [Key]
        [DatabaseGenerated(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity)]
        public Int64 ID { get; set; }

        [IgnoreDataMember]
        public virtual Stream Stream { get; set; }

        [Required]
        [DataMember]
        [Display(Name = "Name")]
        public string name { get; set; }

        [Required]
        [DataMember]
        [Display(Name = "Units")]
        public string units { get; set; }

        [Required]
        [DataMember]
        [Display(Name = "Type")]
        public string type { get; set; }
    }

    public class DevicePlusStreams
    {
        public Device Device { get; set; }
        public List<Stream> Streams { get; set; }
    }

    public class StreamPlusDataStream
    {
        public StreamPlusDataStream()
        {
            dataStreams = new List<DataStream>();
        }
        public Stream stream { get; set; }
        public List<DataStream> dataStreams { get; set; }
    }

    public class DevStreamDataStream
    {
        public DevStreamDataStream()
        {
            DataStreams = new List<DataStream>();
        }
        public Stream Stream { get; set; }
        public List<DataStream> DataStreams { get; set; }

        public Device Device { get; set; }
    }

    [NotMapped]
    public class DeviceWithStreams : Device
    {
        public DeviceWithStreams()
        {
            Streams = new List<StreamWithDefinitions>();
        }

        public DeviceWithStreams(Device dev)
        {
            base.DateCreated = dev.DateCreated;
            base.Description = dev.Description;
            base.DeviceID = dev.DeviceID;
            base.DeviceKey = dev.DeviceKey;
            base.DeviceName = dev.DeviceName;
            base.IPCreated = dev.IPCreated;
            base.IsPrivate = dev.IsPrivate;
            base.UserName = dev.UserName;
            base.User = dev.User;
            Streams = new List<StreamWithDefinitions>();
        }

        [DataMember]
        public List<StreamWithDefinitions> Streams { get; set; }
    }

    [NotMapped]
    public class DeviceWithStreamsAndData : Device
    {
        public DeviceWithStreamsAndData()
        {
            Streams = new List<StreamWithData>();
        }

        public DeviceWithStreamsAndData(Device dev)
        {
            base.DateCreated = dev.DateCreated;
            base.Description = dev.Description;
            base.DeviceID = dev.DeviceID;
            base.DeviceKey = dev.DeviceKey;
            base.DeviceName = dev.DeviceName;
            base.IPCreated = dev.IPCreated;
            base.IsPrivate = dev.IsPrivate;
            base.UserName = dev.UserName;
            base.User = dev.User;
            Streams = new List<StreamWithData>();
        }

        [DataMember]
        public List<StreamWithData> Streams { get; set; }
    }

    [NotMapped]
    public class StreamWithDefinitions : Stream
    {
        public StreamWithDefinitions()
        {
            DataStreams = new List<DataStream>();
        }
        public StreamWithDefinitions(Stream stream)
        {
            base.DateCreated = stream.DateCreated;
            base.Description = stream.Description;
            base.device = stream.device;
            base.IPCreated = stream.IPCreated;
            base.isPrivate = stream.isPrivate;
            base.StreamID = stream.StreamID;
            base.StreamName = stream.StreamName;
            base.Units = stream.Units;
            DataStreams = new List<DataStream>();
        }

        [DataMember]
        public List<DataStream> DataStreams { get; set; }
    }

    [NotMapped]
    public class StreamWithData : Stream
    {
        public StreamWithData()
        {
            DataStreams = new List<DataStream>();
        }
        public StreamWithData(Stream stream)
        {
            base.DateCreated = stream.DateCreated;
            base.Description = stream.Description;
            base.device = stream.device;
            base.IPCreated = stream.IPCreated;
            base.isPrivate = stream.isPrivate;
            base.StreamID = stream.StreamID;
            base.StreamName = stream.StreamName;
            base.Units = stream.Units;
            DataStreams = new List<DataStream>();
            Data = new List<Data>();
        }

        [DataMember]
        public List<DataStream> DataStreams { get; set; }

        [DataMember]
        public List<Data> Data { get; set; }
    }

    [DataContract]
    [Table("Dashboard")]
    public class Dashboard
    {
        [DataMember]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        [Display(Name = "Dashboard ID")]
        public long ID { get; set; }

        [DataMember]
        [Display(Name = "Name")]
        public string Name { get; set; }

        [DataMember]
        [Display(Name = "Description")]
        public string Description { get; set; }

        [DataMember]
        public string SerializedLayout { get; set; }

        [DataMember]
        public bool Hidden { get; set; }

        [IgnoreDataMember]
        public virtual ApplicationUser User { get; set; }

    }

    [DataContract]
    [Table("SharedDashboard")]
    public class SharedDashboard
    {
        [DataMember]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public long ID { get; set; } 
        public virtual ApplicationUser User { get; set; }
        public virtual Dashboard Dashboard { get; set; }

    }
    

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        public DbSet<Device> Devices { get; set; }
        public DbSet<Stream> Streams { get; set; }
        public DbSet<SensorStreamManage.Models.DataStream> DataStreams { get; set; }
        public DbSet<Dashboard> Dashboards { get; set; }
        public DbSet<SharedDashboard> SharedDashboards { get; set; }

    }
}