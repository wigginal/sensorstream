﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;

namespace SensorStreamManage
{
    public class Logger
    {
        private readonly static Lazy<Logger> _instance = new Lazy<Logger>(() => new Logger(ConfigurationManager.AppSettings["LogFile"]));
        private readonly static Lazy<Logger> _troubleinstance = new Lazy<Logger>(() => new Logger(ConfigurationManager.AppSettings["LogFile"] + "trouble"));
        public string filename;
        public string filepath;
        private Object lockobj = new Object();
        Queue<string> stringsToLog = new Queue<string>();

        public static Logger Instance
        {
            get
            {
                return _instance.Value;
            }
        }


        public static Logger TroubleInstance
        {
            get
            {
                return _troubleinstance.Value;
            }
        }

        public Logger(string logFilePath)
        {
            lock (lockobj)
            {
                filepath = logFilePath;
                filename = logFilePath + "managelog-" + DateTime.Now.ToString("yyyyMMdd");
                FileStream write_file = new FileStream(filename, FileMode.OpenOrCreate, FileAccess.ReadWrite);
                StreamWriter writing = new StreamWriter(write_file);
                writing.WriteLine("Log File:" + DateTime.Now + " ***************");
                writing.Close();
                write_file.Close();
            }
        }

        public void LogData(string data)
        {
            string file = filepath + "manage-" + DateTime.Now.ToString("yyyyMMdd");
            lock (lockobj)
            {
                if (file != filename)
                {
                    filename = file;
                    FileStream write_file = new FileStream(file, FileMode.OpenOrCreate, FileAccess.ReadWrite);
                    StreamWriter writing = new StreamWriter(write_file);
                    writing.WriteLine("Log File:" + DateTime.Now + " ***************");
                    writing.Close();
                    write_file.Close();
                }
                stringsToLog.Enqueue(DateTime.Now + " : " + data);
                try
                {
                    using (StreamWriter w = File.AppendText(filename))
                    {
                        while (stringsToLog.Count > 0)
                        {
                            Log(stringsToLog.Peek(), w);
                            stringsToLog.Dequeue();
                        }
                    }


                }
                catch (Exception)
                {
                }
            }
        }

        public void Log(string logMessage, TextWriter w)
        {
            w.Write("\r\nLog Entry : ");
            w.WriteLine("  :");
            w.WriteLine("  :{0}", logMessage);
            w.WriteLine("-------------------------------");
        }
    }
}