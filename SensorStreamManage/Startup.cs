﻿using Microsoft.AspNet.SignalR;
using Microsoft.Owin;
using Microsoft.Owin.Cors;
using Owin;

[assembly: OwinStartupAttribute(typeof(SensorStreamManage.Startup))]
namespace SensorStreamManage
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.Map("/signalr", map =>
            {
                map.UseCors(CorsOptions.AllowAll);
                var hubConfiguration = new HubConfiguration
                {
                    // EnableJSONP = true                    
                    // EnableDetailedErrors = true                                     
                };
                map.RunSignalR(hubConfiguration);
            });
            ConfigureAuth(app);
        }
    }
}
