﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Owin;
using SensorStreamManage.Models;
using System.Net;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using Cassandra;
using Cassandra.Data.Linq;
using SensorStreamCore;
using System.Configuration;
using System.Text;

namespace SensorStreamManage.Controllers
{
    [Authorize]
    public class DeviceController : Controller
    {
        private ApplicationDbContext db;
        private UserManager<ApplicationUser> _userManager;

        public UserManager<ApplicationUser> UserManager
        {
            get
            {
                return _userManager;
            }
            private set
            {
                _userManager = value;
            }
        }

        /// <summary>
        /// The Controller used for device and stream creation, deletion and management
        /// </summary>
        public DeviceController()
        {
            //Create the database connection for EF
            db = new ApplicationDbContext();
            //create a controller to manage users and authentication
            _userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(db));
        }


        // GET: Device Listing
        /// <summary>
        /// Stream ID to download the python file setup
        /// </summary>
        public async Task<ActionResult> GetPython(string id)
        {
            //get the user for verification
            ApplicationUser user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
            if (user == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);
            }

            //get the stream to display
            Stream stream = (from b in db.Streams
                             where b.StreamID == id
                             select b).First();
            if (stream == null)
            {
                return HttpNotFound();
            }

            //retrieve the device as well
            var dev = await db.Devices.FindAsync(stream.device.DeviceID);
            if (dev == null)
            {
                return HttpNotFound();
            }

            //check to make sure the person requesting actually has access to this device
            if (dev.User.Id != user.Id)
            {
                return new HttpStatusCodeResult(HttpStatusCode.Unauthorized);
            }

            //combine the device streams and datastreams to display and pass to view
            List<DataStream> datastreams = db.DataStreams.Where(b => b.Stream.StreamID == stream.StreamID).ToList();
            DevStreamDataStream dsds = new DevStreamDataStream() { Device = dev, Stream = stream, DataStreams = datastreams };

            StringBuilder sb = new StringBuilder();
            sb.Append(System.IO.File.ReadAllText(HttpContext.Server.MapPath("/App_Data/sensorstream.py")));
            sb.AppendLine("stream = \"" + stream.StreamID + "\"");
            sb.AppendLine("key = \"" + dev.DeviceKey + "\"");
            sb.AppendLine("");
            sb.AppendLine("#replace this with the data you would like to send to the stream");
            sb.Append("data = [{ 'streamid' : stream, 'values' : {");
            bool first = true;
            foreach (var ds in datastreams)
            {
                if (!first)
                    sb.Append(" , ");

                sb.Append("'" + ds.name + "' : ");
                if (CassandraUtilities.IsIntegerType(ds.type))
                {
                    sb.Append(" 1 ");
                }
                else if (CassandraUtilities.IsDateType(ds.type))
                {
                    sb.Append("datetime.datetime.now().isoformat()");
                }
                else if (CassandraUtilities.IsBoolType(ds.type))
                {
                    sb.Append("'true'");
                }
                else if (CassandraUtilities.IsGuidType(ds.type))
                {
                    sb.Append("'" + Guid.NewGuid().ToString() + "'");
                }
                else
                {
                    sb.Append("'test data'");
                }
                first = false;
            }
            sb.AppendLine("} }]");

            sb.AppendLine("sendData(key, data)");

            var bytes = System.Text.Encoding.UTF8.GetBytes(sb.ToString());



            return File(bytes, System.Net.Mime.MediaTypeNames.Application.Octet, stream.StreamName + "_sslib.py");

            //byte[] fileBytes = System.IO.File.ReadAllBytes("c:\folder\myfile.ext");
            //string fileName = "myfile.ext";
            //return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);

        }

        #region Device
        // GET: Device Listing
        /// <summary>
        /// Displays the main dashboard showing all the devices that belong to this user
        /// </summary>
        public async Task<ActionResult> Index()
        {
            //get the user and pass through the name so we can personalize the page
            ApplicationUser user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
            ViewBag.Message = user.UserName;

            //Get Devices from the table that belong to this user. If the list is empty display nothing
            try
            {
                if (db != null)
                {
                    List<Device> dev = db.Devices.Where(device => device.User.Id == user.Id).ToList();
                    //if the list is null, there were no devices, fall through to display an empty list
                    if (dev != null)
                    {
                        return View(dev);
                    }
                }
            }
            catch { }

            //Display thos devices oh so nicely
            return View(new List<Device>());
        }

        //GET: /Devices/Details/<ID>
        /// <summary>
        /// Shows the view containing details of device selected by the user
        /// </summary>
        /// <param name="id">device ID to find</param>
        [Authorize]
        public async Task<ActionResult> Details(string id)
        {
            //get the user to ensure they have access to this device
            ApplicationUser user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
            if (user == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            //retrieve the device and streams
            var dev = await db.Devices.FindAsync(id);
            List<Stream> stream = (from b in db.Streams
                                   where b.device.DeviceID == dev.DeviceID
                                   select b).ToList();

            //combine into a single class to pass into the view
            DevicePlusStreams dps = new DevicePlusStreams { Device = dev, Streams = stream };

            //validation
            if (dev == null)
            {
                return HttpNotFound();
            }
            if (dev.User.Id != user.Id)
            {
                return new HttpStatusCodeResult(HttpStatusCode.Unauthorized);
            }

            //have a look
            return View(dps);
        }

        //GET: /Device/Create
        /// <summary>
        /// shows a view allowing user to create a new device
        /// </summary>
        [Authorize]
        public ActionResult Create()
        {
            return View();
        }

        //POST: /Device/Create - Post Device
        /// <summary>
        /// Takes a posted device and creates a new one in SQL and cassandra with this user
        /// </summary>
        /// <param name="newDevice">The binding to the device that was posted</param>
        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "DeviceName,Description,IsPrivate")]Device newDevice)
        {
            //Retrieve the user info
            ApplicationUser user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
            if (user == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            //Check that the user doesnt have a devices with this name already
            int devCount = db.Devices.Count(dev => dev.User.Id == user.Id && dev.DeviceName == newDevice.DeviceName);

            if (devCount > 0)
            {
                ModelState.AddModelError("", "You already have a device with this name.");
            }

            if (ModelState.IsValid)
            {
                //insert into the main database
                newDevice.DeviceID = Guid.NewGuid().ToString();
                newDevice.DeviceKey = Guid.NewGuid().ToString();
                newDevice.UserName = user.UserName.ToString();
                newDevice.DateCreated = DateTimeOffset.Now;
                newDevice.IPCreated = Request.ServerVariables["REMOTE_ADDR"];

                //create the device in cassandra
                if (CreateCassandraDevice(newDevice))
                {
                    newDevice.User = user;
                    db.Devices.Add(newDevice);
                    await db.SaveChangesAsync();
                    return RedirectToAction("Details", new {id = newDevice.DeviceID});
                }
                ModelState.AddModelError("", "Unknown error occured. Please contact us if this problem persists");
            }

            return View(newDevice);
        }

        //GET /Device/Edit/ID
        /// <summary>
        /// Shows the edit field for the selected device
        /// </summary>
        /// <param name="id">the device ID of the device to display</param>
        [Authorize]
        public async Task<ActionResult> Edit(string id)
        {
            //get the user id to ensure they have access to this device
            ApplicationUser user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
            if (user == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            //get the device to be displayed and verify userID
            var dev = await db.Devices.FindAsync(id);
            if (dev == null)
            {
                return HttpNotFound();
            }
            if (dev.User.Id != user.Id)
            {
                return new HttpStatusCodeResult(HttpStatusCode.Unauthorized);
            }
            return View(dev);
        }

        //POST /Device/Edit/ID
        /// <summary>
        /// post function that updates the device
        /// </summary>
        /// <param name="id">the id of the device to update</param>
        [HttpPost, ActionName("Edit")]
        [ValidateAntiForgeryToken]
        [Authorize]
        public async Task<ActionResult> EditDev(string id)
        {
            //get the user id to verify
            ApplicationUser user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
            if (user == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //get the device
            var dev = await db.Devices.FindAsync(id);
            if (dev == null)
            {
                return HttpNotFound();
            }
            //verify access
            if (dev.User.Id != user.Id)
            {
                return new HttpStatusCodeResult(HttpStatusCode.Unauthorized);
            }

            //attempt the update
            if (TryUpdateModel(dev, "", new string[] { "DeviceKey", "IsPrivate" }))
            {
                //update cassandra first
                if (UpdateCassandraDevice(dev))
                {
                    try
                    {
                        db.Entry(dev).State = EntityState.Modified;
                        db.SaveChanges();
                        return RedirectToAction("Index");
                    }
                    catch (RetryLimitExceededException /* dex */)
                    {
                        //Log the error (uncomment dex variable name and add a line here to write a log.
                        ModelState.AddModelError("", "Unable to save changes. Try again, and if the problem persists, see your system administrator.");
                    }
                }
            }
            return View(dev);
        }

        //GET: /Device/Delete/ID
        /// <summary>
        /// Main page to delete the device 
        /// </summary>
        /// <param name="id">id of the device to delete</param>        
        public async Task<ActionResult> Delete(string id)
        {
            //get the user and device then verify access
            var currentUser = await UserManager.FindByIdAsync(User.Identity.GetUserId());
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var dev = await db.Devices.FindAsync(id);
            if (dev == null)
            {
                return HttpNotFound();
            }
            if (dev.User.Id != currentUser.Id)
            {
                return new HttpStatusCodeResult(HttpStatusCode.Unauthorized);
            }
            return View(dev);
        }

        // POST: /Device/Delete/ID
        /// <summary>
        /// Post method to delete device
        /// </summary>
        /// <param name="id">The id of the device to be deleted</param>
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(string id)
        {
            //get user and device then verify access
            var currentUser = await UserManager.FindByIdAsync(User.Identity.GetUserId());
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var dev = await db.Devices.FindAsync(id);
            if (dev == null)
            {
                return HttpNotFound();
            }
            if (dev.User.Id != currentUser.Id)
            {
                return new HttpStatusCodeResult(HttpStatusCode.Unauthorized);
            }

            //delete device and streams and stats from cassandra
            if (DeleteCassandraDevice(dev))
            {
                //remove the device from SQL
                List<Stream> stream = (from b in db.Streams
                                       where b.device.DeviceID == dev.DeviceID && b.device.User.Id == currentUser.Id
                                       select b).ToList();
                if (stream.Count > 0)
                {
                    foreach (var st in stream)
                    {
                        db.DataStreams.RemoveRange(db.DataStreams.Where(b => b.Stream.StreamID == st.StreamID).ToList());
                        db.Streams.Remove(st);
                    }
                    db.SaveChanges();
                }

                db.Devices.Remove(dev);
                await db.SaveChangesAsync();
            }
            return RedirectToAction("Index");
        }
        #endregion

        #region Stream
        //GET /Device/StreamDetails/ID
        /// <summary>
        /// Page with details of given streams
        /// </summary>
        /// <param name="id">id of the stream to display</param>
        [Authorize]
        public async Task<ActionResult> StreamDetails(string id)
        {
            //get the user for verification
            ApplicationUser user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
            if (user == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            //get the stream to display
            Stream stream = (from b in db.Streams
                             where b.StreamID == id
                             select b).First();
            if (stream == null)
            {
                return HttpNotFound();
            }

            //retrieve the device as well
            var dev = await db.Devices.FindAsync(stream.device.DeviceID);
            if (dev == null)
            {
                return HttpNotFound();
            }

            //check to make sure the person requesting actually has access to this device
            if (dev.User.Id != user.Id)
            {
                return new HttpStatusCodeResult(HttpStatusCode.Unauthorized);
            }

            //combine the device streams and datastreams to display and pass to view
            List<DataStream> datastreams = db.DataStreams.Where(b => b.Stream.StreamID == stream.StreamID).ToList();
            DevStreamDataStream dsds = new DevStreamDataStream() { Device = dev, Stream = stream, DataStreams = datastreams };

            return View(dsds);
        }

        //GET: /Device/StreamCreate/ID
        /// <summary>
        /// displays the page to create new devices
        /// </summary>
        /// <param name="id">id of the device to create the stream under</param>
        [Authorize]
        public async Task<ActionResult> StreamCreate(string id)
        {
            //pass the id through 
            ViewBag.DevId = id;

            //retrieve the user to verify access
            ApplicationUser user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
            if (user == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            //get device
            var dev = await db.Devices.FindAsync(id);
            if (dev == null)
            {
                return HttpNotFound();
            }

            //verify access
            if (dev.User.Id != user.Id)
            {
                return new HttpStatusCodeResult(HttpStatusCode.Unauthorized);
            }

            //pass an empty model into the view
            var model = new StreamPlusDataStream();
            model.dataStreams.Add(new DataStream());
            return View(model);
        }


        //POST: /Device/StreamCreate/ID
        /// <summary>
        /// Creates a new stream from the POST data
        /// </summary>
        /// <param name="newStream">the stream to be created</param>
        /// <param name="id">the device to create the stream with</param>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public async Task<ActionResult> StreamCreate(StreamPlusDataStream newStream, string id)
        {
            //pass through ID in event of failure
            ViewBag.DevId = id;
            try
            {
                //get user id for verification
                ApplicationUser user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
                if (user == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }

                //get device
                var dev = await db.Devices.FindAsync(id);
                if (dev == null)
                {
                    return HttpNotFound();
                }

                //verify access
                if (dev.User.Id != user.Id)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.Unauthorized);
                }

                //update all the stream fields
                newStream.stream.IPCreated = Request.ServerVariables["REMOTE_ADDR"].ToString();
                newStream.stream.DateCreated = DateTimeOffset.Now;
                newStream.stream.StreamID = GuidGenerator.GenerateTimeBasedGuid().ToString();
                newStream.stream.device = dev;
                db.Streams.Add(newStream.stream);

                //validate all of the datastreams, ensure types are valid, and ensure that the names are all unique
                Dictionary<string, bool> unique = new Dictionary<string, bool>();
                if (newStream.dataStreams.Count == 0)
                {
                    Logger.Instance.LogData("empty datastream");
                    ModelState.AddModelError("DataStreams", "A stream requires at least one Data Stream");
                }

                //verify data on all the streams
                foreach (var datastream in newStream.dataStreams)
                {
                    //there has to be a name
                    if (datastream.name == null || datastream.type == null)
                    {
                        ModelState.AddModelError("DataStreams", "All data streams require a valid name");
                        throw new Exception("Cannot have null type or name in the datastreams");
                    }

                    //stream name cannot start with a number
                    if (Char.IsDigit(datastream.name[0]))
                    {
                        ModelState.AddModelError("DataStreams", "Data Streams cannot start with a number");
                        throw new Exception("Data Streams cannot start with a number");
                    }

                    //and it cant be the singular word 'ON' it will fail in cassandra
                    if (datastream.name.Equals("on", StringComparison.OrdinalIgnoreCase))
                    {
                        ModelState.AddModelError("DataStreams", "The name of the data stream cannot be the word 'on'");
                        throw new Exception("The name of the data stream cannot be the word 'on'");
                    }

                    //check the name of the stream against the database
                    if (unique.ContainsKey(datastream.name))
                    {
                        Logger.Instance.LogData("duplicate keys");
                        ModelState.AddModelError("DataStreams", "Each data stream requires a unique name");
                    }
                    else
                    {
                        Logger.Instance.LogData("adding " + datastream.name);
                        unique.Add(datastream.name, true);
                    }

                    datastream.Stream = newStream.stream;

                    //check the type of the stream agains teh valid types
                    if (!CassandraUtilities.IsValidType(datastream.type))
                    {
                        Logger.Instance.LogData("invalid type");
                        ModelState.AddModelError("DataStreams", "DataStream Contains invalid type");
                    }
                    db.DataStreams.Add(datastream);
                }

                //check to see the model validated and then create the streams
                if (ModelState.IsValid)
                {
                    //Create the cassandra streams
                    if (CreateCassandraStream(dev, newStream))
                    {
                        //Save the changes to the sql satabase
                        db.SaveChanges();
                        //rdirect back to the details page
                        return RedirectToAction("Details", new { id = id });
                    }
                    else
                    {
                        ModelState.AddModelError("", "An anknown error occured. Please try again.");
                    }
                }
                else
                {
                    //if the model state wasnt valid write out the details and return the error
                    var allErrors = ModelState.Values.SelectMany(v => v.Errors);
                    foreach (var v in allErrors)
                    {
                        Logger.Instance.LogData(v.ErrorMessage);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Instance.LogData(ex.ToString());
            }
            //validation and insert failed. go back to the same screen
            return View(newStream);
        }

        //GET: /Device/StreamRowCreate/
        /// <summary>
        /// Template for rendering stream rows, currently unused
        /// </summary>
        [Authorize]
        public ActionResult StreamRowCreate()
        {
            return PartialView("StreamRowCreate", new DataStream());
        }

        //GET: /Device/StreamDelete/StreamID
        /// <summary>
        /// Shows page for deleting a single stream
        /// </summary>
        /// <param name="id">the id of the stream to delete</param>
        public async Task<ActionResult> StreamDelete(string id)
        {
            //get the user ID for verification
            ApplicationUser user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
            if (user == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            //retrieve the stream to delete
            Stream stream = (from b in db.Streams
                             where b.StreamID == id
                             select b).First();
            if (stream == null)
            {
                return HttpNotFound();
            }

            //get all the fields of this stream for deletion
            List<DataStream> datastreams = db.DataStreams.Where(b => b.Stream.StreamID == stream.StreamID).ToList();
            DevStreamDataStream dsds = new DevStreamDataStream() { Device = null, Stream = stream, DataStreams = datastreams };

            //verify user owns the device
            if (stream.device.User.Id != user.Id)
            {
                return new HttpStatusCodeResult(HttpStatusCode.Unauthorized);
            }

            //pass id though to create back links
            ViewBag.DevId = stream.device.DeviceID;

            //display the stream for deletion
            return View(dsds);
        }

        //POST: /Device/StreamDelete/StreamID
        /// <summary>
        /// Confirms the deletion of the stream
        /// </summary>
        /// <param name="id">id of the stream to delete</param>
        [Authorize]
        [HttpPost, ActionName("StreamDelete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> StreamDeleteComplete(string id)
        {
            //get the user for authorization
            ApplicationUser user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
            if (user == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            //retrieve the stream to be deleted
            Stream stream = (from b in db.Streams
                             where b.StreamID == id
                             select b).First();

            if (stream == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            //verify this user owns it
            if (stream.device.User.Id != user.Id)
            {
                return new HttpStatusCodeResult(HttpStatusCode.Unauthorized);
            }

            //retrieve the datastreams and the device ID
            var devid = stream.device.DeviceID;
            List<DataStream> datastreams = db.DataStreams.Where(b => b.Stream.StreamID == stream.StreamID).ToList();

            //remove from cassandra and then SQL
            if (DeleteCassandraStream(stream))
            {
                db.DataStreams.RemoveRange(datastreams);
                db.Streams.Remove(stream);
                db.SaveChanges();
                //back to the main device listing
                return RedirectToAction("Details", new { id = devid });
            }
            //post failed, redisplay the confirm screen
            return View();
        }


        //GET: /Device/StreamDeleteALL/DeviceID
        /// <summary>
        /// Deletes all the streams from a given device
        /// </summary>
        /// <param name="id">the device id to delete all the streams from</param>
        public async Task<ActionResult> StreamDeleteAll(string id)
        {
            //get the user for verification
            ApplicationUser user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
            if (user == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            //COllection of streams to delete
            List<DevStreamDataStream> streamsToDelete = new List<DevStreamDataStream>();

            //retrieve all the streams associated with this device
            List<Stream> stream = (from b in db.Streams
                                   where b.device.DeviceID == id && b.device.User.Id == user.Id
                                   select b).ToList();
            if (stream.Count == 0)
            {
                return HttpNotFound();
            }

            //get all the DataStreams for each stream
            foreach (var st in stream)
            {
                streamsToDelete.Add(new DevStreamDataStream { Stream = st, DataStreams = db.DataStreams.Where(b => b.Stream.StreamID == st.StreamID).ToList() });
            }

            //pass through device id for back button
            ViewBag.DevId = id;
            return View(streamsToDelete);
        }

        //POST: /Device/StreamDeleteAll/DeviceID
        /// <summary>
        /// Deletes all of the streams from a given device
        /// </summary>
        /// <param name="id">the id of the device to delete all the streams from</param>
        [Authorize]
        [HttpPost, ActionName("StreamDeleteAll")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> StreamDeleteAllConfirm(string id)
        {
            //get the user for verification
            ApplicationUser user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
            if (user == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            //collect the streams to delete
            List<Stream> stream = (from b in db.Streams
                                   where b.device.DeviceID == id && b.device.User.Id == user.Id
                                   select b).ToList();
            if (stream.Count == 0)
            {
                return HttpNotFound();
            }

            //delete the streams from cassandra
            if (DeleteCassandraStreams(stream))
            {
                //delete each stream from SQL
                foreach (var st in stream)
                {
                    db.DataStreams.RemoveRange(db.DataStreams.Where(b => b.Stream.StreamID == st.StreamID).ToList());
                    db.Streams.Remove(st);
                }
                db.SaveChanges();
            }

            //return to the device details page
            ViewBag.DevId = id;
            return RedirectToAction("Details", new { id = id });
        }
        #endregion

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool CreateCassandraStream(Device dev, StreamPlusDataStream newStream)
        {
            try
            {
                //get the device ID from the header to match with a device
                Guid devID = Guid.Parse(dev.DeviceID);

                //get a Cassandra session and contexts for devices and streams
                ISession session = CassandraInstance.Instance.Session;

                //get the tables needed to verify the device ID and create a new streams
                Table<CassandraSensorDevice> sensetable = SessionExtensions.GetTable<CassandraSensorDevice>(session);
                Table<CassandraStream> streamTable = SessionExtensions.GetTable<CassandraStream>(session);
                Table<CassandraStreamByDevice> streambydev = SessionExtensions.GetTable<CassandraStreamByDevice>(session);

                //get the matching device from the guid provided
                CassandraSensorDevice scd = sensetable.FirstOrDefault(d => d.guid == devID).Execute();

                //if this is null, the deviceID was invalid return the error
                if (scd == null)
                {
                    return false;
                }

                //create the new stream with the collected data
                CassandraStream cs = new CassandraStream
                {
                    IPCreated = newStream.stream.IPCreated,
                    Created = newStream.stream.DateCreated,
                    Name = newStream.stream.StreamName,
                    StreamID = Guid.Parse(newStream.stream.StreamID),
                    Description = newStream.stream.Description,
                    Streams = new List<string>(),
                    devName = dev.DeviceName,
                    User = dev.UserName,
                    privatestream = newStream.stream.isPrivate
                };

                foreach (var data in newStream.dataStreams)
                {
                    string stream = "{\"name\":\"" + data.name + "\",\"units\":\"" + data.units + "\",\"type\":\"" + data.type + "\"}";
                    cs.Streams.Add(stream);
                }

                //create the stream for the device table
                CassandraStreamByDevice streamDev = new CassandraStreamByDevice
                {
                    deviceID = devID,
                    StreamID = cs.StreamID,
                    Name = newStream.stream.StreamName
                };

                var insertBatch = new BatchStatement()
                    .Add(streamTable.Insert(cs))
                    .Add(streambydev.Insert(streamDev));

                //commit the changes to Cassandra
                session.Execute(insertBatch);

                Caching.DeleteKey("getstreams" + scd.DeviceName + scd.UserName);

                //create the new data table to store all the stream data
                string execute = "CREATE TABLE data" + cs.StreamID.ToString().Replace("-", "") + " ( Date text, Tuid timeuuid";
                foreach (var data in newStream.dataStreams)
                {
                    execute += ", " + data.name.Replace(' ', '_') + " " + data.type;
                }
                execute += ", PRIMARY KEY (Date, Tuid) );";

                Logger.Instance.LogData("String to create a new table : " + execute);
                session.Execute(new SimpleStatement(execute));
            }
            catch (Exception ex)
            {
                Logger.Instance.LogData(newStream.stream.IPCreated + " : " + ex.ToString());
                return false;
            }
            return true;
        }

        private bool DeleteCassandraStream(Stream stream)
        {
            try
            {
                CassandraUtilities.DeleteStream(Request.ServerVariables["REMOTE_ADDR"], Guid.Parse(stream.device.DeviceID), Guid.Parse(stream.StreamID));
            }
            catch
            {
                return false;
            }
            return true;
        }

        private bool DeleteCassandraStreams(List<Stream> streams)
        {
            foreach (var stream in streams)
            {
                try
                {
                    CassandraUtilities.DeleteStream(Request.ServerVariables["REMOTE_ADDR"], Guid.Parse(stream.device.DeviceID), Guid.Parse(stream.StreamID));
                }
                catch
                {
                    return false;
                }
            }
            return true;
        }

        private bool CreateCassandraDevice(Device newDevice)
        {
            try
            {
                //get the session and context for the Cassandra cluster and the device table
                ISession session = CassandraInstance.Instance.Session;

                //get the tables for the device we are going to be adding
                Table<CassandraSensorDevice> sensordeviceTable = SessionExtensions.GetTable<CassandraSensorDevice>(session);
                Table<CassandraDevicesByGUID> sensordeviceGUIDTable = SessionExtensions.GetTable<CassandraDevicesByGUID>(session);
                Table<CassandraDeviceByUsername> sensordeviceUserTable = SessionExtensions.GetTable<CassandraDeviceByUsername>(session);

                //ensure the user doesn't have a device with this name already
                if (sensordeviceTable.Where(dev => dev.UserName == newDevice.UserName && dev.DeviceName == newDevice.DeviceName).Count().Execute() > 0)
                {
                    Logger.TroubleInstance.LogData("Device passed model check with valid name, but the name isnt unique: FIX ME - user: " +  newDevice.UserName + "  device Name: " + newDevice.DeviceName );
                    return false;
                }

                //create a Cassandra sensor device to be stored from the current json object
                CassandraSensorDevice csd = new CassandraSensorDevice
                {
                    LatestIP = newDevice.IPCreated,
                    Created = newDevice.DateCreated,
                    Description = newDevice.Description,
                    DeviceName = newDevice.DeviceName,
                    UserName = newDevice.UserName,
                    key = Guid.Parse(newDevice.DeviceKey),
                    privatedevice = newDevice.IsPrivate,
                    guid = Guid.Parse(newDevice.DeviceID)
                };

                //store the device by GUID as well so we can easily look it up
                CassandraDevicesByGUID devguid = new CassandraDevicesByGUID
                {
                    guid = csd.guid,
                    DeviceName = csd.DeviceName,
                    UserName = csd.UserName
                };

                //and again store it by user name so we can look up all devices from a given user
                CassandraDeviceByUsername userDev = new CassandraDeviceByUsername
                {
                    UserName = csd.UserName,
                    DeviceName = csd.DeviceName
                };

                //Add the device to the tables
                sensordeviceTable.Insert(csd).Execute();
                sensordeviceGUIDTable.Insert(devguid).Execute();
                sensordeviceUserTable.Insert(userDev).Execute();

                Caching.DeleteKey("getdevices");

            }
            catch (Exception ex)
            {
                Logger.Instance.LogData(ex.ToString());
                DeleteCassandraDevice(newDevice);
                return false;
            }
            return true;
        }

        private bool UpdateCassandraDevice(Device updatedDevice)
        {
            try
            {
                //get the session and context for the Cassandra cluster and the device table
                ISession session = CassandraInstance.Instance.Session;

                //get the tables for the device we are going to be adding
                Table<CassandraSensorDevice> sensordeviceTable = SessionExtensions.GetTable<CassandraSensorDevice>(session);

                //ensure the user doesn't have a device with this name already
                CassandraSensorDevice csd = sensordeviceTable.Where(dev => dev.UserName == updatedDevice.UserName && dev.DeviceName == updatedDevice.DeviceName).FirstOrDefault().Execute();
                if (csd == null)
                {
                    return false;
                }

                Guid parsed = Guid.Parse(updatedDevice.DeviceKey);
                sensordeviceTable.Where(dev => dev.UserName == updatedDevice.UserName && dev.DeviceName == updatedDevice.DeviceName).Select(dev => new CassandraSensorDevice { Description = updatedDevice.Description, key = parsed, privatedevice = updatedDevice.IsPrivate }).Update().Execute();

                Caching.DeleteKey("getdevices");
            }
            catch (Exception ex)
            {
                Logger.Instance.LogData(updatedDevice.DeviceKey + " " + ex.ToString());
                return false;
            }
            return true;
        }

        private bool DeleteCassandraDevice(Device deleteDevice)
        {
            //get the Cassandra cluster and devices
            ISession session = CassandraInstance.Instance.Session;

            //get the tables containing the devices
            Table<CassandraSensorDevice> sensordeviceTable = SessionExtensions.GetTable<CassandraSensorDevice>(session);
            Table<CassandraDevicesByGUID> sensordeviceGUIDTable = SessionExtensions.GetTable<CassandraDevicesByGUID>(session);
            Table<CassandraDeviceByUsername> sensordeviceUserTable = SessionExtensions.GetTable<CassandraDeviceByUsername>(session);
            Guid devID;
            if (!Guid.TryParse(deleteDevice.DeviceID, out devID))
            {

            }
            try
            {

                //check this device to ensure it exists
                CassandraDevicesByGUID cdbg = sensordeviceGUIDTable.First(dev => dev.guid == devID).Execute();
                CassandraSensorDevice csd = sensordeviceTable.First(dev => dev.UserName == cdbg.UserName && dev.DeviceName == cdbg.DeviceName).Execute();
                CassandraDeviceByUsername cdbu = sensordeviceUserTable.First(dev => dev.UserName == csd.UserName && dev.DeviceName == csd.DeviceName).Execute();

                //remove from the database
                Batch b = session.CreateBatch();
                b.Append(sensordeviceGUIDTable.Where(dev => dev.guid == devID).Delete());
                b.Append(sensordeviceTable.Where(dev => dev.UserName == cdbg.UserName && dev.DeviceName == cdbg.DeviceName).Delete());
                b.Append(sensordeviceUserTable.Where(dev => dev.UserName == csd.UserName && dev.DeviceName == csd.DeviceName).Delete());
                Logger.Instance.LogData(b.ToString());
                b.Execute();
            }
            catch (Exception ex)
            {
                Logger.Instance.LogData(Request.ServerVariables["REMOTE_ADDR"] + " : " + ex.ToString());
            }

            //get the context for the streams
            Table<CassandraStreamByDevice> streambydev = SessionExtensions.GetTable<CassandraStreamByDevice>(session);

            try
            {
                //get all the streams matching this GUID
                foreach (var stream in streambydev.Where(dev => dev.deviceID == devID).Execute())
                {
                    CassandraUtilities.DeleteStream(Request.ServerVariables["REMOTE_ADDR"], devID, stream.StreamID);
                }
            }
            catch (Exception ex)
            {
                Logger.Instance.LogData("Error deleting streams in device.ashx(maybe there weren't any): " + ex.ToString());
            }

            Caching.DeleteKey("getdevices");
            return true;
        }
    }
}