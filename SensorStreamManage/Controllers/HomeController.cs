﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SensorStreamManage.Controllers
{
    public class HomeController : Controller
    {
        [AllowAnonymous]
        public ActionResult Index()
        {
            return View();
        }

        [AllowAnonymous]
        public ActionResult About()
        {
            ViewBag.Message = "What is strmr</small>?";

            return View();
        }

        [AllowAnonymous]
        public ActionResult Dashboard()
        {

            return View();
        }


        [AllowAnonymous]
        public ActionResult Contact()
        {
            ViewBag.Message = "Contact us!";

            return View();
        }


        [AllowAnonymous]
        public ActionResult Samples()
        {
            ViewBag.Message = "Samples";

            return View();
        }

        [AllowAnonymous]
        public ActionResult Documentation()
        {
            ViewBag.Message = "Documentation";

            return View();
        }

        [AllowAnonymous]
        public ActionResult Libraries()
        {
            ViewBag.Message = "Libraries";

            return View();
        }


        public ActionResult MyDevices()
        {
            ViewBag.Message = "Devices";

            return View();
        }
    }
}