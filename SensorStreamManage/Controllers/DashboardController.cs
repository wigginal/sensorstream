﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Newtonsoft.Json;
using SensorStreamManage.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace SensorStreamManage.Controllers
{
    public class DashboardController : Controller
    {

        private ApplicationDbContext db;
        private UserManager<ApplicationUser> _userManager;

        public UserManager<ApplicationUser> UserManager
        {
            get
            {
                return _userManager;
            }
            private set
            {
                _userManager = value;
            }
        }

        /// <summary>
        /// The Controller used for device and stream creation, deletion and management
        /// </summary>
        public DashboardController()
        {
            //Create the database connection for EF
            db = new ApplicationDbContext();
            //create a controller to manage users and authentication
            _userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(db));
        }

        // GET: Dashboard
        [AllowAnonymous]
        public async Task<ActionResult> ViewBoard(string id)
        {
            ApplicationUser user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
            //if (user == null)
            //{
                //return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            //}

            long idOut;

            if (!long.TryParse(id,out idOut))
            {
                return View("Dashboard");
            }

            Dashboard items = db.Dashboards.First(dash => dash.ID == idOut);
            if (items.Hidden == true )
            {
                if(user == null || items.User.Id != user.Id)
                    return new HttpStatusCodeResult(HttpStatusCode.Unauthorized);
            }
            ViewBag.Layout = items.SerializedLayout;
            return View("Dashboard");
        }


        // GET: Dashboard
        public async Task<ActionResult> Index()
        {
            ApplicationUser user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
            if (user == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }
            List<Dashboard> items = db.Dashboards.Where(dash => dash.User.Id == user.Id).ToList();
            return View("MyDashboards",items);
        }

        [HttpGet]
        public ActionResult SaveDashboard()
        {
            Dashboard db = new Dashboard();
            return PartialView("_SaveDashboardPartial", db);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SaveDashboard(string dash)
        {
            ApplicationUser user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
            if (user == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }
            Logger.Instance.LogData(dash);
            Dashboard data = JsonConvert.DeserializeObject<Dashboard>(dash);
            if (data.Name == null || data.SerializedLayout == null)
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);

            
            data.User = user;
            
            //check if the user has one with this name
            if (db.Dashboards.Count(board => board.Name == data.Name && board.User.Id == user.Id) > 0){
                ModelState.AddModelError("Name", "Named Dashboard already exists for this user");
                return PartialView("_SaveDashboardPartial", data);
            }
            db.Dashboards.Add(data);
            db.SaveChanges();
            return new HttpStatusCodeResult(HttpStatusCode.OK); 
        }
    }
}