﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http.Description;
using SensorStreamManage.Models;
using Cassandra;
using Cassandra.Data.Linq;
using SensorStreamCore;
using System.Text;
using System.Threading;
using Newtonsoft.Json;
using System.Collections.Concurrent;
using System.Web.Http.Cors;
using System.IO;
using System.Diagnostics;
using System.Web.Http;

namespace SensorStreamManage.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class APIController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();


        // GET: api/GetDevices
        public IQueryable<Device> GetDevices(string User = null, string Dev = null, string Desc = null)
        {
            IQueryable<Device> returnDevices = null;

            if (User != null)
            {
                returnDevices = db.Devices.Where(dev => dev.IsPrivate == false && dev.UserName == User);
            }

            if (Desc != null)
            {
                if (returnDevices != null)
                    returnDevices = returnDevices.Where(dev => dev.Description.Contains(Desc));
                else
                    returnDevices = db.Devices.Where(dev => dev.IsPrivate == false && dev.Description.Contains(Desc));
            }

            if (Dev != null)
            {
                if (returnDevices != null)
                    returnDevices = returnDevices.Where(dev => dev.DeviceName.Contains(Dev));
                else
                    returnDevices = db.Devices.Where(dev => dev.IsPrivate == false && dev.DeviceName.Contains(Dev));
            }

            if (returnDevices == null)
            {
                returnDevices = db.Devices.Where(dev => dev.IsPrivate == false);
            }

            return returnDevices;
        }

    
        // GET: api/GetDevice/<deviceID>
        [ResponseType(typeof(Device))]
        public async Task<IHttpActionResult> GetDevice(string ids, string Key = null)
        {
            Device device = await db.Devices.FindAsync(ids);
            if (device == null)
            {
                return NotFound();
            }

            if (device.IsPrivate == true && device.DeviceKey != Key)
            {
                return NotFound();
            }

            return Ok(device);
        }

        // GET: api/GetStreams/<deviceID>
        [ResponseType(typeof(DeviceWithStreams))]
        public async Task<IHttpActionResult> GetStreams(string id, string Key = null)
        {
            Device dev = await db.Devices.FindAsync(id);
            if (dev == null)
            {
                return NotFound();
            }

            DeviceWithStreams dws = new DeviceWithStreams(dev);

            if (dws.IsPrivate == true && dws.DeviceKey != Key)
            {
                return NotFound();
            }
            List<Models.Stream> s;
            //retrieve the streams
            if (dws.IsPrivate)
            {
                s = db.Streams.Where(b => b.device.DeviceID == id).ToList();
            }
            else
            {
                s = db.Streams.Where(b => b.device.DeviceID == id && b.isPrivate == false).ToList();
            }

            if (s.Count == 0)
            {
                return NotFound();
            }

            foreach (var stream in s)
            {
                StreamWithDefinitions swd = new StreamWithDefinitions(stream);
                swd.DataStreams = db.DataStreams.Where(b => b.Stream.StreamID == stream.StreamID).ToList();
                dws.Streams.Add(swd);
            }
            return Ok(dws);
        }

        // GET: api/GetStream/<streamID>
        [ResponseType(typeof(DeviceWithStreams))]
        public async Task<IHttpActionResult> GetStream(string id, string Key = null)
        {
            Models.Stream stream = (from b in db.Streams
                                    where b.StreamID == id
                                    select b).First();
            if (stream == null)
            {
                return NotFound();
            }

            Device thedevice = await db.Devices.FindAsync(stream.device.DeviceID);
            if (thedevice == null)
            {
                return NotFound();
            }

            if (thedevice.IsPrivate == true && thedevice.DeviceKey != Key)
            {
                return NotFound();
            }

            DeviceWithStreams dws = new DeviceWithStreams(thedevice);
            if (dws == null)
            {
                return NotFound();
            }

            dws.Streams.Add(new StreamWithDefinitions(stream));
            dws.Streams[0].DataStreams = db.DataStreams.Where(b => b.Stream.StreamID == stream.StreamID).ToList();
            return Ok(dws);
        }

        [ResponseType(typeof(DeviceWithStreamsAndData))]
        public async Task<IHttpActionResult> GetData(string id, string timeonly = null, Int32? count = null, DateTime? end = null, DateTime? start = null, string Key = null)
        {
            //get the stream and devices for the return object
            Models.Stream stream = (from b in db.Streams
                                    where b.StreamID == id
                                    select b).First();
            if (stream == null)
            {
                return NotFound();
            }

            //the device to be returned
            Device thedevice = await db.Devices.FindAsync(stream.device.DeviceID);
            if (thedevice == null)
            {
                return NotFound();
            }

            if (thedevice.IsPrivate == true && thedevice.DeviceKey != Key)
            {
                return NotFound();
            }

            DeviceWithStreamsAndData dwsad = new DeviceWithStreamsAndData(thedevice);
            if (dwsad == null)
            {
                return NotFound();
            }

            dwsad.Streams.Add(new StreamWithData(stream));
            dwsad.Streams[0].DataStreams = db.DataStreams.Where(b => b.Stream.StreamID == stream.StreamID).ToList();
            try
            {
                //Now get the data
                string exec = "";
                if (timeonly != null)
                {
                    exec = "SELECT tuid FROM data" + stream.StreamID.Replace("-", "");
                }
                else
                {
                    exec = "SELECT * FROM data" + stream.StreamID.ToString().Replace("-", "");
                }

                int items;
                //get the number of items requested, reduce the number if it is too high
                if (count != null)
                {
                    if (count > 50000)
                    {
                        items = 50000;
                    }
                    else
                    {
                        items = (int)count;
                    }
                }
                else
                {
                    items = 10000;
                }

                BuildDataQueryString(stream.StreamID, items, start, end, ref exec);
                Logger.Instance.LogData(exec);
                //QueryOptions q = new QueryOptions();
                //q.SetPageSize(int.MaxValue);
                ISession session = CassandraInstance.Instance.Session;
                SimpleStatement ps = new SimpleStatement(exec);
                ps.SetPageSize(int.MaxValue);
                //execute the statement
                RowSet rs = session.Execute(ps);
                //Logger.Instance.LogData("Found rows: " + rs.Count().ToString());
                //if they only want times
                if (timeonly != null)
                {
                    //iterate through the rows, adding the data to the stream
                    foreach (Row row in rs.GetRows())
                    {
                        //if the data is a blob we need to re encode it before transmitting it
                        dwsad.Streams[0].Data.Add(new Data { time = GuidGenerator.GetDateTimeOffset(Guid.Parse(row["tuid"].ToString())).ToString("yyyy-MM-ddTHH:mm:ss.FFF%K") });
                    } //serialize the data and write it out
                }
                else
                {
                    //create a return object
                    List<Row> test = rs.ToList();

                    foreach (Row row in test)
                    {
                        Data data = new Data { time = GuidGenerator.GetDateTimeOffset(Guid.Parse(row["tuid"].ToString())).ToString("yyyy-MM-ddTHH:mm:ss.FFF%K"), values = new Dictionary<string, string>() };

                        //if the data is a blob we need to re encode it before transmitting it
                        foreach (DataStream key in dwsad.Streams[0].DataStreams)
                        {
                            string value = String.Empty;
                            string keyName = key.name.Replace(' ', '_').ToLower();
                            //does the key exist in the row
                            try
                            {
                                if (row[keyName] == null)
                                {
                                    data.values.Add(key.name, "");
                                    continue;
                                }

                                //if it is a blob type convert it and store it to a string
                                if (key.type == "blob")
                                    value = Encoding.ASCII.GetString((byte[])row[keyName]);
                                else
                                    value = row[keyName].ToString();
                                //add it to the dictionary
                                data.values.Add(key.name, value);
                            }
                            catch (Exception)
                            {
                                data.values.Add(key.name, "");
                            }
                        }
                        //add it to the output
                        dwsad.Streams[0].Data.Add(data);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Instance.LogData(ex.ToString());
            }
            //return the data
            return Ok(dwsad);
        }

        public HttpResponseMessage DataSocket()
        {
            if (System.Web.HttpContext.Current.IsWebSocketRequest)
            {
                //System.Web.HttpContext.Current.AcceptWebSocketRequest(new DataHubSockets(Guid.NewGuid().ToString()));
                return Request.CreateResponse(HttpStatusCode.SwitchingProtocols);
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }

        [System.Web.Http.HttpPost]
        public IHttpActionResult AddData()
        {
            try
            {
                String jsonString = new StreamReader(System.Web.HttpContext.Current.Request.InputStream).ReadToEnd();
                Logger.Instance.LogData("json? " + jsonString);

                List<Data> data = JsonConvert.DeserializeObject<List<Data>>(jsonString);

                Logger.Instance.LogData("list is " + data.Count);


                string Key = null;
                if (Request.Headers.Contains("Key"))
                {
                    Key = Request.Headers.GetValues("Key").First();
                }
                else
                {
                    Logger.Instance.LogData("No key in header " + data.Count);
                    return NotFound();
                }

                //get device associated with this key
                Device device = db.Devices.Where(dev => dev.DeviceKey == Key).First();

                if (device == null)
                {
                    Logger.Instance.LogData("device was null");
                    return NotFound();
                }

                //get a list of streams associated with this device key
                List<string> streams = (from b in db.Streams
                                        where b.device.DeviceID == device.DeviceID
                                        select b.StreamID).ToList();

                //long StartingTime = Stopwatch.GetTimestamp();

                if (streams.Count == 0)
                {
                    Logger.Instance.LogData("No stream found");
                    return NotFound();
                }

                int failed = 0;
                int idSet = 0;
                //validate the JSON objects
                Guid StreamID = new Guid();
                Parallel.ForEach(data, jd =>
                {
                    if (jd.streamid == null)
                    {
                        Interlocked.Increment(ref failed);
                        return;
                    }

                    if(Interlocked.Increment(ref idSet) == 1){
                        StreamID = Guid.Parse(jd.streamid);
                    }
                    
                    //try and parse the datetime string
                    try
                    {
                        jd.parsedtime = new DateTimeOffset(DateTime.Parse(jd.time));
                    }
                    catch (Exception)
                    {
                        jd.parsedtime = null;
                    }
                });

                //if one or more of the above items are missing a stream just fail out
                if (failed > 0)
                {
                    //Utilities.LogAndWriteErrorResponse(context, ip + " : data.ashx : streamID missing in one or more data objects", "streamID missing in one or more data objects");
                    Logger.Instance.LogData("streamID Missing Field Exception");
                    return InternalServerError();
                }

                //get the device ID so we can verify this is a valid data push
                ConcurrentDictionary<string, Queue<object[]>> PreparedStatements = new ConcurrentDictionary<string, Queue<object[]>>();

                ParallelOptions po = new ParallelOptions();
                po.MaxDegreeOfParallelism = 48;
                failed = 0;
                object lockObject = new object();

                StringBuilder batchExec = new StringBuilder(data.Count * 250);
                StringBuilder batchSignalr = new StringBuilder(data.Count * 250);

                batchExec.Append("BEGIN BATCH");
                batchExec.Append(Environment.NewLine);

                Parallel.ForEach(data, po, () => new StringBuilder[2]{ new StringBuilder(50000), new StringBuilder(50000)},

                    (json, loopState, partialResults) =>
                    {
                        string[] broadcastArray = CreateInsertString("", ref failed, streams, json);
                        partialResults[0].Append(broadcastArray[0]);
                        partialResults[1].Append( ","+ broadcastArray[1]);
                        return partialResults;
                    },

                    (localPartialSum) =>
                    {
                        // Enforce serial access to single, shared result
                        lock (lockObject)
                        {
                            batchExec.Append(localPartialSum[0]);
                            batchSignalr.Append(localPartialSum[1]);
                        }
                    }
                );

                batchExec.Append(Environment.NewLine);
                batchExec.Append("APPLY BATCH;");
                batchSignalr[0] = '[';
                batchSignalr.Append("]");
                if (failed > 0)
                {
                    //Utilities.LogAndWriteErrorResponse(context, ip + " : data.ashx : Failed parsing the values to insert", "Failed parsing the values to insert");
                    Logger.Instance.LogData("failed parsing insert values");
                    return InternalServerError();
                }

                try
                {
                    //insert the object. When the new version of the api arrives we will turn this into a batch statement
                    ISession session = CassandraInstance.Instance.Session;
                    session.Execute(batchExec.ToString());
                    DataBroadcaster.Instance.BroadcastData(StreamID, batchSignalr.ToString());
                }
                catch (Exception ex)
                {
                    Logger.Instance.LogData(ex.ToString());
                    return InternalServerError();
                }
                //SendSuccessResponse(context);

                //long EndingTime = Stopwatch.GetTimestamp();
                //long ElapsedTime = EndingTime - StartingTime;

                //double ElapsedSeconds = ElapsedTime * (1.0 / Stopwatch.Frequency);

                //Logger.Instance.LogData(device.DeviceID + "  Request took: " + ElapsedSeconds * 1000);
                return Ok();
            }
            catch (Exception ex)
            {
                Logger.Instance.LogData(ex.ToString());
                return InternalServerError();
            }

        }

        private void BuildDataQueryString(string id, int count, DateTime? start, DateTime? finish, ref string exec)
        {
            Guid streamID = Guid.Parse(id);
            DateTime begin = DateTime.Now;
            DateTime end = DateTime.Now;
            if (start != null)
                begin = (DateTime)start;
            if (finish != null)
                end = (DateTime)finish;

            if (start != null && finish != null)
            {
                //create the list of hour string possible, then retrieve by tuid desc
                Guid begintuid = GuidGenerator.GenerateTimeBasedGuid(begin);
                Guid endtuid = GuidGenerator.GenerateTimeBasedGuid(end);

                if (begin > end)
                {
                    DateTime holder = end;
                    end = begin;
                    begin = holder;
                }

                exec += " where date IN (";
                exec += " '" + begin.ToString("yyyyMMddHH") + "' ";
                begin = begin.AddHours(1);

                while (begin < end)
                {
                    exec += ", '" + begin.ToString("yyyyMMddHH") + "' ";
                    begin = begin.AddHours(1);
                }
                exec += " ) and tuid >= " + begintuid + " and tuid <= " + endtuid + " ORDER BY tuid DESC";
            }
            else
            {
                //We need stats for all these to determine how many 
                if (start != null)
                {
                    Guid test = GuidGenerator.GenerateTimeBasedGuid(begin);

                    exec += " where date IN (";
                    exec += Statistics.Instance.GetInKeysStart(streamID, count, begin);
                    exec += ") and tuid >= " + test + " ORDER BY tuid DESC";
                }
                else if (finish != null)
                {
                    Guid test = GuidGenerator.GenerateTimeBasedGuid(end);

                    exec += " where date IN (";
                    exec += Statistics.Instance.GetInKeysEnd(streamID, count, end);
                    exec += ") and tuid <= " + test + " ORDER BY tuid DESC";
                }
                else
                {
                    exec += " where date IN (";
                    exec += Statistics.Instance.GetInKeys(streamID, count);
                    exec += ") ORDER BY tuid DESC";
                }
            }
            exec += " LIMIT " + count.ToString() + ";";
        }

        private static string[] CreateInsertString(string ip, ref int failed, List<string> streams, Data json)
        {
            //parse the StreamID so we can insert this data into the correct stream
            Guid StreamID = Guid.Parse(json.streamid);

            //make sure the streamID matches the deviceID to ensure security
            if (!streams.Contains(json.streamid))
            {
                Logger.Instance.LogData(ip + " : Invalid \"key\" in the header, stream and device dont match ");
                Interlocked.Increment(ref failed);
                return new string[2]{"",""};
            }

            using (ApplicationDbContext ac = new ApplicationDbContext())
            {
                var streamValueNameToType = ac.DataStreams.Where(ds => ds.Stream.StreamID == json.streamid).ToDictionary(ds => ds.name);
                //check the units so we can parse the value
                //Dictionary<string, string> streamValueNameToType = CassandraUtilities.GetStreamColumns(StreamID);
                StringBuilder insertString = new StringBuilder(400);

                insertString.Append("INSERT INTO data");
                insertString.Append(StreamID.ToString().Replace("-", ""));
                insertString.Append(" (Date, Tuid");

                Queue<string> valuesToInsert = new Queue<string>();
                List<string> keysToRemove = new List<string>();

                if (json.parsedtime == null)
                    json.parsedtime = DateTimeOffset.Now;

                string datestring = json.parsedtime.Value.ToString("yyyyMMddHH");
                Guid itemID = GuidGenerator.GenerateTimeBasedGuid(json.parsedtime.Value);
                valuesToInsert.Enqueue("'" + datestring + "'");
                valuesToInsert.Enqueue(itemID.ToString());

                foreach (var item in json.values.OrderBy(x => x.Key))
                {
                    //check if the key exists in the stream
                    if (!streamValueNameToType.ContainsKey(item.Key))
                    {
                        //shouldnt be here remove it so we can brodcast and store correctly
                        keysToRemove.Add(item.Key);
                        continue;
                    }

                    if (String.IsNullOrWhiteSpace(item.Value) || String.IsNullOrEmpty(item.Value))
                    {
                        keysToRemove.Add(item.Key);
                        continue;
                    }
                    //parse the value
                    string j = null;
                    try
                    {
                        j = CassandraUtilities.ParseValueGetString(streamValueNameToType[item.Key].type, item.Value.Trim());
                    }
                    catch (Exception ex)
                    {
                        Logger.Instance.LogData(ip + " : Unable to parse value to the correct object type: key - " + item.Key + " Val- " + item.Value.ToString() + " key type - " + streamValueNameToType[item.Key].type + " Err: " + ex.Message);
                        Interlocked.Increment(ref failed);
                        return new string[2] { "", "" };
                    }

                    //add it to the object queue
                    valuesToInsert.Enqueue(j);
                    //add its spot in the insertString
                    insertString.Append(", ");
                    if (item.Key.Contains(' '))
                        insertString.Append(item.Key.Replace(' ', '_'));
                    else
                        insertString.Append(item.Key);

                }

                foreach (var item in streamValueNameToType)
                {
                    if (item.Value.type == "blob" && json.values.ContainsKey(item.Key))
                    {
                        byte[] j = (byte[])CassandraUtilities.ParseValue(item.Value.type, json.values[item.Key]);
                        json.values[item.Key] = Convert.ToBase64String(j);
                    }
                }

                //remove the extra values we dont need
                foreach (string key in keysToRemove)
                {
                    json.values.Remove(key);
                }

                //finish the excess things we dont need
                insertString.Append(") VALUES ( ");
                insertString.Append(valuesToInsert.Dequeue());
                while (valuesToInsert.Count != 0)
                {
                    insertString.Append(", ");
                    insertString.Append(valuesToInsert.Dequeue());
                }
                insertString.Append(");");

                //broadcast the insert
                json.time = json.parsedtime.Value.ToString("yyyy-MM-ddTHH:mm:ss.FFF%K");

                //DataBroadcaster.Instance.BroadcastData("Complex", JsonConvert.SerializeObject(json), StreamID);
                Statistics.Instance.UpdateStatistics(StreamID, json);
                return new string[2] { insertString.ToString(), JsonConvert.SerializeObject(json) };
            }
        }


        private static readonly string ServerUploadFolder = "C:\\Temp"; //Path.GetTempPath();

        [Route("files")]
        [HttpPost]
        public async Task<FileResult> UploadSingleFile()
        {
            var streamProvider = new MultipartFormDataStreamProvider(ServerUploadFolder);
            await Request.Content.ReadAsMultipartAsync(streamProvider);

            return new FileResult
            {
                FileNames = streamProvider.FileData.Select(entry => entry.LocalFileName),
                Names = streamProvider.FileData.Select(entry => entry.Headers.ContentDisposition.FileName),
                ContentTypes = streamProvider.FileData.Select(entry => entry.Headers.ContentType.MediaType),
                Description = streamProvider.FormData["description"],
                CreatedTimestamp = DateTime.UtcNow,
                UpdatedTimestamp = DateTime.UtcNow,
                DownloadLink = "TODO, will implement when file is persisited"
            };
        }

        public class FileResult
        {
            public IEnumerable<string> FileNames { get; set; }
            public string Description { get; set; }
            public DateTime CreatedTimestamp { get; set; }
            public DateTime UpdatedTimestamp { get; set; }
            public string DownloadLink { get; set; }
            public IEnumerable<string> ContentTypes { get; set; }
            public IEnumerable<string> Names { get; set; }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}