﻿using System.Web;
using System.Web.Mvc;

namespace SensorStreamManage
{
    public class FilterConfig
    {
        [RequireHttps]
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            filters.Add(new System.Web.Mvc.AuthorizeAttribute());
            filters.Add(new RequireHttpsAttribute());
        }
    }
}
