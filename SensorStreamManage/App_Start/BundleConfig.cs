﻿using System.Web;
using System.Web.Optimization;

namespace SensorStreamManage
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js", 
                        "~/Scripts/jquery.dataTables.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css",
                      "~/Content/jquery.dataTables.css"));

            bundles.Add(new StyleBundle("~/Content/dashboard").Include(
                        "~/Content/jquery.gridster.css",
                        "~/Content/nv.d3.css",
                        "~/Content/dashboard.css"
                        ));

            bundles.Add(new ScriptBundle("~/bundles/dashboard").Include(
                        "~/Scripts/jquery.signalR-2.2.0.min.js",
                        "~/Scripts/jquery.gridster.with-extras.js",
                        "~/Scripts/d3/d3.js",
                        "~/Scripts/nv.d3.js",
                        "~/Scripts/moment-with-locales.js",
                        "~/Scripts/dataman.js",
                        "~/Scripts/extensions.js"));

            // Set EnableOptimizations to false for debugging. For more information,
            // visit http://go.microsoft.com/fwlink/?LinkId=301862
            BundleTable.EnableOptimizations = false;
        }
    }
}
