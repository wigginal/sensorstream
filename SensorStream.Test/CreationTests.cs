﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SensorStream.Test
{
    [TestClass]
    public class CreationTests
    {
        static string server = "http://localhost";
        int delay = 200;
        public SSConnection getConnection()
        {
            Thread.Sleep(delay);
            return new SSConnection(server);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void CreateDevice_WithInvalidDevice()
        {
            Device newDev = new Device() {  devicename = "", description = ".NET library test", username = "asd" };
            SSConnection conn = getConnection();
            conn.CreateDevice(newDev);
        }

        [TestMethod]
        public void CreateDevice_WithValidDevice()
        {
            Device newDev = CreateTestDevice();
            DeleteDevice_AfterInsert(newDev);
        }

        public Device CreateTestDevice()
        {
            Device newDev = new Device() { devicename = "asd", description = ".NET library test", username = "asd" };
            SSConnection conn = getConnection();
            Device retDevice = conn.CreateDevice(newDev);
            Assert.AreEqual(retDevice.devicename, "asd");
            Assert.AreEqual(retDevice.description, ".NET library test");
            Assert.AreEqual(retDevice.username, "asd");
            Assert.IsFalse(String.IsNullOrEmpty(retDevice.guid.ToString()));
            Assert.IsFalse(String.IsNullOrEmpty(retDevice.created.ToString()));
            Assert.IsFalse(String.IsNullOrEmpty(retDevice.latestip));
            Thread.Sleep(delay);
            return retDevice;
        }

        public void DeleteDevice_AfterInsert(Device dev)
        {
            SSConnection conn = getConnection();
            Assert.IsTrue(conn.DeleteDevice(dev));
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void DeleteDevice_BeforeInsert()
        {
            Device newDev = new Device() { devicename = "tester", description = ".NET library test", username = "asd" };
            SSConnection conn = getConnection();
            conn.DeleteDevice(newDev);
        }

        [TestMethod]
        public void DeleteStream_WithValidStuff()
        {
            Device newDev = CreateTestDevice();
            DeviceStream testStream = CreateTestComplexStream(newDev);

            //delete that stream
            SSConnection conn = getConnection();
            conn.DeleteStream(newDev, testStream);
            Thread.Sleep(delay);

            //delete the device
            DeleteDevice_AfterInsert(newDev);
        }

        [TestMethod]
        public void DeleteDeviceStreams_WithValidStuff()
        {
            Device newDev = CreateTestDevice();
            DeviceStream testStream = CreateTestComplexStream(newDev);

            SSConnection conn = getConnection();
            conn.DeleteDeviceStreams(newDev);
            Thread.Sleep(delay);
            //delete the device
            DeleteDevice_AfterInsert(newDev);
        }

        [TestMethod]
        public void CreateComplexStream_Valid()
        {
            Device newDev = CreateTestDevice();

            //create a complex stream
            DeviceStream testStream = CreateTestComplexStream(newDev);

            //delete the complex stream
            SSConnection conn = getConnection();
            conn.DeleteDeviceStreams(newDev);
            Thread.Sleep(delay);
            //delete the device
            DeleteDevice_AfterInsert(newDev);
        }

        private DeviceStream CreateTestComplexStream(Device newDev)
        {
            SSConnection conn = getConnection();
            DeviceStream newDS = new DeviceStream() { description = "some crazy stream", name = "Nelsi per inch of force", streams = new List<StreamInfo>() { new StreamInfo() { name = "somethingcomplex", type = "int", units = "snorts per volt" } } };
            DeviceStream retStream = conn.CreateStream(newDev, newDS);
            Assert.IsFalse(retStream.streamid == Guid.Empty.ToString());
            Assert.AreEqual(retStream.name, "Nelsi per inch of force");
            Thread.Sleep(delay);
            return retStream;
        }

        private bool AddComplexData(Device dev, DeviceStream str)
        {
            Random random = new Random();
            int rand = random.Next(200);
            SSConnection conn = getConnection();
            Data d = new Data() { streamid = str.streamid, time = SSConnection.ConvertDateTimeToString(DateTime.Now), values = new Dictionary<string, string>() };
            d.values.Add("somethingcomplex", rand.ToString());
            return conn.SendData(dev, d);
        }

        private bool AddComplexData(Device dev, DeviceStream str, int houroffset)
        {
            Random random = new Random();
            int rand = random.Next(200);
            SSConnection conn = getConnection();
            Data d = new Data() { streamid = str.streamid, time = SSConnection.ConvertDateTimeToString(DateTime.Now.AddHours(houroffset)), values = new Dictionary<string, string>() };
            d.values.Add("somethingcomplex", rand.ToString());
            return conn.SendData(dev, d);
        }

        [TestMethod]
        public void CreateGeigerCounterStream()
        {
            SSConnection conn = getConnection();
            Device newDev = new Device() { devicename = "TK1 Recognizer", description = "Jetson TK1 with stereo facial recognition. Using haar cascade method with openCV for frontal facial detection", username = "asd" };
            Device retDevice = conn.CreateDevice(newDev);
            Thread.Sleep(delay);
            DeviceStream newDS = new DeviceStream() { description = "Images of detected faces", name = "Faces", streams = new List<StreamInfo>() { new StreamInfo() { name = "Faces", type = "blob", units = "image" } } };
            DeviceStream retStream = conn.CreateStream(retDevice, newDS);
            //Thread.Sleep(delay);
            //conn.DeleteDeviceStreams(newDev);

            Thread.Sleep(delay);
        }


        [TestMethod]
        public void CreateComplexData_Valid()
        {
            Device newDev = CreateTestDevice();

            //create a complex stream
            DeviceStream testStream = CreateTestComplexStream(newDev);

            //add complex data
            Assert.IsTrue(AddComplexData(newDev, testStream));

            //delete the device
            DeleteDevice_AfterInsert(newDev);
        }


        [TestMethod]
        public void CreateMultipleComplexData_Valid()
        {
            Device newDev = CreateTestDevice();

            //create a complex stream
            DeviceStream testStream = CreateTestComplexStream(newDev);

            //add complex data
            Assert.IsTrue(AddComplexData(newDev, testStream));

            Assert.IsTrue(AddComplexData(newDev, testStream));

            Assert.IsTrue(AddComplexData(newDev, testStream));

            Assert.IsTrue(AddComplexData(newDev, testStream));

            Assert.IsTrue(AddComplexData(newDev, testStream));

            Assert.IsTrue(AddComplexData(newDev, testStream));

            Assert.IsTrue(AddComplexData(newDev, testStream));


            //delete the device
            DeleteDevice_AfterInsert(newDev);
        }

        [TestMethod]
        public void CreateMultipleComplexDataHourly_Valid()
        {
            Device newDev = CreateTestDevice();

            //create a complex stream
            DeviceStream testStream = CreateTestComplexStream(newDev);

            //add complex data
            Assert.IsTrue(AddComplexData(newDev, testStream));

            Assert.IsTrue(AddComplexData(newDev, testStream));

            Assert.IsTrue(AddComplexData(newDev, testStream,2 ));

            Assert.IsTrue(AddComplexData(newDev, testStream, 2));

            Assert.IsTrue(AddComplexData(newDev, testStream, 5));

            Assert.IsTrue(AddComplexData(newDev, testStream, 5));

            Assert.IsTrue(AddComplexData(newDev, testStream, 5));

            //delete the device
            DeleteDevice_AfterInsert(newDev);
        }


        [TestMethod]
        public void GetDevices_Valid()
        {
            Device newDev = CreateTestDevice();
            SSConnection conn = getConnection();
            List<Device> devices = conn.GetDevices();
            Assert.IsTrue(devices.Count > 0);
            DeleteDevice_AfterInsert(newDev);
        }

        [TestMethod]
        public void GetStreams_Valid()
        {
            Device newDev = CreateTestDevice();
            DeviceStream testStream = CreateTestComplexStream(newDev);

            //delete that stream
            SSConnection conn = getConnection();
            Device dev = conn.GetStreamsFromDeviceID(newDev);
            CheckStreamValid(dev.streams[0]);
            dev = conn.GetStreamFromStreamID(testStream);
            CheckStreamValid(dev.streams[0]);
            dev = conn.GetStreamsFromUserAndDeviceNames(newDev);
            CheckStreamValid(dev.streams[0]);
            //delete the device
            DeleteDevice_AfterInsert(newDev);
        }

        private void CheckStreamValid(DeviceStream deviceStream)
        {
            Assert.AreEqual(deviceStream.name, "Nelsi per inch of force");
        }

        [TestMethod]
        public void GetComplexData_Valid()
        {
            Device newDev = CreateTestDevice();

            //create a complex stream
            DeviceStream testStream = CreateTestComplexStream(newDev);

            //add complex data
            Assert.IsTrue(AddComplexData(newDev, testStream));

            //Get the complex data
            SSConnection conn = getConnection();
            Device d = conn.GetData(testStream);
            Assert.IsTrue(d.streams[0].data.Count == 1);
            d = conn.GetData(testStream, true);
            Assert.IsTrue(d.streams[0].data.Count == 1);

            //delete the device
            DeleteDevice_AfterInsert(newDev);
        }

        [TestMethod]
        public void GetStatistics_Valid()
        {
            Device newDev = CreateTestDevice();
            DeviceStream testStream = CreateTestComplexStream(newDev);
            Assert.IsTrue(AddComplexData(newDev, testStream));
            SSConnection conn = getConnection();
            OverallStreamStatistics stats = conn.GetStatistics(testStream);
            DeleteDevice_AfterInsert(newDev);
        }


        [TestMethod]
        public void SubscribeTest()
        {
            SSConnection conn = getConnection();
            Device newDev = CreateTestDevice();
            DeviceStream testStream = CreateTestComplexStream(newDev);

            //subscribe
            bool connected = conn.startConnection(server);
            conn.dataRecieved += conn_dataRecieved;
            conn.Subscribe(testStream.streamid);

            //add simple data
            Assert.IsTrue(AddComplexData(newDev, testStream));

            //make sure we got the data from signalr
            int i = 0;
            while (i < 10 && !dataRecieved)
            {
                Thread.Sleep(1000);
                Assert.IsTrue(AddComplexData(newDev, testStream));
                i++;
            }
            if (dataRecieved == false)
            {
                DeleteDevice_AfterInsert(newDev);
                throw new Exception("Failed!");
            }
            //remove subscription
            conn.Unsubscribe(testStream.streamid);
            Thread.Sleep(1000);

            dataRecieved = false;

            //add data
            Assert.IsTrue(AddComplexData(newDev, testStream));

            //make sure we didn't get anything
            while (i < 10 && !dataRecieved)
            {
                Assert.IsTrue(AddComplexData(newDev, testStream));
                Thread.Sleep(500);
                i++;
            }

            if (dataRecieved == true)
            {
                DeleteDevice_AfterInsert(newDev);
                throw new Exception("Failed!");
            }
            //delete device
            DeleteDevice_AfterInsert(newDev);

        }

        static bool dataRecieved = false;
        void conn_dataRecieved(object sender, EventArgs e)
        {
            dataRecieved = true;
        }

    }
}
