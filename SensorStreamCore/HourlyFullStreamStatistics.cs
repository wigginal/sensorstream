﻿using Cassandra.Data.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SensorStreamCore
{
    public class HourlyFullStreamStatistics
    {
        [PartitionKey]
        public string streamid;

        [ClusteringKey(0)]
        public string timeindex;

        public long count;

        public Dictionary<string, IndividualStreamStatistics> streamstats = new Dictionary<string, IndividualStreamStatistics>();
    }
}
