﻿using Cassandra;
using Cassandra.Data.Linq;
using System.Diagnostics;


namespace SensorStreamCore
{
    public sealed class CassandraCluster
    {

        private ISession _session;
        public ISession Session
        {
            get
            {
                return _session;
            }
        }

        public CassandraCluster(string cassandraNode, string cassandraKeyspace)
        {
            Diagnostics.CassandraTraceSwitch.Level = TraceLevel.Verbose;
            Cluster cluster = Cluster.Builder().AddContactPoint(cassandraNode).Build();

            _session = cluster.Connect();
            _session.CreateKeyspaceIfNotExists(cassandraKeyspace);
            _session.ChangeKeyspace(cassandraKeyspace);
        }

        public void SwitchKeyspace(string keyspaceName)
        {
            try
            {
                Session.ChangeKeyspace(keyspaceName);
            }
            catch (InvalidQueryException)
            {
                Session.CreateKeyspaceIfNotExists(keyspaceName);
                Session.ChangeKeyspace(keyspaceName);
            }
        }
    }
}