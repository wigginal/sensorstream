﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SensorStreamCore
{
    public class Data
    {
        [JsonProperty("streamid", NullValueHandling = NullValueHandling.Ignore)]
        public string streamid;

        [JsonProperty("time", NullValueHandling = NullValueHandling.Ignore)]
        public string time;

        [JsonProperty("values", NullValueHandling = NullValueHandling.Ignore)]
        public Dictionary<string, string> values;

        [JsonProperty("parsedtime", NullValueHandling = NullValueHandling.Ignore)]
        public Nullable<DateTimeOffset> parsedtime;
    }
}
