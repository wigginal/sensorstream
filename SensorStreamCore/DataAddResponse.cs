﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SensorStreamCore
{
    public class DataAddResponse
    {
        [JsonProperty("streamid", NullValueHandling = NullValueHandling.Ignore)]
        public string streamid;

        [JsonProperty("time", NullValueHandling = NullValueHandling.Ignore)]
        public DateTimeOffset time;

        [JsonProperty("status", NullValueHandling = NullValueHandling.Ignore)]
        public string status;
    }
}
