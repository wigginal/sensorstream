﻿using Cassandra.Data.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SensorStreamCore
{
    public class TranscriptionLink
    {
        [PartitionKey]
        public Guid stream1;

        [ClusteringKey(0)]
        public Guid stream2;
    }
}
