﻿using Cassandra.Data.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SensorStreamCore
{
    public class CassandraStreamByDevice
    {
        [PartitionKey]
        public Guid deviceID;

        [ClusteringKey(0)]
        public Guid StreamID;

        public string Name;
    }
}
