﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SensorStreamCore
{
    public class StreamInfo
    {
        [JsonProperty("name")]
        public string name;

        [JsonProperty("units")]
        public string units;

        [JsonProperty("type")]
        public string type;
    }
}
