﻿using Cassandra;
using Cassandra.Data.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SensorStreamCore
{
    public class CassandraStream
    {
        [PartitionKey]
        public Guid StreamID;

        public string devName;

        public string User;

        public string Name;

        public string Units;

        public string Description;

        public DateTimeOffset Created;

        public string IPCreated;

        public List<String> Streams;

        public bool privatestream;

    }
}
