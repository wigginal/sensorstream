﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SensorStreamCore
{
    public class AudioData
    {
        [JsonProperty("streamid", NullValueHandling = NullValueHandling.Ignore)]
        public string streamid;

        [JsonProperty("time", NullValueHandling = NullValueHandling.Ignore)]
        public string time;

        [JsonIgnore]
        public Nullable<DateTimeOffset> parsedtime;

        [JsonProperty("value", NullValueHandling = NullValueHandling.Ignore)]
        public string value;

        [JsonProperty("speechtotext", NullValueHandling = NullValueHandling.Ignore)]
        public string speechtotext;

        [JsonProperty("words", NullValueHandling = NullValueHandling.Ignore)]
        public List<String> words;

        [JsonProperty("wordscores", NullValueHandling = NullValueHandling.Ignore)]
        public List<double> wordscores;
    }
}
