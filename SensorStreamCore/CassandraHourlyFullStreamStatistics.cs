﻿using Cassandra.Data.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SensorStreamCore
{
    public class CassandraHourlyFullStreamStatistics
    {
        [PartitionKey]
        public string streamid;

        [ClusteringKey(0)]
        public string timeindex;

        public long count;

        public string streamstats;
    }
}
