﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SensorStreamCore
{
    public class IndividualStreamStatistics
    {
        public string id;
        public long count;
        public double variance;
        public double mean;
        public double m2;
        public double min;
        public double max;
        public double stddev;
    }
}
