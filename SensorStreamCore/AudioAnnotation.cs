﻿using Cassandra.Data.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SensorStreamCore
{
    public class AudioAnnotation
    {
        [PartitionKey]
        public string word;

        [ClusteringKey(0)]
        public Guid stream;

        [ClusteringKey(1)]
        public Guid streamindex;

    }
}
