﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SensorStreamCore
{
    public class OverallStreamStatistics
    {
        public string streamid;
        public long count;
        public DateTimeOffset firstentry;
        public DateTimeOffset lastentry;
        public Dictionary<string, IndividualStreamStatistics> streamstats = new Dictionary<string, IndividualStreamStatistics>();
    }
}
