﻿using Cassandra.Data.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SensorStreamCore
{
    public class UserNames
    {
        [PartitionKey]
        public string UserName;
        public string msID;
    }
}
