﻿using Cassandra.Data.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SensorStreamCore
{
    public class CassandraSensorDevice
    {
        [PartitionKey]
        public string UserName;

        [ClusteringKey(0)]
        public string DeviceName;

        [SecondaryIndex]
        public Guid guid;

        [SecondaryIndex]
        public string LatestIP;

        [SecondaryIndex]
        public DateTimeOffset Created;

        public Guid key;

        public bool privatedevice;

        public string Description;
    }
}
