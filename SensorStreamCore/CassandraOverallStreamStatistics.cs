﻿using Cassandra.Data.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SensorStreamCore
{
    public class CassandraOverallStreamStatistics
    {
        [PartitionKey]
        public string streamid;
        public long count;
        public DateTimeOffset firstentry;
        public DateTimeOffset lastentry;
        public string streamstats;
    }
}
