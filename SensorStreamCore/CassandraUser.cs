﻿using Cassandra.Data.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SensorStreamCore
{
    public class CassandraUser
    {
        [PartitionKey]
        public string Id;
        [ClusteringKey(0)]
        public Guid DateAdded;
        public string Name;
        public string Email;
        public string UserName;
        public string Locale;
        public string Picture;
    }
    public class SharedWith
    {
        [PartitionKey]
        public Guid DeviceID;
        [ClusteringKey(0)]
        public string UserName;
    }

    public class UnregisteredDevice
    {
        [PartitionKey]
        public Guid DeviceID;
        public string Key;
        public string DeviceInfo;
        public string Streams;
        public bool Registered;
        public DateTimeOffset date;
        public string IPRegistered;
        public string UserName;
    }

    public class SharedWithMe
    {
        [PartitionKey]
        public string msID;

        public Guid DeviceID;
    }

    public class PublicDevice
    {
        [PartitionKey]
        public Guid DeviceID;
    }
}