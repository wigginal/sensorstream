﻿using Cassandra.Data.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SensorStreamCore
{
    public class CassandraDevicesByGUID
    {
        [PartitionKey]
        public Guid guid;

        public string DeviceName;

        public string UserName;
    }
}
