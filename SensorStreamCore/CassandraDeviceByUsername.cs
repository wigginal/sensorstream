﻿using Cassandra.Data.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SensorStreamCore
{
    public class CassandraDeviceByUsername
    {
        [PartitionKey]
        public string UserName;

        [PartitionKey(1)]
        public string DeviceName;
    }
}
